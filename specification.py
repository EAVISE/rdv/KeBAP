# Internal representation of a logical specification or model
# Authors: Robin De Vogelaere, Kylian Van Dessel

from __future__ import annotations

import warnings

from blocks import Block, Vocabulary, Structure, Theory, Optimise, Query
from nodes import Node, BlockName


class AttributeNotEmptyError(Exception):
    def __init__(self, knd: str):
        msg = f'\'{knd}\' block is already in use. Create new specification or overwrite block manually.'
        super().__init__(msg)


class AttributeNotExistError(Exception):
    def __init__(self, knd: str):
        msg = (f'\'{knd}\' block does not exist. '
               'Supported blocks are \'vocabulary\', \'structure\', \'theory\', and \'objectvalue\'.')
        super().__init__(msg)


def apply(func_, list_, *args, **kwargs):
    return list(map(lambda x: func_(x, *args, **kwargs), filter(None, list_)))


class Specification:
    def __init__(self, blocks, ignore_freevars=False):
        self.vocabulary: Vocabulary  = None
        self.structure: Structure = None
        self.theory: Theory = None
        self.optimise: Optimise = None
        self.query: Query = None

        self.add_blocks(blocks)  # add blocks if unique and existing kind
        self._complete_specification()  # add empty blocks for the ones that were not provided
        # TODO check if this fixes your problem
        #  + check if this is still needed since constructed from elements are no longer considered free variables
        #    (might need some fine tuning still but...
        #    As far as I can tell, preliminary tests show it only prevents 4 errors, which are actual errors...
        self._complete_symbol_signatures(raise_error_on_symbol_mismatch=not ignore_freevars)  # add typing and/or arity to all symbols

    def __call__(self):
        return list(filter(None, [self.vocabulary, self.structure, self.theory, self.optimise, self.query]))

    def __str__(self):
        return self.to_tree()

    def to_tree(self, depth=2):
        """ Convert specification blocks' trees to string representation. """
        return '\n'.join(x.tree.to_tree(depth) for x in self())

    #  COMPLETE/CHECK BLOCKS  ------------------------------------------------------------------------------------------
    def _complete_specification(self):
        """ Add empty block for each not existing block. """
        if not self.vocabulary:
            self.vocabulary = Vocabulary(Node.Block('vocabulary', 'V'))
        if not self.structure:
            self.structure = Structure(Node.Block('structure', 'S', self.vocabulary.name))
        if not self.theory:
            self.theory = Theory(Node.Block('theory', 'T', self.vocabulary.name))

    def _complete_symbol_signatures(self, raise_error_on_symbol_mismatch=True):
        """ Complete all symbols in theory and structure with their typing and/or arity based on vocabulary symbols"""
        if not self.vocabulary or self.vocabulary.is_empty():
            warnings.warn('The symbols\' signatures could not be completed, '
                 'the specification does not have a vocabulary (or it is empty).')
            return
        matcher = self.vocabulary.match_vocabulary(self.vocabulary, create_new_matcher=True)
        for block in self():
            if block == self.vocabulary:
                continue
            if block.vocname != self.vocabulary.name:
                print('warning: The symbols\' signatures could not be completed, '
                     'the specification does not use a consistent vocabulary.')
                continue
            try:
                block.match_vocabulary(matcher, create_new_matcher=False)
            except ValueError as e:
                if raise_error_on_symbol_mismatch:
                    raise e
                warnings.warn(f'Some symbol(\'s) signature(s) could not be completed, '
                      f'due to ambiguity or (an) unknown symbol(s).')
        return

    def _check_vocabulary_consistency(self):
        """ Check that all blocks use the same vocabulary. """
        if not self.vocabulary:
            warnings.warn('Cannot check consistent vocabulary use, specification does not have a vocabulary.')
        else:
            if self.vocabulary.is_empty():
                warnings.warn(f'Vocabulary \'{self.vocabulary.name}\' is empty. Unless the complete specification '
                              f'is empty, this should not be the case.')
            for block in self():
                if block.vocname != self.vocabulary.name:
                    warnings.warn(f'Inconsistent vocabulary use, {block.kind} \'{block.name}\' uses vocabulary'
                                  f' \'{block.vocname}\' instead of \'{self.vocabulary.name}\'.')
                    return False
        # check theory and structure are based on same vocabulary
        if self.theory.vocname != self.structure.vocname:
            warnings.warn(f'Inconsistent vocabulary use, THEORY \'{self.theory.name} \' uses vocabulary {self.theory.vocname}, '
                          f'while STRUCTURE \'{self.structure.name}\' uses vocabulary {self.structure.vocname}')
            return False
        return True

    #  ADD BLOCKS  -----------------------------------------------------------------------------------------------------
    def add_block(self, block: Block):
        """ Add a Block to the corresponding property. """
        #if isinstance(block, Node):
        #    Block = block.symbol.capitalize()
        #    block = globals()[Block](block)
        attr = block.kind.lower()
        try:
            self.__getattribute__(attr)
        except AttributeError:
            raise AttributeNotExistError(attr)
        if self.__getattribute__(attr) is not None:
            raise AttributeNotEmptyError(attr)
        self.__setattr__(attr, block)

    def add_blocks(self, blocks: list):
        apply(self.add_block, blocks)


if __name__ == '__main__':
    pass
