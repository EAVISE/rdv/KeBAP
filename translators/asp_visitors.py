# ASP representation visitors
# Authors: Kylian Van Dessel, Joost Vennekens

from __future__ import annotations

import re
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from nodes import Node, Symbol
from blocks import Structure  # , Vocabulary  # TODO check need in ASPTest
from visitors import VocabularyVisitor, StructureVisitor, TheoryVisitor
from manipulators.finder_visitors import extract_name, FreeVarFinder, DefinedSymbolFinder
from manipulators.updater_visitors import PredicateReplacer

asp_symbol = {'EQ': '=', 'NEQ': '!=', 'LT': '<', 'GT': '>', 'LEQ': '<=', 'GEQ': '>=',
              'CNT': '#count', 'SUM': '#sum', 'MIN': '#min', 'MAX': '#max',
              'PLUS': '+', 'MINUS': '-', 'TIMES': '*', 'DIVIDE': '/', 'MODULO': '\\'}


class ASPGenerateDefine(StructureVisitor, VocabularyVisitor):
    """ Create ASP define and generate part from structure (and vocabulary) assignments and vocabulary declarations"""

    def __init__(self, struct : Structure|None = None):
        self.assigned = []
        if struct:
            self.assigned = struct.get_fully_interpreted_predicates()

    def visit_VOCABULARY(self, node, *args):
        return self.visit(node.children[2])

    def visit_VOCABULARYBODY(self, node, *args):
        return '\n'.join(list(filter(None, [self.visit(x) for x in node.children])))

    def visit_DECLARATION(self, node, *args):
        symb = Symbol(node.children[0])
        try:  # (assignment in structure)
            symb.find_in(self.assigned)
            pass
        except ValueError:
            if len(node.children) > 1:  # assignment
                return self.visit(Node("ASSIGNMENT", node.children + [Node('NVALUED', [None])]))
            else:  # declaration
                if not hasattr(symb, 'typing'):
                    raise AttributeError('Symbol typing missing for generating ASP choice rule for '
                                         f'"{extract_name(node)}". Types or unsorted predicates should '
                                         'be interpreted by the structure or assigned values in vocabulary.')
                if symb.typing:
                    args, vars = zip(*self.visit(node.children[0].children[1]))
                    return f'{{ {self.visit(node.children[0])}({", ".join(vars)}) }} :- {", ".join(args)}.'
                return f'{{ {self.visit(node.children[0])} }}.'
        return

    def visit_STRUCTURE(self, node, *args):
        return self.visit(node.children[2])

    def visit_STRUCTUREBODY(self, node, *args):
        return '\n'.join(filter(None, map(self.visit, node.children)))

    def visit_ASSIGNMENT(self, node, *args):
        is_false = (node.children[2].children[0] is False)
        if PredicateSymbol(node.children[0]).arity == 0:
            if len(node.children[1].children) == 0:  # boolean false
                return f'not not {self.visit(node.children[0])}.' if is_false else ''
            if len(node.children[1].children[0].children) == 0:  # boolean true
                return ('not ' if is_false else '') + f'{self.visit(node.children[0])}.'
        return ' '.join(list(map(lambda x: ('not ' if is_false else '') + f'{self.visit(node.children[0])}({x}).',
                                 self.visit(node.children[1]))))

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_TYPING(self, node, *args):
        return [(f'{self.visit(x)}(X{i})', f'X{i}') for i, x in enumerate(node.children)]

    def visit_PRED_ENUM(self, node, *args):
        return list(map(self.visit, node.children))

    def visit_TUPLE(self, node, *args):
        return ','.join(list(map(self.visit, node.children)))

    def visit_NAME(self, node, *args):
        x = extract_name(node)
        try:
            int(x)
        except ValueError:
            #x = re.sub(r'"', '', x).lower()
            pass
        return x

    def visit_FUNC(self, node, *args):
        raise NotImplementedError('FO function symbols are not supported in ASP.' +
                                  '(There are methods provided for converting them to predicate symbols)')

    def visit_FUNC_ENUM(self, node, *args):
        raise NotImplementedError('FO function assignments are not supported in ASP.')


class ASPTest(TheoryVisitor):
    def __init__(self, vocab=None, struc=None):
        self.rules = []
        self.minimize = ''
        self.param_counter = 0  # counter for auxiliary predicates
        self.var_counter = 0  # counter for auxiliary variables
        #self.vocab = vocab  # type: Vocabulary  # TODO check need
        self.struc = struc  # type: Structure

    def __str__(self):
        return '\n'.join([f'{x[0]} :- {x[1]}.'.lstrip() for x in self.rules] + [self.minimize]).rstrip() + '\n'

    def new_param(self, var):
        self.param_counter += 1
        if var == [] or var == set([]):
            return f"'p{str(self.param_counter - 1)}"
        else:
            return f"'p{str(self.param_counter - 1)}({', '.join([self.visit(x) for x in var])})"

    def new_var(self):
        self.var_counter += 1
        return f'_Y{str(self.var_counter - 1)}'

    def emit(self, head, body='', var=None, typing=None, eqs=None):
        """ Add ASP rule to list (add any remaining variables' typing to rule first). """
        if eqs:
            body += ASPTest.join_with_commas(eqs, leading_comma=True)
        if var:
            body += ASPTest.make_types(var, typing)
        self.rules.append((head, body))

    @staticmethod
    def freevars(node):
        return FreeVarFinder().visit(node)

    @staticmethod
    def join_with_commas(lst, leading_comma=False):
        if leading_comma:
            return f", {', '.join(lst)}".rstrip(', ')
        return ', '.join(lst)

    @staticmethod
    def make_types(var, typing):
        """ return typed variables to append to formula.
            e.g., x[T] y[T2]: ... -> :- ..., T(x), T2(y).
        """
        res = ', '.join(list(map(lambda x: f'{typing[extract_name(x)]}({extract_name(x)})', var)))
        if res == '':
            return res
        return f', {res}'

    @staticmethod
    def varlist_to_dict(varlist):
        """ Return list of variables (with their typing) as a dict. """
        return dict([(extract_name(x.children[0]), extract_name(x.children[1])) for x in varlist])

    def visit_THEORY(self, node, *args):
        # TODO clear self.rules first? (issues with using the same class object twice...
        self.visit(node.children[2])
        return str(self)

    def visit_THEORYBODY(self, node, *args):
        list(map(self.visit, node.children))

    def visit_CONSTRAINT(self, node, typing=None):
        self.var_counter = 0
        typing = {}
        self.emit('', f'not {self.visit(node.children[0], typing)}', ASPTest.freevars(node.children[0]), typing)

    # term optimization

    def visit_OPTIMISE(self, node, *args):
        # TODO clear self.rules first? (issues with using the same class object twice...
        self.minimize = self.visit(node.children[2], *args)
        return str(self)

    def visit_OPTIMISEBODY(self, node, minimize=True):
        term = node.children[0].children[0]
        if term.children[0] != 'SUM':
            raise NotImplementedError('Only constant terms or simple aggregates can be optimized yet.')
        varlist = term.children[1]
        formula = self.visit(term.children[2], ASPTest.varlist_to_dict(varlist))
        if self.rules:
            formula = self.rules[-1][1]
            self.rules = self.rules[:-1]
        x = self.visit(term.children[3]).lstrip('(').rstrip(')')
        vars = f'{x} {" ".join(list(map(extract_name, varlist[:-1])))}'
        if minimize:
            return f'#minimize{{{vars}: {formula}}}.'
        return f'#maximize{{{vars}: {formula}}}.'

    # logical expressions

    def visit_NOT(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)
        self.emit(term, f'not {self.visit(node.children[0], typing)}', vars, typing)
        return term

    def visit_AND(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)
        self.emit(term, ', '.join(list(map(lambda x: self.visit(x, typing), node.children))), vars, typing)
        return term

    def visit_OR(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)
        list(map(lambda x: self.emit(term, self.visit(x, typing), vars, typing), node.children))
        return term

    # quantification

    def visit_FORALL(self, node, typing=None):
        typing.update(ASPTest.varlist_to_dict(node.children[0]))
        var = ASPTest.freevars(node)
        term = self.new_param(var)

        type_var = self.make_types(node.children[0][:1], typing)[2:]
        types = self.make_types(node.children[0], typing)
        agg_val = f'{{ {type_var} : {self.visit(node.children[1], typing)}{types} }} = '

        try:
            type_ = Node.Type(node.children[0][0].children[1])
            nb = str(self.struc.get_nb_of_elements(type_))
        except (AttributeError, TypeError):  # no structure provided, type interpretation not found
            nb_var = self.new_var()
            nb = f'{nb_var}, {nb_var} = {{ {type_var} : {type_var} }}'
        agg_val += nb
        self.emit(term, agg_val, var, typing)
        return term

    def visit_EXISTS(self, node, typing=None):
        typing.update(ASPTest.varlist_to_dict(node.children[0]))
        vars_ = ASPTest.freevars(node)
        term = self.new_param(vars_)
        vars_ch = ASPTest.freevars(node.children[1])
        self.emit(term, self.visit(node.children[1], typing), vars_ch, typing)
        return term

    # mathematical expressions
    # TODO check if this works properly
    def visit_SET(self, node, typing=None):
        typing.update(ASPTest.varlist_to_dict(node.children[0]))
        vars = ASPTest.freevars(node)
        eqs = []  # list with equations to add to atom
        set_ = ('{' + self.visit(node.children[1], typing)
                + ' : ' + self.make_types(node.children[0], typing)[2:] + '}')
        return set_, eqs, vars, typing

    # TODO check if this works properly
    def visit_SETOBJ(self, node, typing=None):
        typing.update(ASPTest.varlist_to_dict(node.children[0]))
        vars = ASPTest.freevars(node)
        eqs = []  # list with equations to add to atom
        res = self.visit(node.children[2], typing)
        if isinstance(res, tuple):  # (VAR, VAR = ..., [X, Y, ...], {X:T, Y:T2, ...})
            eqs.extend([res[1]] if not isinstance(res[1], list) else res[1])
            vars = vars.union(res[2])
            typing.update(res[3])
            res = res[0]
        var_ = ', '.join([res] + list(set(map(extract_name, node.children[0]))-{res}))
        set_ = ('{' + var_ + ':'
                + self.visit(node.children[2], typing) + self.make_types(node.children[1], typing) + '}')
        return set_, eqs, vars, typing

    # TODO check if this works properly
    def visit_AGGRSET(self, node, typing=None):
        print('warning: functionality not tested properly')
        set_, eqs, vars, typing = self.visit(node.children[1], typing)
        symb = (asp_symbol[node.children[0]] if node.children[0] != 'CNT' else '')
        agg = f'{symb}{set_}'
        var = self.new_var()
        eqs.append(f'{var} = {agg}')
        return var, eqs, vars, typing

    def visit_AGGRLST(self, node, *args):
        raise NotImplementedError('Translation of aggregate lists not yet implemented.')

    def visit_CMP(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)

        eqs = []
        l = self.visit(node.children[1], typing)
        r = self.visit(node.children[2], typing)
        if isinstance(l, tuple):  # (VAR, VAR = ..., [X, Y, ...], {X:T, Y:T2, ...})
            eqs.extend(l[1])
            vars.extend(l[2])
            typing.update(l[3])
            l = l[0]
        if isinstance(r, tuple):
            eqs.extend(r[1])
            vars.extend(r[2])
            typing.update(r[3])
            r = r[0]

        cmp = f'{l} {asp_symbol[node.children[0]]} {r}'
        self.emit(term, cmp, vars, typing, eqs)
        return term

    def visit_ABS(self, node, typing=None):
        return f'|{self.visit(node.children[0], typing)}|'

    def visit_MIN(self, node, *args):
        try:
            type_ = Node.Type(node.children[0])
            return str(self.struc.get_min_of_elements(type_))
        except (AttributeError, TypeError):  # no structure provided, type interpretation not found
            print(f'warning: Did not find interpretation of "{extract_name(node.children[0])}"'
                            'for MIN of type. Alternative will not work in all cases.')
            pass
        var = self.new_var()
        return f'#min{{{var}:{self.visit(node.children[0])}({var})}}'

    def visit_MAX(self, node, typing=None):
        try:
            type_ = Node.Type(node.children[0])
            return str(self.struc.get_max_of_elements(type_))
        except (AttributeError, TypeError):  # no structure provided, type interpretation not found
            print(f'warning: Did not find interpretation of "{extract_name(node.children[0])}"'
                            'for MAX of type. Alternative will not work in all cases.')
            pass
        var = self.new_var()
        return f'#max{{{var}:{self.visit(node.children[0])}({var})}}'

    def visit_MATH(self, node, *args):
        if not node.children[0]:  # UMINUS
            return f'({asp_symbol[node.children[1]]}{self.visit(node.children[2])})'
        return f'({self.visit(node.children[0])}{asp_symbol[node.children[1]]}{self.visit(node.children[2])})'

    def visit_MINUS(self, node, *args):
        return f'({asp_symbol[node.symbol]}{self.visit(node.children[0])})'

    def visit_ATOM(self, node, typing=None):
        args = f'({",".join(list(map(lambda x: self.visit(x, typing), node.children[1:])))})'
        if args == '()':  # don't bother printing empty parentheses
            args = ''
        return self.visit(node.children[0], typing) + args

    # base elements

    def visit_BOOL(self, node, *args):
        if node.get_name() == 'TRUE':
            return '#true'
        return '#false'

    def visit_VAR(self, node, *args):
        return f'{self.visit(node.children[1])}({self.visit(node.children[0])})'

    def visit_FUNC(self, node, typing=None):
        raise NotImplementedError('Functions are not supported in ASP.')

    def visit_PRED(self, node, typing=None):
        return self.visit(node.children[0], typing)

    def visit_TERM(self, node, typing=None):
        return self.visit(node.children[0], typing)

    def visit_NAME(self, node, typing=None):
        return re.sub('"', '', node.children[0])
        # return node.children[0]

    # definitions

    def visit_DEFINITION(self, node, *args):
        #asp_ = ASPTest(self.vocab, self.struc)  # TODO check need

        # change definition predicate(s) to auxiliary predicate(s) and add equivalence constraint for these predicates
        for pred in DefinedSymbolFinder().visit(node):
            new_name = '_' + pred.name  # "_p"
            node = PredicateReplacer(pred, new_name).visit(node)
            vars = ['X' + str(i) for i in range(pred.arity)]
            if vars:
                typing = {var_type[0]: extract_name(var_type[1]) for var_type in zip(vars, pred.typing)}
                body = f'{pred.name}({",".join(vars)}), not {new_name}({",".join(vars)})'
                self.emit('', body, vars, typing)
                body = f'not {pred.name}({",".join(vars)}), {new_name}({",".join(vars)})'
                self.emit('', body, vars, typing)
            else:
                self.emit('', f'{pred.name}, not {new_name}')
                self.emit('', f'not {pred.name}, {new_name}')

        # definition constraints
        list(map(lambda x: self.visit(x), node.children))

    def visit_DEFIMP(self, node, *args):
        typing = {}
        vars_ = set(list(ASPTest.freevars(node.children[2])))
        if node.children[0]:
            typing.update(ASPTest.varlist_to_dict(node.children[0]))
            vars_.update(list(map(lambda x: x.children[0], node.children[0])))
        head = self.visit(node.children[1], typing)
        body = self.visit(node.children[2], typing)
        self.emit(head, body, vars_, typing)
