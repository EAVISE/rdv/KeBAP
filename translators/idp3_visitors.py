# IDP3 representation visitors
# Authors: Kylian Van Dessel

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from nodes import Node, Symbol, extract_name
from visitors import VocabularyVisitor, StructureVisitor, TheoryVisitor

idp3_symbol = {'EQ': '=', 'NEQ': '~=', 'LT': '<', 'GT': '>', 'LEQ': '=<', 'GEQ': '>=',
              'CNT': '#', 'SUM': 'sum', 'MIN': 'min', 'MAX': 'max', 'PROD': 'prod',
              'ABS': 'abs', 'PLUS': '+', 'MINUS': '-', 'TIMES': '*', 'DIVIDE': '/', 'MODULO': '%',
              'TRUE': 'true', 'FALSE': 'false'}


class IDP3Vocabulary(VocabularyVisitor):
    """ Create IDP3 vocabulary from Vocabulary node. """

    def visit_VOCABULARY(self, node, *args):
        return (f'vocabulary {self.visit(node.children[0])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_VOCABULARYBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_DECLARATION(self, node, *args):
        symb = self.visit(node.children[0])
        if not isinstance(symb, list):
            symb = [symb]
        try:
            symb.insert(1, self.visit(node.children[1]))
        except IndexError:
            pass
        return ' '.join(symb)

    def visit_PRED(self, node, *args):
        p = Symbol(node)
        if p.is_type():
            s = [f'type {p.name}']
            s.extend(filter(None, [self.visit(x, *args) for x in node.children if x.symbol!='NAME']))
            return s
        if p.arity == 0:
            return f'{p.name}'
        return f'{p.name}({",".join(p.typing)})'

    def visit_FUNC(self, node, *args):
        f = Symbol(node)
        if f.arity == 0:
            return f'{f.name}: {f.return_type}'
        return f'{f.name}({",".join(f.typing)}): {f.return_type}'

    def visit_PFUNC(self, node, *args):
        return f'partial {self.visit_FUNC(node)}'

    def visit_PRED_ENUM(self, node, *args):
        if node.children[0].symbol == 'TUPLE':
            return f'= {{ {"; ".join(list(map(self.visit, node.children)))} }}'
        return f'constructed from {{ {", ".join(list(map(self.visit, node.children)))} }}'

    def visit_TYPING(self, node, *args):
        return [self.visit(x) for x in node.children]

    def visit_TUPLE(self, node, *args):
        return self.visit(node.children[0], *args)

    def visit_TERM(self, node, *args):
        return self.visit(node.children[0], *args)

    def visit_NAME(self, node, *args):
        return extract_name(node)

    def visit_SUBSET(self, node, *args):
        return f'isa {", ".join([self.visit(x, *args) for x in node.children])}'

    def visit_BUILTIN(self, node, *args):
        return self.visit_SUBSET(node, *args)
        #return f'isa {self.visit(node.children[0], *args)}'

    def visit_SUPERSET(self, node, *args):
        return f'contains {", ".join([self.visit(x, *args) for x in node.children])}'

    def visit_ARITY(self, node, *args):
        return ''


class IDP3Structure(StructureVisitor):
    """ Create IDP3 structure from structure node. """

    def visit_STRUCTURE(self, node, *args):
        return (f'structure {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_STRUCTUREBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_ASSIGNMENT(self, node, *args):
        try:
            arity = Symbol(node.children[0]).arity
        except AttributeError:
            arity = None
        assign = [self.visit(node.children[0]), self.visit(node.children[1], arity)]
        try:
            extra = {x.symbol: self.visit(x) for x in node.children[2:]}
            assign.insert(1, extra['NVALUED'])
        except KeyError:
            pass
        return ' = '.join([''.join(assign[:-1]), assign[-1]])

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_PRED_ENUM(self, node, *args):
        if len(node.children) == 0 and args[0] == 0:  # boolean 'false' (='{}')
            return 'false'
        elif len(node.children) == 1 and len(node.children[0].children) == 0:  # boolean 'true' (='{()}')
            return 'true'
        return '{' + '; '.join(list(map(self.visit, node.children))) + '}'

    def visit_FUNC(self, node, *args):
        return self.visit(node.children[0])

    def visit_FUNC_ENUM(self, node, *args):
        if len(node.children) == 1:
            if len(node.children[0].children) == 1:
                return self.visit(node.children[0])
        return '{' + '; '.join(
            list(map(lambda x: self.visit(x)[::-1].replace(',', '>-', 1)[::-1], node.children))) + '}'

    def visit_NVALUED(self, node, *args):
        if node.children[0] is True:
            return '<ct>'
        elif node.children[0] is False:
            return '<cf>'
        return ''

    def visit_TUPLE(self, node, *args):
        return ','.join(list(map(self.visit, node.children)))

    def visit_BOOL(self, node, *args):
        return idp3_symbol[node.children[0]]

    def visit_NAME(self, node, *args):
        return extract_name(node)


class IDP3Theory(TheoryVisitor):
    """ Create IDP3 theory from Theory node. """

    def visit_THEORY(self, node, *args):
        return (f'theory {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_OPTIMISE(self, node, *args):
        return (f'term {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n\t' + self.visit(node.children[2]) + '\n}')

    def visit_QUERY(self, node, *args):
        return (f'query {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n\t' + self.visit(node.children[2]) + '\n}')

    def visit_THEORYBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_CONSTRAINT(self, node, *args):
        return f'{self.visit(node.children[0])}.'

    # Formula's
    def visit_LIMP(self, node, *args):
        return ' <= '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_RIMP(self, node, *args):
        return ' => '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_EQUIV(self, node, *args):
        return ' <=> '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_NOT(self, node, *args):
        return f'~({self.visit(node.children[0])})'

    def visit_AND(self, node, *args):
        return ' & '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_OR(self, node, *args):
        return ' | '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_FORALL(self, node, *args):
        return f'!{" ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}'

    def visit_EXISTS(self, node, *args):
        if len(node.children) > 2:
            return (f'?{idp3_symbol[node.children[2][0]]}{self.visit(node.children[2][1])} ' +
                    f'{" ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}')
        return f'?{" ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}'

    # Atoms
    def visit_ATOM(self, node, *args):
        args = '(' + ','.join([self.visit(x) for x in node.children[1:]]) + ')'
        if args == '()':  # Don't bother printing empty parentheses
            args = ''
        return self.visit(node.children[0]) + args

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_CMP(self, node, *args):
        t = [self.visit(x) for x in node.children[1:]]
        for i in range(len(t)):
            try:
                if node.children[i + 1].children[0].symbol == 'MATH':
                    t[i] = t[i][1:-1]
            except AttributeError:
                pass

        return f' {idp3_symbol[node.children[0]]} '.join(t)

    def visit_BOOL(self, node, *args):
        return idp3_symbol[extract_name(node)]

    # Terms
    def visit_TERM(self, node, *args):
        if len(node.children) > 1:
            return f'{self.visit(node.children[0])}({",".join(list(map(self.visit, node.children[1:])))})'
        return self.visit(node.children[0])

    def visit_ABS(self, node, *args):
        if node.children[0].children[0].symbol == 'MATH':
            return f'abs{self.visit(node.children[0])}'
        return f'abs({self.visit(node.children[0])})'

    def visit_MIN(self, node, *args):
        return f'MIN[:{self.visit(node.children[0])}]'

    def visit_MAX(self, node, *args):
        return f'MAX[:{self.visit(node.children[0])}]'

    def visit_MATH(self, node, *args):
        if len(node.children) > 3:
            raise NotImplementedError('Chained mathematical operations not (yet) implemented.')
        if not node.children[0]:  # UMINUS
            return f'({idp3_symbol[node.children[1]]}{self.visit(node.children[2])})'
        return f'({self.visit(node.children[0])}{idp3_symbol[node.children[1]]}{self.visit(node.children[2])})'

    def visit_MINUS(self, node, *args):
        return f'({idp3_symbol[node.symbol]}{self.visit(node.children[0])})'

    def visit_SETOBJ(self, node, *args):
        return ('{' + ' '.join(list(map(self.visit, node.children[0])))
                + ' : ' + ' : '.join(list(map(self.visit, node.children[1:]))) + '}')

    def visit_SET(self, node, *args):
        return self.visit_SETOBJ(node, args)

    def visit_AGGRSET(self, node, *args):
        return idp3_symbol[node.children[0]] + self.visit(node.children[1], args)

    def visit_AGGRLST(self, node, *args):
        return f'{idp3_symbol[node.children[0]]}({", ".join(list(map(self.visit, node.children[1:])))})'

    def visit_FUNC(self, node, *args):
        return self.visit_PRED(node, *args)

    def visit_IFTERM(self, node, *args):
        raise NotImplementedError('IFTERM not supported in IDP3')

    def visit_IFFORMULA(self, node, *args):
        raise NotImplementedError('IFFORMULA not supported in IDP3')

    # Definitions
    def visit_DEFINITION(self, node, *args):
        return '{\n\t\t' + '\n\t\t'.join(list(map(self.visit, node.children))) + '\n\t}'

    def visit_DEFIMP(self, node, *args):
        if node.children[0]:
            return f'{self.visit(Node("FORALL", node.children[0:2]))} <- {self.visit(node.children[2])}.'
        return f'{self.visit(node.children[1])} <- {self.visit(node.children[2])}.'

    # (General)
    def visit_NAME(self, node, *args):
        return node.children[0]

    def visit_VAR(self, node, *args):
        return f'{self.visit(node.children[0])}[{self.visit(node.children[1])}]'
