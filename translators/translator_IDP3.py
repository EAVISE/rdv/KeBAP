# IDP3 translator
# Authors: Kylian Van Dessel
from __future__ import annotations

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from translators.translator import Translator
from translators.idp3_visitors import IDP3Vocabulary, IDP3Structure, IDP3Theory
from specification import Specification
from blocks import Block, Vocabulary, Structure, Theory, Optimise, Query
from nodes import Node, BlockName

LATEST_VERSION = "???"

map_ = {}


def register(block):
    def decorator(func):
        map_[block] = func
        return func

    return decorator


class IDP3Translator(Translator):

    def __init__(self, version=LATEST_VERSION):
        self.version = version
        pass

    @register('VOCABULARY')
    def translate_vocabulary(self, vocab : Vocabulary | Node):
        """ Translate a vocabulary block to IDP3. """
        if isinstance(vocab, Vocabulary):
            vocab = vocab.tree
        return IDP3Vocabulary().visit(vocab)

    @register('STRUCTURE')
    def translate_structure(self, struc : Structure | Node):
        """ Translate a structure block to IDP3. """
        if isinstance(struc, Structure):
            struc = struc.tree
        return IDP3Structure().visit(struc)

    @register('THEORY')
    def translate_theory(self, theor : Theory | Node):
        """ Translate a theory block to IDP3. """
        if isinstance(theor, Block):
            theor = theor.tree
        return IDP3Theory().visit(theor)

    @register('OPTIMISE')
    def translate_optimise(self, optim : Optimise | Node):
        """ Translate an optimise (term) block to IDP3"""
        return self.translate_theory(optim)

    @register('QUERY')
    def translate_query(self, query : Query | Node):
        """ Translate a query block to IDP3"""
        return self.translate_theory(query)

    def translate(self, spec: Specification, **kwargs):
        """ Translate a Specification to the IDP3 language. """
        return '\n\n'.join(map(self.translate_block, spec()))

    def translate_block(self, block: Block):
        """ Translate a Block to the IDP3 language. """
        try:
            BlockName(block.kind)
        except:
            raise NotImplementedError(f'Cannot translate unknown "{block.kind}" block to IDP3')
        return map_[block.kind](self,block)

    def translate_node(self, node : Node):
        """ Translate a Node to the IDP3 language. """
        for k in map_.keys():
            try:
                return map_[k](self, node)
            except AttributeError:
                continue
        raise NotImplementedError(f'did not find a translation to IDP3 for node "{node.symbol}"')


if __name__ == '__main__':
    try:
        file, lang = sys.argv[1:3]
    except (IndexError, ValueError):
        try:
            file = sys.argv[1]
        except IndexError:
            print('Nothing to do, there was no file provided.')
            exit(0)
        else:
            print('No input language provided for file, trying to guess language from file extension instead.')
            lang = file.split('.')[-1]
    lang = lang.upper()

    import parsers
    parser = getattr(getattr(parsers, f'parser_{lang}'), f'{lang}Parser')()
    print(f'Parsing {lang} specification from file \'{file}\'.')
    myspec = Specification(parser.parse_file(file))
    print(f'Translating specification to IDP3 language.')
    print(IDP3Translator().translate(myspec))
    exit(0)
