# Translator abstract interface class
# authors: Kylian Van Dessel

from abc import ABC, abstractmethod


class Translator(ABC):
    """ Abstract class to implement translators for different (output) languages
    Methods:
        translate(<Specification>)
    """
    # TODO add specification to init to do other things than translating ?
    #  e.g. move conversions such as a normal form rewrite here as well?
    #  (but then a part of the modeller functionality ends up here...)
    #  def __init__(self, spec: Specification):
    #    self.spec = spec

    @abstractmethod
    def translate(self, spec, **kwargs):
        """ Translate specification to output language representation. """
        pass


if __name__ == '__main__':
    pass
