# ASP translator
# Authors: Kylian Van Dessel

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from specification import Specification
from translators.translator import Translator
from translators.asp_visitors import ASPGenerateDefine, ASPTest


class ASPTranslator(Translator):
    @staticmethod
    def to_generate(vocab, struc=None):
        """ Convert Vocabulary to ASP generate part, takes into account already
                defined predicates in Structure (if any) . """
        return ASPGenerateDefine(struc).visit(vocab.tree)

    @staticmethod
    def to_define(struc):
        """ Convert Structure to ASP define part. """
        return ASPGenerateDefine().visit(struc.tree)

    @staticmethod
    def to_test(theor, vocab=None, struc=None):
        """ Convert Theory to ASP test part. """
        return ASPTest(vocab, struc).visit(theor.tree)

    @staticmethod
    def to_optimise(theor, vocab=None, struc=None, minimise_else_maximise: bool = True):  # TODO
        return ASPTest(vocab, struc).visit(theor.tree, minimise_else_maximise)

    def translate_blocks(self, vocab=None, struc=None, theor=None, optim=None, minimise_else_maximise: bool = True):
        """ Translate a specification (blocks) to ASP. """
        print('warning: translating a specification based on separate blocks only works if '
              'these are compatible with ASP (e.g. they do not contain function symbols, etc.')
        repr = []
        if struc:
            repr.append(self.to_define(struc))
        if vocab:
            repr.append(self.to_generate(vocab, struc))
        if optim:
            # TODO CHECKOUT might not work properly
            repr.append(self.to_optimise(theor, vocab, struc, minimise_else_maximise))
        elif theor:
            repr.append(self.to_test(theor, vocab, struc))
        return '\n\n'.join(repr)

    def translate(self, spec: Specification, overwrite=False):
        """ Translate a Specification to the ASP language. """
        # TODO TBD make copy of spec and return new one? or provide arg option to overwrite or not?
        #  maybe both... make a copy and only replace if completely succesful *and* an overwrite is desired
        if not overwrite:
            import copy
            spec = copy.deepcopy(spec)
        to_asp_compatible(spec)
        to_asp_naming_conventions(spec)
        repr = filter(None, [self.to_define(spec.structure) if spec.structure else None,
                             self.to_generate(spec.vocabulary, struc=spec.structure) if spec.vocabulary else None,
                             self.to_test(spec.theory, vocab=spec.vocabulary, struc=spec.structure) if spec.theory else None,
                             # self.translate_term(spec.optimise) if spec.optimise else None  # TODO
                             ])
        return '\n\n'.join(repr)


# TODO move this to a series of naming convetions in manipulators/ and call proper convention
def to_asp_naming_conventions(spec):
    """ Change symbol names to match ASP naming conventions. """
    spec.symbol_map = spec.vocabulary.match_asp_naming_convention(spec.vocabulary)
    spec.theory.match_asp_naming_convention(spec.symbol_map, create_new_map=False)
    spec.structure.match_asp_naming_convention(spec.symbol_map, create_new_map=False)
    if spec.optimise:
        spec.optimise.match_naming_convention(spec.symbol_map, create_new_map=False)


# TODO idem previous
def to_asp_compatible(spec: Specification):
    """ Normalize the specification so that it can be run by asp solver. """
    # match symbol occurrences with vocabulary symbols and add typing info
    match = spec.vocabulary.match_vocabulary(spec.vocabulary)
    for block in filter(None, [spec.structure, spec.theory, spec.optimise]):
    #for block in spec():
        block.match_vocabulary(match, create_new_matcher=False)

    # normalize constraints
    spec.theory.normalize()

    # convert function symbol( occurrence)s to predicate symbol
    spec.structure.convert_functions()
    spec.theory.convert_functions(spec.vocabulary)  # use vocab to get typing info of function symbols  # TODO check if usage of vocabulary is still necessary after matching vocab:
    spec.converted_functions = spec.vocabulary.convert_functions(spec.theory)  # add implied constraints to theory
    if spec.optimise:
        spec.optimise.convert_functions(spec.vocabulary)

    # match new symbols
    match = spec.vocabulary.match_vocabulary(spec.vocabulary)
    for block in filter(None, [spec.structure, spec.theory, spec.object_value]):
        block.match_vocabulary(match, create_new_matcher=False)

    # normalize again
    spec.theory.normalize()
    if spec.optimise:
        spec.optimise.normalize()


if __name__ == '__main__':
    try:
        file, lang = sys.argv[1:3]
    except (IndexError, ValueError):
        print('warning: No file or input language provided, using test file instead.')
        file, lang = './ctd/idp3_dummy_nofun.idp3', 'IDP3'

    print(f'Parsing specification from {lang} language, file: \'{file}\'.')
    import parsers
    parser = getattr(getattr(parsers, f'parser_{lang}'), f'{lang}Parser')()
    spec = parser.parse_file(file)

    print(f'Translating specification to ASP language.')
    repr = ASPTranslator().translate(spec)
    print(repr)
    exit(0)
