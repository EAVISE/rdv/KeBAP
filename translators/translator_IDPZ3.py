# IDP-Z3 translator
# Authors: Kylian Van Dessel, Robin De Vogelaere
from __future__ import annotations

import os
import sys
import warnings

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from translators.translator import Translator
from translators.idpz3_visitors import IDPZ3Vocabulary, IDPZ3Structure, IDPZ3Theory
from specification import Specification
from blocks import Block, Vocabulary, Structure, Theory
from nodes import Node, BlockName

LATEST_VERSION = "OCT24"

map_ = {}


def register(block):
    def decorator(func):
        map_[block] = func
        return func

    return decorator


class IDPZ3Translator(Translator):

    def __init__(self, version=LATEST_VERSION):
        self.version = version
        pass

    @register('VOCABULARY')
    def translate_vocabulary(self, vocab: Vocabulary | Node):
        """ Translate a vocabulary block to IDP-Z3. """
        if isinstance(vocab, Vocabulary):
            vocab = vocab.tree
        return IDPZ3Vocabulary().visit(vocab)

    @register('STRUCTURE')
    def translate_structure(self, struc: Structure | Node):
        """ Translate a structure block to IDP-Z3. """
        if isinstance(struc, Structure):
            struc = struc.tree
        return IDPZ3Structure().visit(struc)

    @register('THEORY')
    def translate_theory(self, theor: Theory | Node):
        """ Translate a theory block to IDP-Z3. """
        if isinstance(theor, Theory):
            theor = theor.tree
        return IDPZ3Theory().visit(theor)

    #@register('OPTIMISE')
    #def translate_optimise(self, optim):
    #    """ (!) Term block for optimisation not supported by IDP-Z3. """
    #    warnings.warn('the term block (for optimisation) is not translated to IDP-Z3, '
    #                  'functionality should be addressed through API or procedure instead.')
    #    return ''

    #@register('QUERY')
    #def translate_query(self, query):
    #    """ (!) Query block not supported by IDP-Z3. """
    #    warnings.warn('the query block is not translated to IDP-Z3, '
    #                  'functionality should be addressed through API or procedure instead.')
    #    return ''

    def translate(self, spec: Specification, **kwargs):
        """ Translate a Specification to the IDP-Z3 language. """
        return '\n\n'.join(filter(None, map(self.translate_block, spec())))

    def translate_block(self, block: Block):
        """ Translate a Block to the IDP-Z3 language. """
        try:
            BlockName(block.kind)
        except:
            raise NotImplementedError(f'Cannot translate unknown "{block.kind}" block to IDP-Z3')
        if block.kind in ["QUERY", "OPTIMISE"]:
            warnings.warn(f'Cannot translate {block.kind} block to IDP-Z3, but I might be able to ignore it...')
            return ''
        return map_[block.kind](self,block)

    def translate_node(self, node : Node):
        """ Translate a Node to the IDP-Z3 language. """
        for k in map_.keys():
            try:
                return map_[k](self, node)
            except AttributeError:
                continue
        raise NotImplementedError(f'Did not find a translation to IDP-Z3 for node "{node.symbol}"')


if __name__ == '__main__':
    try:
        file, lang = sys.argv[1:3]
    except (IndexError, ValueError):
        try:
            file = sys.argv[1]
        except IndexError:
            print('Nothing to do, there was no file provided.')
            exit(0)
        else:
            print('No input language provided for file, trying to guess language from file extension instead.')
            lang = file.split('.')[-1]
    lang = lang.upper()

    import parsers
    parser = getattr(getattr(parsers, f'parser_{lang}'), f'{lang}Parser')()
    print(f'Parsing {lang} specification from file \'{file}\'.')
    myspec = Specification(parser.parse_file(file))
    print(f'Translating specification to IDP-Z3 language.')
    print(IDPZ3Translator().translate(myspec))
    exit(0)
