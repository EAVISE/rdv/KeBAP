# IDP-Z3 representation visitors
# Authors: Robin De Vogelaere, Kylian Van Dessel

import os
import sys
import warnings
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from nodes import Node, Symbol, extract_name
from visitors import VocabularyVisitor, StructureVisitor, TheoryVisitor

# TODO point to symbol map in lex
idpz3_symbol = {'EQ': '=', 'NEQ': '~=', 'LT': '<', 'GT': '>', 'LEQ': '=<', 'GEQ': '>=',
                'LIMP': '<=', 'RIMP': '=>', 'EQUIV': '<=>',
              'CNT': '#', 'SUM': 'sum', 'MIN': 'min', 'MAX': 'max', 'PROD': 'prod',
              'ABS': 'abs', 'PLUS': '+', 'MINUS': '-', 'TIMES': '*', 'DIVIDE': '/', 'MODULO': '%',
              'TRUE': 'true', 'FALSE': 'false'}


class IDPZ3Vocabulary(VocabularyVisitor):
    """ Create IDP vocabulary from Vocabulary node. """

    def visit_VOCABULARY(self, node, *args):
        return (f'vocabulary {self.visit(node.children[0])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_VOCABULARYBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_DECLARATION(self, node, *args):
        symb = self.visit(node.children[0])
        if not isinstance(symb, list):
            symb = [symb]
        try:
            symb.insert(1, self.visit(node.children[1]))
        except IndexError:
            pass
        return ' '.join(symb)

    def visit_PRED(self, node, *args):
        p=Symbol(node)
        if p.is_type():
            return [f'type {p.name}'] + list(filter(None, [self.visit(x, *args) for x in node.children if x.symbol != 'NAME']))
        try:
            typing = " * ".join(p.typing) + ' '
        except AttributeError: # predicate without typing
            typing = ''
        return f'{p.name}: {typing}-> {p.return_type}'

    def visit_FUNC(self, node, *args): return self.visit_PRED(node, *args)

    def visit_PFUNC(self, node, *args): raise NotImplementedError

    def visit_PRED_ENUM(self, node, *args):
        # TODO add support for constructed from (which does apparently still exist in IDP-Z3)
        return f':= {{ {", ".join(list(map(self.visit, node.children)))} }}'

    # TODO IDPZ3 NEWVERSION
    #def visit_PRED_ENUM  # TODO laat ASSIGN en SUPERSET symbool teruggeven, i.e. := of :>
        #return f'{idpz3_symbol[node.symbol?self.visit(node.children[?])?]} {{ {", ".join(list(map(self.visit, node.children)))} }}'

    def visit_TYPING(self, node, *args):
        return [self.visit(x) for x in node.children]

    def visit_TUPLE(self, node, *args):
        return self.visit(node.children[0])

    def visit_TERM(self, node, *args):
        return self.visit(node.children[0])

    def visit_NAME(self, node, *args):
        return extract_name(node)

    def visit_SUBSET(self, node, *args):
        return f'<: {self.visit(node.children[0])}'

    # TODO IDPZ3 NEWVERSION
    #def visit_SUPERSET(self, node, *args):
    #    return f':>'
    #    return f':> {self.visit(node.children[0])}'

    def visit_BUILTIN(self, node, *args):  # TODO what to do with builtins?
        return ''
        #return self.visit_SUBSET(node, *args)  # (returns '<: int')

    def visit_ARITY(self, node, *args):
        return ''

class IDPZ3Structure(StructureVisitor):
    """ Create IDP-Z3 structure from structure node. """

    def visit_STRUCTURE(self, node, *args):
        return (f'structure {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_STRUCTUREBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_ASSIGNMENT(self, node, *args):
        try:
            arity = Symbol(node.children[0]).arity
        except AttributeError:
            arity = None
        assign = [self.visit(x, arity) for x in node.children]
        return f'{assign[0]} := {"".join(assign[1:])}.'

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_PRED_ENUM(self, node, *args):
        if len(node.children) == 0 and args[0] == 0:  # boolean 'false' (='{}')
            return 'false'
        elif len(node.children) == 1 and len(node.children[0].children) == 0:  # boolean 'true' (='{()}')
            return 'true'
        return '{' + ', '.join(list(map(self.visit, node.children))) + '}'

    def visit_FUNC(self, node, *args):
        return self.visit(node.children[0])

    def visit_FUNC_ENUM(self, node, *args):
        # TODO CHECKOUT situation of nested if statements
        if len(node.children) == 1:
            if len(node.children[0].children) == 1:
                return self.visit(node.children[0])
        return '{' + ', '.join(list(map(lambda x: self.visit(x)[::-1].replace(',', ' >- )', 1)[:0:-1]
                if len(x.children) > 2 else self.visit(x).strip('()').replace(',', ' -> '), node.children))) + '}'
        # lambda: tuple contains brackets (e.g. (a, b, c)). If tuple contains more than two items, replace the last
        # ',' with ') ->' and ignore the final sign (i.e. the closing bracket of the original tuple.
        # If the tuple contains only two arguments, no brackets are needed (e.g. a -> b) and the only ',' is replaced.

    def visit_ELSE(self, node, *args):
        return f" else {self.visit(node.children[0])}"

    def visit_NVALUED(self, node, *args):
        # TODO check how to deal with if and elif statements (order) + is this still supported in IDPZ3?
        if node.children[0] is True:
            return '<ct>'
        elif node.children[0] is False:
            return '<cf>'
        return ''

    def visit_TUPLE(self, node, *args):
        items = list(map(self.visit, node.children))
        if len(items) == 0:
            return ''
        elif len(items) == 1:
            return str(items[0])
        else:
            return '(' + ','.join(items) + ')'

    def visit_BOOL(self, node, *args):
        return idpz3_symbol[node.children[0]]

    def visit_NAME(self, node, *args):
        return extract_name(node)


class IDPZ3Theory(TheoryVisitor):
    """ Create IDP theory from Theory node. """

    def visit_THEORY(self, node, *args):
        return (f'theory {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_OPTIMISE(self, node, *args):
        # TODO what to do with this? I think, the proper way is to make sure any specification is compatible with te translation language
        #  (i.e., it doesn't contain any visit_NODE's that are not supported -> this block can simply be omitted them)
        warnings.warn('IDP-Z3 does not support a separate term block for optimisation, simply ignored')
        return ''

    def visit_THEORYBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_CONSTRAINT(self, node, *args):
        return f'{self.visit(node.children[0])}.'

    # Formula's
    # TODO AVOID ERRORS BY USING SAME SYMBOL MAP AS IN LEX/YACC ? !!! -> change IDP3 and IDPZ3 Translators
    def visit_RIMP(self, node, *args):
        #return ' => '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))
        return f' {idpz3_symbol[node.symbol]} '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_LIMP(self, node, *args):
        # return ' <= '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))
        return self.visit_RIMP(node, *args)

    def visit_EQUIV(self, node, *args):
        #return ' <=> '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))
        return self.visit_RIMP(node, *args)

    def visit_IFFORMULA(self, node, *args):
        return (f"if {self.visit(node.children[0])} then {self.visit(node.children[1])} "
                f"else {self.visit(node.children[2])}")

    def visit_NOT(self, node, *args):
        return f'~({self.visit(node.children[0])})'

    def visit_AND(self, node, *args):
        return ' & '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_OR(self, node, *args):
        return ' | '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_FORALL(self, node, *args):
        # TODO (low priority): if same type, only mention once: !x, y in T, instead of x in T, y in T.
        return f'!{", ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}'

    def visit_EXISTS(self, node, *args):
        if len(node.children) > 2:
            raise NotImplementedError('Generalised existential quantifiers not supported in IDP-Z3')
        return f'?{", ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}'

    # Atoms

    def visit_ATOM(self, node, *args):
        if node.children[0].symbol == "PRED":
            return self.visit(node.children[0]) + '(' + ','.join([self.visit(x) for x in node.children[1:]]) + ')'
        return self.visit(node.children[0])

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_CMP(self, node, *args):
        t = [self.visit(x) for x in node.children[1:]]
        for i in range(len(t)):
            try:
                if node.children[i + 1].children[0].symbol == 'MATH':
                    t[i] = t[i][1:-1]
            except AttributeError:
                pass

        return f' {idpz3_symbol[node.children[0]]} '.join(t)

    def visit_BOOL(self, node, *args):
        return idpz3_symbol[extract_name(node)]


    # Terms

    def visit_TERM(self, node, *args):
        if node.children[0].symbol in ["FUNC", "PFUNC"]:
            if len(node.children) > 1:
                return f'{self.visit(node.children[0])}({",".join(list(map(self.visit, node.children[1:])))})'
            return f'{self.visit(node.children[0])}()'
        if len(node.children) > 1:
            # TODO check if this occurs in any other case than w. function symbols.
            print('debug: I was looking for this occurrence in visit_TERM of visitors_IDPZ3: ', str(node))
            return f'{self.visit(node.children[0])}({",".join(list(map(self.visit, node.children[1:])))})'
        return f"{self.visit(node.children[0])}"

    def visit_IFTERM(self, node, *args):
        self.visit_IFFORMULA(node, *args)

    def visit_ABS(self, node, *args):
        if node.children[0].children[0].symbol == 'MATH':
            return f'abs{self.visit(node.children[0])}'
        return f'abs({self.visit(node.children[0])})'

    def visit_MIN(self, node, *args):  # for compatibility with IDP3
        return f"min{{__x | __x in {self.visit(node.children[0])}}}"

    def visit_MAX(self, node, *args):  # for compatibility with IDP3
        return f"max{{__x | __x in {self.visit(node.children[0])}}}"

    def visit_MATH(self, node, *args):
        # TODO chained mathematical operations
        if len(node.children) > 3:
            raise NotImplementedError('Chained mathematical operations not (yet) implemented.')
        if not node.children[0]:  # UMINUS
            return f'({idpz3_symbol[node.children[1]]}{self.visit(node.children[2])})'
        return f'({self.visit(node.children[0])}{idpz3_symbol[node.children[1]]}{self.visit(node.children[2])})'

    def visit_MINUS(self, node, *args):
        return f'({idpz3_symbol[node.symbol]}{self.visit(node.children[0])})'

    def visit_SETOBJ(self, node, *args):
        return ('{' + self.visit(node.children[2]) + '|' + ", ".join(list(map(self.visit, node.children[0])))
                + ' : ' + self.visit(node.children[1]) + '}')

    def visit_SET(self, node, *args):
        return '{' + ", ".join(list(map(self.visit, node.children[0]))) + ' : ' + self.visit(node.children[1]) + '}'

    def visit_AGGRSET(self, node, *args):
        if node.children[0] == "SUM":  # sum adds extra {}
            return idpz3_symbol[node.children[0]] + '{' + self.visit(node.children[1], *args) + '}'
        return idpz3_symbol[node.children[0]] + self.visit(node.children[1], *args)

    def visit_AGGRLST(self, node, *args):
        # TODO supported by IDP-Z3? else find a way to convert or raise NIE
        warnings.warn('IDP-Z3 support for aggregate list unknown, might not work properly.')
        return f'{idpz3_symbol[node.children[0]]}({", ".join(list(map(self.visit, node.children[1:])))})'

    #def visit_FUNC(self, node, *args):
    #    func = self.visit(node.children[0])
    #    if Symbol(node).arity == 0:
    #        func += '()'
    #    return func

    def visit_FUNC(self, node, *args):
        return self.visit_PRED(node, *args)

    # Definitions
    def visit_DEFINITION(self, node, *args):
        return '{\n\t\t' + '\n\t\t'.join(list(map(self.visit, node.children))) + '\n\t}'

    def visit_DEFIMP(self, node, *args):
        if node.children[0]:
            return f'{self.visit(Node("FORALL", node.children[0:2]))} <- {self.visit(node.children[2])}.'
        return f'{self.visit(node.children[1])} <- {self.visit(node.children[2])}.'

    # (General)
    def visit_NAME(self, node, *args):
        return node.children[0]

    def visit_VAR(self, node, *args):
        return f'{self.visit(node.children[0])} in {self.visit(node.children[1])}'
