Keep track of progress/improvements, e.g. added functionality support or error 
reduction for test bench, by including the test result files 'log.csv'* and 
'run.txt' in `./progress/` for the following test:

    ```python test_bench/test_bench.py -p idpz3 idp3 csv -t idpz3 idp3 asp -x idpz3 -v 2 -c```

Add underscore commit date(?) to filename, e.g. 'log_240101.txt'

*log.csv can be added to log_progress.ods as a new sheet as well as to the 
 comparison sheet (to see changes in progress immediately).

-------------------------------------------------------------------------------

Add *only* relevant testing files to the appropriate language subdirectory of 
`./test_bench/` for executing them during test fase. Create new subdirectories 
as needed, and update test command above(!).
Random test files and any other files can be added to `./test_files/` instead.

-------------------------------------------------------------------------------

(!) Important note (!)
For now, test_bench.py is still run on 'old' test_files. (Until the test bench 
suite is extensive enough.)
