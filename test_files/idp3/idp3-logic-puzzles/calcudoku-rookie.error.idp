/*  
 * calcudoku-rookie.idp (aka. kenken) (limited to only summations)
 * kylian van dessel
 */

vocabulary V {
    type Row isa int
    type Column isa int
    type Value isa int
    type Sum isa int
    type Id
    Group(Row, Column): Id		
    Sum(Id): Sum
    
    Given(Row, Column, Value)		
    Solution(Row, Column): Value
}

structure S : V {
    Row = { 0..4 }
    Column = { 0..4 }    
    Value = { 1..5 }
    Sum = { 1..25 }
    Id = { 1..10 }
    Group = {
        0,0->1;  0,1->2;  0,2->2;  0,3->2;  0,4->3;
        1,0->1;  1,1->1;  1,2->4;  1,3->4;  1,4->3;
        2,0->5;  2,1->5;  2,2->6;  2,3->6;  2,4->7;
        3,0->8;  3,1->5;  3,2->6;  3,3->9;  3,4->7;
        4,0->8;  4,1->8;  4,2->10; 4,3->10; 4,4->10;
    }
    Sum = { 1->10; 2->6; 3->7; 4->7; 5->11; 6->7; 7->6; 8->6; 9->4; 10->11; }
    
    Given = {}    
}

theory T : V {
	// given numbers are part of solution
    !r[Row] c[Column] n[Value]: Given(r, c, n) => Solution(r, c) = n.

    // numbers in the same row/column should be different
    !r[Row] c1[Column] c2[Column]: c1~=c2 => Solution(r, c1) ~= Solution(r, c2).
    !r1[Row] r2[Row] c[Column]: r1~=r2 => Solution(r1, c) ~= Solution(r2, c).

    // number per group should add up to sum
    !g[Id]: sum{r[Row] c[Column]: Group(r, c)=g: Solution(r, c)} = Sum(g).
}

procedure main() {
    stdoptions.nbmodels=3         
    printmodels(modelexpand(T,S))    
}
