/*  
 * hidato.idp 
 * kylian van dessel
 */

vocabulary V {
    type Row isa int
    type Column isa int
    type Value isa int
    Wall(Row, Column)

    Given(Row, Column, Value)
    Solution(Row, Column): Value
}

structure S : V {
    Row = { 0..7 }
    Column = { 0..7 }
    Value = { 0..40 }
    Wall = {
        						 0,5; 0,6; 0,7;
        						 1,5; 1,6; 1,7;
        							  2,6; 2,7;
        							  3,6; 3,7;
        								   4,7;
        5,0; 5,1; 						   5,7;
        6,0; 6,1; 6,2; 6,3;
        7,0; 7,1; 7,2; 7,3; 7,4; 7,5;
    }

    Given = { 
        		0,1,33; 0,2,35; 
        				1,2,24; 1,3,22; 
        						2,3,21;
        		3,1,26; 		3,3,13; 3,4,40; 3,5,11; 
        4,0,27; 						4,4,9;  		4,6,1;
        								5,4,18;
        										6,5,7;
        												7,6,5;
    }
}

theory T : V {  
    // given values are part of solution
    !r[Row] c[Column] n[Value]: Given(r, c, n) => Solution(r, c) = n.
    
    // walls don't have a value (,all other cells should be different from zero)
    !r[Row] c[Column]: Wall(r,c) <=> Solution(r, c) = 0.
    
    // consecutive numbers should be adjacent
    !r[Row] c[Column] r2[Row] c2[Column]: Solution(r, c) ~= 0 & Solution(r2, c2) = Solution(r, c) + 1 => abs(r2-r) =< 1 & abs(c2-c) =< 1.
    
    // each number should occur (exactly) once
    !n[Value]: n~=0 => ?r[Row] c[Column]: Solution(r, c) = n.
}

procedure main() {
    stdoptions.nbmodels = 3	
    printmodels(modelexpand(T,S))
}	
