A separate directory is provided to collect sample files for any issues. Please 
add them to `./_issues` to clarify created issues on Git and please adhere to 
the following naming convention: 'iss<issue-number>.<fileext>', e.g. 'iss5.idp',
or 'iss<issue-number>_<nb>.<fileext>' for multiple files, e.g. 'iss7_1.idp3'.
