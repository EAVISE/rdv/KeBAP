# IDPZ3 runner
# Robin De Vogelaere, Kylian Van Dessel

import os
import sys
import idp_engine
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from runners.runner import Runner
from specification import Specification
from blocks import Structure
from parsers.parser_IDPZ3 import IDPZ3Parser, YaccError
from translators.translator_IDPZ3 import IDPZ3Translator


class IDPZ3Error(Exception): pass
class IDPZ3UnsatError(IDPZ3Error): pass
class IDPZ3LoadError(IDPZ3Error): pass
class IDPZ3TimeoutError(IDPZ3Error): pass


class IDPZ3Runner(Runner):
    def __init__(self, spec:Specification, max_models:int = 10, timeout:int = 10, raise_unsat:bool = False):
        self.specification = spec
        self.max_models = max_models
        self.timeout_seconds = timeout
        self.raise_unsat = raise_unsat
        self.complete = False

    def __str__(self):
        return self.translate()

    def translate(self):
        """ Return IDP representation of specification. """
        return IDPZ3Translator().translate(self.specification)

    # ------------------------------------------------------------------------------------------------------------------

    def get_decision_table(self, goal_string='', timeout_seconds=20, max_rows=50, first_hit=False, verify=False):
        """Get decision table from IDP API. Note: first hit default changed to False"""
        t_name = self.theory.name
        s_name = self.structure.name
        t, s = idp_engine.IDP.from_str(str(self)).get_blocks(f"{t_name}, {s_name}")
        theory = idp_engine.Theory(t, s, extended=True)
        return theory.decision_table(goal_string=goal_string, timeout_seconds=timeout_seconds,
                                     max_rows=max_rows, first_hit=first_hit, verify=verify)

    def get_dt_alternative(self, goal_string='', timeout_seconds=20, max_rows=50, first_hit=True, verify=False):
        """Returns rules as string, whereas get_decision_table returns lists with the terms as string."""
        t_name = self.theory.name
        s_name = self.structure.name
        t, s = idp_engine.IDP.from_str(str(self)).get_blocks(f"{t_name}, {s_name}")
        return idp_engine.decision_table(t, s, goal_string=goal_string, timeout_seconds= timeout_seconds,
                                         max_rows=max_rows, first_hit=first_hit, verify=verify)

    # Reasoning

    def _get_blocks(self):
        """ Return idp engine theory and structure block object, resp. """
        t_name = self.theory.name
        s_name = self.structure.name
        try:
            t, s = idp_engine.IDP.from_str(str(self)).get_blocks(f"{t_name}, {s_name}")
        except Exception as e:
            raise IDPZ3LoadError(e)
        return t, s

    def _get_theory(self):
        """ Return idp engine Theory object. """
        try:
            idp_theory = idp_engine.Theory(*self._get_blocks())
        except Exception as e:
            raise IDPZ3LoadError(e)
        return idp_theory

    def _get_theory_optimise(self, term, minimise=True):
        """ Return idp engine Theory object for optimising term (minimisation or maximisation). """
        try:
            idp_theory = self._get_theory().optimize(term, minimise)
        except AssertionError as e:  # if optimisation term not found
            raise IDPZ3LoadError(e)
        return idp_theory

    @staticmethod
    def _get_sat(msg, raise_unsat=False):
        """ Return satisfiability (SAT/UNSAT/TIMEOUT) of idp engine output. """
        if msg == "No models.":
            if raise_unsat:
                raise IDPZ3UnsatError(msg)
            return "UNSAT"
        elif msg.startswith("No model found in"):
            if raise_unsat:
                raise IDPZ3TimeoutError(msg)
            return "TIMEOUT"
        return "SAT"

    @staticmethod
    def run_from_file(file, t_name:str = 'T', s_name:str = 'S', optimise = None, max_models=10, timeout_seconds=10, raise_unsat=False):
        """ Run IDP-Z3 system from file. """
        with open(file) as f:
            idp = f.read()
        try:
            t, s = idp_engine.IDP.from_str(idp).get_blocks(f"{t_name}, {s_name}")
            idp_theory = idp_engine.Theory(t, s)
        except AttributeError as e:
            try:
                # Allows theory block without structure block
                t, = idp_engine.IDP.from_str(idp).get_blocks(f"{t_name}")
                idp_theory = idp_engine.Theory(t)
            except AttributeError as e:
                # TODO doesn't (only) catch the error you think... pls catch them separately
                #raise IDPZ3LoadError(e, 'theory or structure name does not match any block')
                raise IDPZ3LoadError(e)

        if optimise:
            idp_theory = idp_theory.optimise(optimise[1], optimise[0])
        models = list(map(str, idp_theory.expand(max_models, timeout_seconds)))
        models, message = models[:-1], models[-1].strip()
        sat = IDPZ3Runner._get_sat(message, raise_unsat)
        return models, sat, message

    def _structure_generator(self, idp_theory):
        """ Run IDP-Z3 system on idp engine Theory object.
             Return Structure object for every model returned by idp engine, satisfiability,
             and message returned by idp engine
        """

        # run idp system
        models = list(idp_theory.expand(self.max_models, self.timeout_seconds, self.complete))
        # final item in IDP's expand is a string with information on additional models.

        # messages and sat assertion
        models, message = models[:-1], models[-1].strip()
        sat = self._get_sat(message, self.raise_unsat)

        # create structure blocks from idp system output models
        structures = []
        for index, model in enumerate(models):
            try:
                blocklist = IDPZ3Parser().parse_string(f'structure S_{index}:V{{' + model.__str__() + '}')
                block = blocklist[0]
            except YaccError as e:
                raise IDPZ3LoadError(e)
            structures.append(block)
        return structures, sat, message
    # TODO this used to be a boolean. Should this be changed back?

    def model_expand(self, max_models="DFLT", timeout_seconds="DFLT", complete="DFLT", raise_unsat="DFLT"):
        """ Calls the model expand function from the IDP engine
            Optional additional arguments provided for similarity to IDP method of the same name"""
        # TODO automatically add type enumerations from starting structure to results
        self.set_tings(max_models=max_models, timeout_seconds=timeout_seconds,
                       complete=complete, raise_unsat=raise_unsat)  # update settings
        idp_theory = self._get_theory()  # get idp system theory
        return self._structure_generator(idp_theory)

    def model_optimise(self, term, minimise=True,
                       max_models="DFLT", timeout_seconds="DFLT", complete="DFLT", raise_unsat="DFLT"):
        """ Calls the optimisation function from the IDP engine. """
        if term[-1] != ')':  # add parentheses if not already present, i.e., term_name -> term_name()
            term = term + '()'
        self.set_tings(max_models=max_models, timeout_seconds=timeout_seconds,
                          complete=complete, raise_unsat=raise_unsat)  # update settings
        idp_theory = self._get_theory_optimise(term, minimise=minimise)  # get idp system theory
        return self._structure_generator(idp_theory)

    def model_check(self):
        """ Calls the model check function from the IDP engine. """
        return idp_engine.model_check(*self._get_blocks())

    def propagate(self, sort=False):
        """ Calls the model propagation function from the IDP engine. """
        return idp_engine.model_propagate(*self._get_blocks(), sort=sort)


if __name__ == '__main__':
    print('Test IDP-Z3 runner')
    file = "./ctd/idpz3_dummy.idpz3"

    # test static run
    my_run = IDPZ3Runner.run_from_file(file)[0][0]
    print(f'\nstatic run: \n{my_run}')

    # test runner
    my_spec = Specification.from_file("IDPZ3", file)
    print(f'\nspecification overview: \n{my_spec}')
    runner = IDPZ3Runner(my_spec)
    my_run = runner.model_check()
    print(f'\nmodel check : \n{my_run}')
    my_run = runner.model_expand(max_models=3, raise_unsat=True)
    print(f'\nmodel expand with settings {runner.settings} : \n{my_run}')
    my_run = runner.model_optimise('aantalKleuren', max_models=2, timeout_seconds=5)
    print(f'\nmodel optimise with settings {runner.settings} : \n{my_run}')
