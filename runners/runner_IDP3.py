# IDP3 runner
# Authors: Kylian Van Dessel

import os
import sys
import subprocess
import re
from tempfile import TemporaryDirectory
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from runners.runner import Runner
from specification import Specification
from blocks import Structure
from parsers.parser_IDP3 import IDP3Parser, YaccError
from translators.translator_IDP3 import IDP3Translator

idp3_engine = os.path.abspath(__file__).rsplit('/', 1, )[0] + "/idp3/bin/idp"


class IDP3Error(Exception):
    pass


class IDP3LoadError(IDP3Error):
    pass


class IDP3Runner(Runner):
    def __init__(self, spec:Specification, max_models:int = 10, timeout:int = 10, raise_unsat:bool = False):
        self.specification = spec
        self.max_models = max_models
        self.timeout_seconds = timeout
        self.raise_unsat = raise_unsat

    def __str__(self):
        return self.translate()

    def translate(self):
        """ Return IDP representation of specification. """
        return IDP3Translator().translate(self.specification)

    # ------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def _get_sat(msg, raise_unsat=False):
        """ Return satisfiability (SAT/UNSAT/TIMEOUT) of idp engine output. """
        if msg == "Unsatisfiable":
            if raise_unsat:
                raise IDP3Error(msg)
            return "UNSAT"
        elif msg.startswith("No model found in"):  # TODO check messgae for timout in idp3!
            if raise_unsat:
                raise IDP3Error(msg)
            return "TIMEOUT"
        return "SAT"

    @staticmethod
    def _run_idp3system(idp:str, is_file:bool = False, raise_unsat=False):
        """ Run IDP3 system (on string repr. or file). """
        # preprocessing -- get idp file
        if not is_file:
            tmpdir = TemporaryDirectory()
            tmpfile = tmpdir.name + 'tmp.lp'
            with open(tmpfile, 'w') as f:
                f.write(idp)
            idp = tmpfile

        # run -- system call
        out = subprocess.run([idp3_engine, idp], capture_output=True)

        # postprocessing -- clean up
        if not is_file:
            tmpdir.cleanup()

        # result -- messages and sat assertion
        if not out.stdout:
            e = '\n'.join([s for s in out.stderr.decode().strip().split('\n') if not s.startswith('Warning: Verify')])
            raise IDP3Error(e)
        message, models = out.stdout.decode().split('\n', 1)
        # TODO remove autocompleting warning from msg
        # TODO create list of msg's
        sat = IDP3Runner._get_sat(message, raise_unsat)
        if sat != "SAT":
            return [], sat, message

        # create structure blocks from idp system output models (if any)
        models = re.split('Model [0-9]+\n=======\n', models)[1:]
        structures = []
        for index, model in enumerate(models):
            try:
                block = Structure(IDP3Parser().parse_string(model.replace('structure', f'structure S_{index}'))[0])
            except YaccError as e:
                raise IDP3LoadError(e)
            structures.append(block)
        return structures, sat, message

    def _create_prodedure(self, task):
        options = "\n\t".join(["stdoptions.verbosity.groundingstats=1", "stdoptions.verbosity.solvingstats=1",
                               f"stdoptions.nbmodels={self.max_models}", f"stdoptions.timeout={self.timeout_seconds}"])
        return f'\n\nprocedure main() {{ \n\t{options} \n\tprintmodels({task}) \n}}'

    @staticmethod
    def run_from_file(file, **settings):
        msg0 = [f'warning: unused setting \'{x}={settings[x]}\', '
                    f'provide setting in procedure when running from file' for x in settings if x != 'raise_unsat']
        if 'raise_unsat' in settings:
            structures, sat, msg = IDP3Runner._run_idp3system(file, is_file=True, raise_unsat=settings['raise_unsat'])
        else:
            structures, sat, msg = IDP3Runner._run_idp3system(file, is_file=True)
        return structures, sat, [msg]+msg0

    def model_expand(self, max_models="DFLT", timeout_seconds="DFLT", raise_unsat="DFLT"):
        """ Perform model expansion with IDP3 system. """
        self.set_tings(max_models=max_models, timeout_seconds=timeout_seconds, raise_unsat=raise_unsat)
        if self.optimise:
            print(f'warning: Objective term given but not used, term will be lost: {self.optimize}')
            self.optimise = None
        idp = str(self)
        proc = self._create_prodedure(f'modelexpand({self.theory.get_name()}, {self.structure.get_name()})')
        return IDP3Runner._run_idp3system(idp + proc, raise_unsat=self.raise_unsat)

    def model_optimize(self, term=None, timeout_seconds="DFLT", raise_unsat="DFLT"):
        """ Perform optimisation with IDP3 system. """
        self.set_tings(timeout_seconds=timeout_seconds, raise_unsat=raise_unsat)
        if term:
            if self.optimise:
                print('warning: specification already had a term to optimise, this one will be overwritten now.')
            self.optimise = term
        if not self.optimise:
            raise AttributeError('nothing to optimise, maybe you forgot to provide a term or wanted a model expand?')
        idp = str(self)
        proc = self._create_prodedure(
                    f'minimize({self.theory.get_name()}, {self.structure.get_name()}, {self.optimise.get_name()}))')
        return IDP3Runner._run_idp3system(idp + proc, raise_unsat=self.raise_unsat)


if __name__ == '__main__':
    print('Test IDP3 runner')
    file = "./ctd/idp3_dummy.idp3"

    # test static run
    my_run = IDP3Runner.run_from_file(file, raise_unsat=True)
    print(f'\nstatic run: \n{my_run}')
    exit()
    # test runner
    my_spec = Specification.from_file("IDP3", file)
    print(f'\nspecification overview: \n{my_spec}')

    runner = IDP3Runner(my_spec)
    my_run = runner.model_expand(max_models=3, timeout_seconds=1)
    print(f'\nmodel expand with settings {runner.settings} : \n{my_run}')

    # TODO this one is not working yet (due to issues with idp term block (=optimise block))
    #  my_run = runner.optimise(timeout_seconds=0)
    #  print(f'\noptimise with settings {runner.settings} : \n{my_run}')
