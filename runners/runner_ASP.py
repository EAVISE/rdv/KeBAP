# ASP runner
# Kylian Van Dessel, Jo Devriendt

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from tempfile import TemporaryDirectory
from io import StringIO
from clingo.control import Control

from runners.runner import Runner
from specification import Specification
from translators.translator_ASP import ASPTranslator


class ClingoUnsatError(Exception):
    pass


class ClingoLoadError(Exception):
    pass


class ClingoGroundError(Exception):
    pass


class ASPRunner(Runner):
    def __init__(self, spec: Specification, max_models: int = 10, timeout_seconds: int = 10,
                 clingo_stats: int = 0, raise_unsat: bool = False):
        self.specification = spec
        self.converted_functions = {}  # keep track of function symbols that were converted to predicate symbols
        self.max_models = max_models
        self.timout_seconds = timeout_seconds
        self.clingo_stats = clingo_stats
        self.raise_unsat = raise_unsat

    def __str__(self):
        return self.translate()

    def translate(self):
        """ Return IDP representation of specification. """
        return ASPTranslator().translate(self.specification)

    # ------------------------------------------------------------------------------------------------------------------

    def _normalize(self):
        """ Normalize the specification so that it can be run by asp solver. """
        # match symbol occurrences with vocabulary symbols and add typing info
        match = self.vocabulary.match_vocabulary(self.vocabulary)
        for block in filter(None, [self.structure, self.theory, self.optimise]):
            block.match_vocabulary(match, create_new_matcher=False)

        # normalize constraints
        self.theory.normalize()

        # convert function symbol( occurrence)s to predicate symbol(s)
        self.structure.convert_functions()
        self.theory.convert_functions(
            self.vocabulary)  # use vocab to get typing info of function symbols  # TODO #KYL check if this is necessary after matching vocab
        self.converted_functions = self.vocabulary.convert_functions(self.theory)  # add implied constraints to theory
        if self.optimise:
            self.optimise.convert_functions(self.vocabulary)

        # match new symbols
        match = self.vocabulary.match_vocabulary(self.vocabulary)
        for block in filter(None, [self.structure, self.theory, self.optimise]):
            block.match_vocabulary(match, create_new_matcher=False)

        # normalize again
        self.theory.normalize()
        if self.optimise:
            self.optimise.normalize()

    def _convert(self):
        """ Convert the parse trees of a normalized FOL program to ASP. """
        # change symbol names to match ASP naming conventions
        self.symbol_map = self.vocabulary.match_asp_naming_convention(self.vocabulary)
        self.theory.match_asp_naming_convention(self.symbol_map, create_new_map=False)
        self.structure.match_asp_naming_convention(self.symbol_map, create_new_map=False)

    def _convert_term(self):
        # TODO fix this one
        self.optimise.match_naming_convention(self.symbol_map, create_new_map=False)
        return self.optimise
        #return self.term.to_asp(self.vocabulary)

    def make_asp_compatible(self, convert_term: bool = True):
        """ Translate existing specification (internal representation) to ASP. Skip normalisation if already done."""
        if not self.specification:
            print('warning: Nothing to do. Got an empty specification.')
            return
        self._normalize()
        self._convert()
        if convert_term and self.optimise:
            self._convert_term()

    ### Reasoning

    @staticmethod
    def _run_clingo(asp: str, is_file: bool = False, clingo_stats: int = 0, raise_unsat: bool = False):

        def raise_unsat_error(arg):
            raise ClingoUnsatError("Unsat")

        def get_stats(ctl):
            def clingo_stats_to_str(stats, k1, k2):
                s = ''
                for k3 in stats[k1][k2]:
                    s += f'{k3}: {stats[k1][k2][k3]}'
                return s

            if clingo_stats == 1:
                cs = list()
                cs.append(clingo_stats_to_str(ctl.statistics, "problem", "generator"))
                cs.append(clingo_stats_to_str(ctl.statistics, "problem", "lp"))
                cs.append(clingo_stats_to_str(ctl.statistics, "solving", "solvers"))
                cs.append(clingo_stats_to_str(ctl.statistics, "summary", "models"))
                return cs
            if clingo_stats == 2:
                from json import dumps
                return [dumps(ctl.statistics, sort_keys=True, indent=1, separators=(',', ': '))]
            return []

        # load specification
        if not is_file:
            tmpdir = TemporaryDirectory()
            tmpfile = tmpdir.name + 'tmp.lp'
            with open(tmpfile, 'w') as f:
                f.write(asp)
            asp = tmpfile
        ctl = Control()  # Type: Control
        try:
            ctl.load(asp)
        except RuntimeError as e:
            raise ClingoLoadError(e)

        # run system
        try:
            ctl.ground([("base", [])])
        except RuntimeError as e:
            raise ClingoGroundError(e)
        old_stdout = sys.stdout
        sys.stdout = model = StringIO()
        if raise_unsat:
            sat = ctl.solve(on_model=print, on_core=raise_unsat_error)
        else:
            sat = ctl.solve(on_model=print)
        sys.stdout = old_stdout  # reset stdout
        if not is_file:
            tmpdir.cleanup()  # remove tmp dir

        # messages
        msg = get_stats(ctl)  # get printable stats
        return model.getvalue(), sat, msg

    @staticmethod
    def run_from_file(file, **args):
        return ASPRunner._run_clingo(file, is_file=True, **args)

    def solve(self, optimise: bool = False, **args):
        self.set_tings(**args)
        self.make_asp_compatible(convert_term=(optimise is not None))
        asp = str(self)
        models, sat, msg = self._run_clingo(asp, is_file=False, clingo_stats=self.clingo_stats,
                                            raise_unsat=self.raise_unsat)
        return models, sat, msg

    def model_expand(self, clingo_stats: int = "DFLT", max_models: int = "DFLT",
                     timeout_seconds="DFLT", raise_unsat: bool = "DFLT"):
        if self.optimise:
            print('warning: Specification has optimisation term (this will be ignored).')
        return self.solve(optimise=False, clingo_stats=clingo_stats, max_models=max_models,
                          timeout_seconds=timeout_seconds, raise_unsat=raise_unsat)

    def model_optimise(self, term=None, clingo_stats: int = "DFLT", max_models: int = "DFLT",
                       timeout_seconds="DFLT", raise_unsat: bool = "DFLT"):
        if term:
            if self.optimise:
                print(f'warning: Specification already had a term to optimize "{self.optimise}" (this will be lost).')
            self.optimise = term
        if not self.optimise:
            raise ClingoLoadError("No term found for optimisation.")
        return self.solve(optimise=True, clingo_stats=clingo_stats, max_models=max_models,
                          timeout_seconds=timeout_seconds, raise_unsat=raise_unsat)


if __name__ == '__main__':
    try:
        # asp_file = sys.argv[1]  # TODO add ASP test file
        file, lang = sys.argv[1:3]
    except (IndexError, ValueError):
        print('warning: no file provided, using test file instead.')
        # asp_file = './ctd/asp_dummy.lp'
        file, lang = './ctd/idp3_dummy.idp3', 'IDP3'

    print(f"parsing specification from \'{lang}\' file \'{file}\'")
    my_spec = Specification.from_file(lang, file)

    my_runner = ASPRunner(my_spec)
    print(f'running ASP translation on specification (from \'{lang}\' file \'{file}\')')
    my_asp = my_runner.translate()
    print(my_asp)

    my_models = my_runner.model_expand(clingo_stats=1, max_models=3, timeout_seconds=1)
    print(f'running ASP solver on translation (with settings {my_runner.settings})')
    print(my_models)

    #my_run = ASPRunner.run_from_file(file)
    #print(f'\nstatic run: \n{my_run}')
    exit(0)
