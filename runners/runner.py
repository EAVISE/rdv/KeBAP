# Runner abstract class
# authors: Kylian Van Dessel

from abc import ABC, abstractmethod


class Runner(ABC):
    """ Abstract class to implement runners for different languages/systems

    Properties:
        specification
        **settings

    Methods:
        translate
        **reasoning-methods

    Callable:
        returns all declared blocks in specification in the following order: Vocab, Struc, Theor, Term
    """

    @property
    def vocabulary(self):
        return self.specification.vocabulary

    @property
    def structure(self):
        return self.specification.structure

    @property
    def theory(self):
        return self.specification.theory

    @property
    def optimise(self):
        return self.specification.object_value

    @optimise.setter
    def optimize(self, block):
        self.specification.object_value = block

    @property
    def settings(self):
        """ Return runner (solver) settings. """
        return {k: v for k,v in self.__dict__.items() if k not in ['specification', 'converted_functions']}

    @settings.setter
    def settings(self, d):
        for k in d:
            try:
                self.__getattribute__(k)
            except AttributeError as e:
                print(f'warning: Setting not updated, {e}')
            if d[k] != "DFLT":
                #print(f'Updates setting \'{k}\' = \'{d[k]}\'')
                self.__setattr__(k, d[k])

    def set_tings(self, **kwargs):
        # TODO only added to call self.set_tings(name=arg, ...) i.o. self.settings = {'name':arg, ...}
        #  was thinking about changing it to self.settings = (arg, ...) and retract name of arg variable..
        #  f'{foo=}'.split('=')[0]
        self.settings = kwargs

    @abstractmethod
    def __str__(self):
        """ Return specification in dedicated language. """
        pass

    @abstractmethod
    def translate(self):
        """ Translate specification to dedicated language. """
        pass

    def __call__(self):
        """ Return non-empty blocks of specification. """
        return filter(None, [self.vocabulary, self.structure, self.theory, self.specification.object_value])


if __name__ == '__main__':
    pass
