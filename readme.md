# KeBAP

Knowledge-Base API for Python (KeBAP) is a framework for using the FO(.) knowledge representation language in Python. 

## Description

WORK IN PROGRESS - This repository is still in the early stages of development and is still very incomplete (+ requires a lot of cleaning up). The code is made available to allow anyone interested in the project to play around with the code, check if this project might suit their needs or contribute their own ideas.

The goals of this project are: 
 - to create an intuitive API for IDP-Z3 in Python, reusing the same internal structure as the source .idp file to create an abstract syntax tree.
 - to allow the integration of different data sources in IDP.

The abstract syntax tree is largely taken over from the FOLASP project (https://gitlab.com/EAVISE/folasp/folasp-engine), but the parser has been updated to parse files in the IDP-Z3 syntax. The old IDP3 parser from the original FOLASP is still available, but is currently not actively supported.

The project currently includes a csv-importer and a [cDMN](https://cdmn.be)-importer. Work is being done to import knowledge graphs. On the output end, a translation to ASP is possible through the integration of FOLASP, although this is yet to be properly tested for IDP-Z3 files. Partial translation between IDP3 and IDP-Z3 are also possible, (although there is a known issue with regard to constants translated from IDP3 to IDP-Z3 which miss mandatory brackets in the Theory block). This translation is limited to the features supported by both versions of IDP.

---

## Getting started

### Installation

```bash
git clone https://gitlab.com/EAVISE/rdv/KeBAP
cd KeBAP
pip3 install -r requirements.txt
```

### Executing program

KeBAP contains the following structure:
TODO insert image here

A Specification object contains 3 blocks: Vocabulary, Structure, Theory. These are the same blocks one would typically find in an IDP Specification. These blocks are stored internally as an AST, which is largely language independent.

A Specification object can be instantiated from and IDP-Z3 file using the following method:
'''from specification import Specification
spec = Specification.from_file("IDPZ3", "path/to/file")'''

From this point, the Specification object or blocks contained in that object can be amended by adding or removing items or merging the blocks with different blocks of the same kind.

To use inference methods of a certain language, the Specification object must first be passed to a Runner. E.g. for IDP-Z3:
'''from runners.runner_IDPZ3 import IDPZ3Runner
runner = IDPZ3Runner(spec)'''

From the runner, the user has access to the different inference methods provided by the goal language (e.g. model_expand, optimize, propagate, ...).

---

## Development

The project consists of:
- some files for the internal representation and operation of the program: *specification.py, blocks.py, nodes.py, visitors.py*
- a directory for **parsers** (input language to Specification) `./parsers/`
- a directory for **translators** (Specification to output language) `./translators/`
- a director for **manipulators** (Manipulations on Specification) `./manipulations/` -- WIP still have to separate this from the visitors.py file
- a directory for system **runners** `./runners/`
- a script for extensive **testing** `test_bench.py` and some directories with test files and scripts `./test_dummies/`, `./test_files/`, and `./test_scripts/`
- and some project **information**; This `readme.md` file (which *nog een beetje te wensen over laat*) and `requirements.txt`

### ./parsers/  --  Adding a(n input) language parser

1) Add your parse file(s) to directory `./parsers/`. Make sure the (main) file is named *"parser_<LANG>.py"* and includes the 
methods ".parse(str)" and ".parse_file(filepath)" that call whatever your parser does and returns a (list of) Node 
object(s). The parse_file()-method will probably simply read the file and call the parse()-method, e.g.:

    *./parsers/parser_IDP3.py*
    ```python 
    def parse_file(file, args):
        # parse file (or read file in string and call parse() if suitable)
    def parse(stri, args):
        # parse string
    ```
    If one of these methods does not apply, simply raise a NotImplementedError, e.g. for a file importer of a data language an open file might be required (string method not applicable).
    For convention, name any additional files *"<lang>_..."*, e.g.: lex-yacc files for the IPD3 parser *idp3_lex.py* and *idp3_yacc.py*.
    
    Note: generic data processing and importing methods are provided in './parsers/data_preprocessing.py'

2) **(optional)** Test or run your parser; Although it is not required, it might be handy to provide a main block in your parser that simply takes a file(path) from the command line and parses the file (or takes a dummy file instead), e.g.: 
    
    *./parsers/parser_IDP3.py*
    ```python
    if __name__ == '__main__':
        try:
            filepath = sys.argv[1]
        except IndexError:
            filepath = 'test_dummies/ctd/idp3_dummy.idp3'
        print(*parse_file(idp_file))
    ```
    ``` 
    $ python parser_IDP3.py my_file.idp3
    ```
    You can provide the dummy file in `/test_dummies/` as well as a small(!) test file in `/test_scripts/`.
   Limit this to a single test file and the minimal number of required dummy files for quickly testing/sanity checking the parser.
   For convention, name these files *"<lang>_..."*, e.g.: *idp3_parser_test.py* and *idp3_dummy.idp3* .
    
    For extensive testing add test files to directory `./test_files/` instead (preferably in a new subdirectory).

3) Import your module in `./parsers/parser.py`, e.g.: 

    *./parsers/parser.py*
    ```python 
    import parsers.parser_IDP3
    ```
    From then on you can create a Specification (`specification.py`) from your input language using your parser.

4) **(optional)** Test or run our awesome framework with your contribution newly integrated! 
    ```python 
    from specification import Specification
    my_spec = Specification.from_file("IDP3","my_file.idp3")
    print(my_spec)
    ```
    You can also easily add a test to `./ctd/test_parser.py` by simply adding a configuration to the variable 'tests', e.g.:  
    
    *./ctd/test_parser.py* 
    ```python
    ...
    tests.append({'lang': 'IDP3', 'file': './ctd/idp3_dummy.idp3'})
    ...
    ```

### ./Translators/  --  Adding an output language translator 

** TODO: write some information, pretty much the same as the above and below... ***

### ./Runners/  --  Adding an output system runner 

** TODO: update this information: **
Add your runner file(s) to './runners/' the (main) file should be named '.runner_LANG' and should include a class 
'LANGRunner(Runner)' that inherits from the abstract class runners.runner.Runner()
Implement all abstract methods (or raise an Exception in case a method is not applicable).
    - "translate()" for parsing a specification to an output language (or a representation suitable for the output system)
    - "model_expand()" for the most default reasoning of extending a model S while satisfying a theory T (both over a same vocabulary V)
e.g. `./runners/runner_IDP3.py`:
```python
from runners.runner import Runner
class IDP3Runner(Runner):
    ...
    def translate(self):
        ...
    def model_expand(self, options):
        ...
```

Try running it with:

```python 
from specification import Specification
from runners.runner_IDP3 import IDP3Runner
my_spec = Specification.from_file("CDMN", "test_file.xlsx")  
my_runner = IDP3Runner(my_spec)  
print(str(my_runner))             # output specification in relevant language
my_models = my_runner.model_expand(...)  # run model expansion on specification
```


## Authors

Robin De Vogelaere, Kylian Van Dessel, Lucas Van Laer, Simon Vandevelde, Joost Vennekens

(based on FOLASP: Kylian Van Dessel, Jo Devriendt, Joost Vennekens)
