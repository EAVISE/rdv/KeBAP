# OI
# output and input logger for test_bench.py

from __future__ import annotations

# VERBOSITY  -----------------------------------------------------------------------------------------------------------

LOG = 0    # 0 = None; 1 = error; 2 = info (results); 3 = debug


# print to stdout
def _print(lvl: int, msg: str | list, tags: list = []):
    if LOG < lvl:
        return
    if not (isinstance(msg, list) or isinstance(msg, tuple)):
        msg = str(msg).split('\n')
    tag = ('\t' + ''.join([f'[{x}]' for x in tags])) if tags else ''
    [print(' '.join(filter(None, [tag, str(x)]))) for x in msg]


def nprint(msg): _print(0, str(msg), [])                         # 0 none/exceptional print
def eprint(msg, tags=[]): _print(1, msg, ['error', *tags])       # 1 error print
def wprint(msg, tags=[]): _print(2, msg, ['warning', *tags])     # 2 warning print
def iprint(msg, tags=[]): _print(2, msg, ['info', *tags])        # 2 info print
def dprint(msg, tags=[]): _print(3, msg, ['debug', *tags])       # 3 debug print


# write to file
def _write(lvl: int, file: str, msg: str | list, is_new: bool = False):
    if LOG < lvl:
        return
    if not msg:
        dprint(f"nothing to write to file {file}")
        return
    if not isinstance(msg, list):
        msg = [msg]
    mode = 'w' if is_new else 'a'
    with open(file, mode) as fw:
        fw.writelines(list(map(lambda x: str(x)+'\n', filter(None, msg))))


def nwrite(file, msg, new=False): _write(0, file, msg, new)
def ewrite(file, msg, new=False): _write(1, file, msg, new)
def iwrite(file, msg, new=False): _write(2, file, msg, new)
def dwrite(file, msg, new=False): _write(3, file, msg, new)


def _update(lvl, file, s, x=None, sep=';'):
    """Update a file by appending to each line horizontally (to the right),
       start on first line or search for x in header column and start on that row instead."""
    def _write_horizontal(l, s, i=0, fill=True):
        blnk = sep.join(['']*len(s[0]))
        s = [sep.join(map(str, y)) for y in s]
        for j in range(len(s)):
            l[i+j] = f'{l[i+j].rstrip()}{sep}{s[j]}\n'
        if fill:
            for j in range(j+1, len(l)):
                l[j] = f'{l[j].rstrip()}{sep}{blnk}\n'
        with open(file, 'w') as fw:
            fw.writelines(l)
        return
    if LOG < lvl:
        return
    with open(file, 'r') as fr:
        lr = fr.readlines()
    for i in range(len(lr)):
        if x:
            if lr[i].startswith(x):
                _write_horizontal(lr, s, i, False)
                return
        else:
            _write_horizontal(lr, s)
            return
    print(f'warning: Couldn\'t find what part of file needed to be updated ("{x}"), this might be a serious issue!')


def iupdate(file, s): _update(2, file, s)


def _copy(lvl, file_src, file_dst):
    if LOG < lvl:
        return
    import shutil
    shutil.copy(file_src, file_dst)


def dcopy(file_src, file_dst):  _copy(3, file_src, file_dst)


# LOGGING PRINTS  ------------------------------------------------------------------------------------------------------

def title(s, char="="):
    return f'\n{s}:\n{char * (len(s)+1)}'


def files(files_: dict):
    """ return listing (string) of files by language. """
    return '\n\n'.join(f'{l}:\t' + '\n\t\t'.join(files_[l]) for l in files_ if files_[l])


# error file
def err_header(sep=';'):
    return sep.join(['id', 'file', 'input language', 'language', 'test', 'error'])


def err_lines(*args, sep=';'):
    tag = ';'.join([str(x) for x in args[:-1]])
    msg = args[-1] if isinstance(args[-1], list) else str(args[-1]).replace('\n','').split('Error: ')
    msg = list(filter(None, msg))
    if not msg:
        msg = ['undefined error']
    return [sep.join([tag, x]) for x in msg]


# overview
def sum_header(file=None, start=None, descr=None, owner=None):
    return '\n'.join(filter(None, [(f'File:\t{file}' if file else None),
                                   (f'Date:\t{start}' if start else None),
                                   (f'Descr.:\t{descr}' if descr else None),
                                   (f'Owner:\t{owner}' if owner else None)
                                   ]))


def run_settings(uid, start, d_in=None, d_out=None, d_log=None, f_log=None, f_res=None, f_err=None, red=False):
    return '\n'.join([title('IO settings'),
                      f'Unique identifier: {uid}',
                      f'Date and time: {start}',
                      f'Input directory: {d_in if d_in else ""}',
                      f'Output directory: {d_out if d_out else ""}',
                      f'Logging directory: ' + f'{d_log}' if LOG else 'No',
                      f'Logging output: ' + (f'{f_log}, {f_res}, {f_err}' if LOG else 'No'),
                      f'Redirect output: ' + (f' {d_log}/out.txt and /err.txt' if red else ' No')
                      ])


def run_setup(t_in, t_out, t_mx, map_key, l_all, l_support, size_hcol=None, size_col=None):
    """ log table of experiments that will be performed """
    defarg = {'in': t_in, 'out': t_out, 'mx': t_mx}
    if not size_hcol:
        size_hcol = max(map(len, map_key.values())) + 2
    if not size_col:
        size_col = len(max(l_all)) + 2
    blank = " " * size_hcol
    line = f'+{"-" * size_hcol}+{"+".join(["-" * size_col for _ in l_all])}+\n'
    s = blank + ' ' + line[len(blank) + 1:]
    s += f' {blank}| {"| ".join([f"{y:{size_col - 1}}" for y in l_all])}|\n'
    s += line
    for x in l_support.keys():
        sx = ["x" if y in defarg[x] else ("" if y in l_support[x] else "-") for y in l_all]
        s += f'| {map_key[x]:{size_hcol - 1}}| ' + '| '.join([f'{y:^{size_col - 1}}' for y in sx]) + '|\n'
    s += line
    return s


def run_results(lang: str, errors: dict, chk, lg):
    r = []
    j = 0
    if chk:
        i = len(errors["check"][lang])
        #i = sum([len(errors["check"][l]) for l in errors["check"]])
        j+=i
        s = f'{lang} check:'
        r.append(f'{s:<20}' + ("OK" if i == 0 else f'NOK ({i} errors)'))

    i = len(errors["parsing"][lang])
    #i = sum([len(errors["parsing"][l]) for l in errors["parsing"]])
    j += i
    s = f'{lang} parsing:'
    r.append(f'{s:<20}' + ("OK" if i == 0 else f'NOK ({i} errors)'))

    if errors["translating"]:
        for next in errors["translating"]:
            i = len(errors["translating"][next])
            j += i
            s = f'{next} translating:'
            r.append(f'{s:<20}' + ("OK" if i == 0 else f'NOK ({i} error{"s" if i>1 else ""})'))

    if errors["model expand"]:
        for next in errors["model expand"]:
            i = len(errors["model expand"][next])
            j += i
            s = f'{next} reasoning:'
            r.append(f'{s:<20}' + ("OK" if i == 0 else f'NOK ({i} errors)'))

    return (f'{lang} input test: ' + ("SUCCES" if j == 0 else f"FAILED ({j} errors)")
            + ('\n\t' + '\n\t'.join(r) + '\n' if LOG >= 2 else ''))


def run_files(files_, name):
    if not files:
        return title(name, '-')
    return '\n'.join([title(name, '-'), files(files_)])


# info / experiments result file
def res_header(chk, t_out, t_mx, sep=';'):
    t = [f"{x} status{sep} {x} time (s)" for x in filter(None,
                                                         ['check' if chk else None, 'parse', *[f'translate {y}' for y in t_out], *[f'solve {y}' for y in t_mx]])]
    return sep.join(['id', 'file', 'lang', *t])


def res_line(i, res, chk=True, sep=';', rnd=10):
    s = (str(i), res['file'], res['lang'],
         *(([round(x,rnd) if isinstance(x,float) and rnd else x for x in res['check']] if res['check'] else ['', '']) if chk else []),
         *([round(x,rnd) if isinstance(x,float) and rnd else x for x in res['parsing']] if res['parsing'] else ['', '']),
         *[(round(x, rnd) if isinstance(x, float) and rnd else x) for y in res['translating'] for x in y],
         *[(round(x, rnd) if isinstance(x, float) and rnd else x) for y in res['model expand'] for x in y]
         )
    # TODO do something smarter, last two test results will be shifted (wrt. header) if not all translation / reasoning
    #  test are performed on all input languages (or even if not in the same order)...
    return sep.join(map(str, s))


# log file
def log_header(chk, ign, t_in, t_out, t_mx):
    return '\n'.join(filter(None, ['INPUT LANGUAGE >',
                                   'INPUT FILES' if chk or ign else None,
                                   *[f'{y} files' for y in t_in],
                                   #*['\n'.join(filter(None, [f'ignored {y} files' if ign else None,
                                   #                          f'corrupted {y} files' if chk else None])) for y in t_in],
                                    'PARSING', *[f'{y} parsing ' for y in t_in],
                                    'TRANSLATING' if t_out else None, *[f'{y} translate ' for y in t_out],
                                    'REASONING' if t_mx else None, *[f'{y} solving ' for y in t_mx]
                                   ]))


def log_numbers(input_lang, nb_inp, ign, nb_ign, chk, e_chk, t_in, e_in, t_out, e_out, t_mx, e_mx):
    def pro(v, compl=False):
        if compl:
            v = 1-v
        return int(v*100)

    # HEADER (= 4 columns per language or total)
    if input_lang:
        s = [[input_lang, ' ', ' ', ' '], list(filter(None,
             # ['#files', '#ignored' if ign else '', '#corrupt' if chk else '', ' ' if not (chk and ign) else '',' ']))]
             ['#all', '#ign' if ign else '', '#bad' if chk else '', ' ' if not (chk and ign) else '', ' ']))]
        l = [input_lang]
        if isinstance(nb_inp, int):
            nb_inp = {input_lang: nb_inp}
    else:
        s = [["TOTAL", ' ', ' ', ' '],
             # ['#files', '#discarded', '#used', '%used']]
             ['#all', '#del', '#use', '%use']]
        l = t_in
    # h = ['#files', '#errors', '#succes', '%succes']
    h = ['#all', '#err', '#ok', '%ok']

    # INPUT LOGGING
    if chk or ign:
        # number of files
        if input_lang and isinstance(nb_ign, int):
            nb_ign = {input_lang: nb_ign}                                       # nb of ignored files
        nb_bad = {x: (len(e_chk[x]) if e_chk[x] else 0) for x in l}             # nb of corrupt files
        nb = {x: nb_inp[x] + nb_bad[x] + nb_ign[x] for x in l}                  # nb of files (all)

        # check any files at all
        if sum(nb.values()) == 0:
            print('warning: No test files found.')
            return ''
        for x, n in nb.items():
            if n == 0:
                print(f'warning: No {x} files found.')

        # add input numbers to csv
        if input_lang:
            s.extend([([nb[x], nb_ign[x], nb_bad[x], ''] if x in l else ['-']*3+[' ']) for x in t_in])
        else:
            s.extend([[nb[x], nb_ign[x]+nb_bad[x], nb_inp[x], pro(nb_inp[x]/nb[x])] for x in t_in])

        # number of test files, i.e. all - ignored
        nb = {x: nb[x]-nb_ign[x] for x in nb}

        # check any test files
        if sum(nb.values()) == 0:  # no input files
            print('warning: No input files found (all files were ignored).')
            return s
        for x, i in nb.items():
            if i == 0:
                print(f'warning: No {x} input files found (all {x} files were ignored).')

    # number of actual input files for parser(s), i.e. all - ignored - corrupt
    nb = nb_inp

    # check any input files
    if sum(nb.values()) == 0:  # no valid input files
        print('warning: No valid input files found (all files were ' 
              f'{"either ignored or " if sum(nb_ign.values())>0 else ""}corrupt).')
        return s
    for x, n in nb.items():
        if n == 0:
                print(f'warning: No valid {x} input files found (all {x} files were '
                        f'{"either ignored or " if sum(nb_ign.values())>0 else ""}corrupt).')

    # PARSING LOGGING
    if t_in:
        # number of parsing errors
        nb_ep = {x: len(e_in[x]) for x in l}

        # add parsing numbers to csv
        s.append(h)
        s.extend((([nb[x], nb_ep[x], nb[x]-nb_ep[x], pro(nb_ep[x]/nb[x], compl=True)] if nb[x] != 0 else ["NT"]*4)
                  if x in l else ['-']*4) for x in t_in)

        # number of correctly parsed files, i.e. input - errors
        nb = {x: nb[x] - nb_ep[x] for x in l}

        # check any parsed files
        if sum(nb.values()) == 0:  # no files correctly parsed
            print('warning: No input files parsed correctly.')
            return s
        for i, x in nb.items():
            if i == 0:
                print(f'warning: No {x} input files parsed correctly.')

    # total number of files for further execution (sum input languages)
    nb = {x: sum(nb.values()) for x in t_out+t_mx}

    # TRANSLATING LOGGING
    if t_out:
        # number of translating errors
        nb_et = {x: len(e_out[x]) for x in t_out}

        # add translating numbers to csv
        s.append(h)
        s.extend(([nb[x], nb_et[x], nb[x]-nb_et[x], pro(nb_et[x]/nb[x], compl=True)] if nb[x] != 0 else ["NT"]*4)
                 for x in t_out)

        # number of successfully translated files
        nb = {x: nb[x] - nb_et[x] for x in t_out}

        # check any translated files
        if sum(nb.values()) == 0:  # no files correctly translated
            print('warning: No input files translated correctly.')
            return s
        if input_lang:
            for x, n in nb.items():
                if n == 0:
                    print(f'warning: No {input_lang} input files correctly translated to {x}.')

    # REASONING LOGGING
    if t_mx:
        # number of reasoning errors
        nb_es = {x: len(e_mx[x]) for x in t_mx}

        # add reasoning numbers to csv
        s.append(h)
        s.extend(([nb[x], nb_es[x], nb[x]-nb_es[x], pro(nb_es[x] / nb[x], compl=True)] if nb[x] != 0 else ['NT']*4)
                 for x in t_mx)
    return s
