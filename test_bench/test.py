# Authors: Kylian Van Dessel, Simon Vandevelde
# Description:
#   automated test bench, returns csv w. all test results + csv w. all errors
#   (IDP-Z3 test bench copied from IDP-Z3: https://gitlab.com/krr/IDP-Z3/-/tree/main/tests?ref_type=heads)
# Usage:
#   There are a lot of default settings, minimally provide
#   - what tests you want to run, i.e. parsing (-p), translating (-t), and/or reasoning (-x),
#   - and probably the logging level, i.e. verbosity (-v), may generate ridiculously many files otherwise)
#   e.g. $ python test_bench.py -p IDP3 CSV  -t IDPZ3  -v 1
# Out:
#   Depending on the verbosity setting;
#   In the output directory ./tmp (=default) you will find some logging files for errors, results, and overviews,
#   as well as, input, output, and result test files. logging a log file with any generated messages (log.txt),
# Logging:
#   based on log level or verbosity (-v) the following things are printed/written:
#   0 NONE (write: run overview file (run.txt), print: only system errors and particular run info)
#   1 ERROR (write: error file (err.csv), print: errors messages)
#   2 INFO (write: results file (res.csv), results overview file (log.csv), and output files (*.out, *.<lang>), print: )
#   3 DEBUG (write: input files (*.in), print: all messages)


import os
import sys
import argparse
import glob
import re
from datetime import datetime
import time as chrono
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import oi as logger
from oi import nwrite, ewrite, iwrite, dwrite, nprint, eprint, wprint, dprint, iupdate, dcopy
from modeller import Modeller

# TODO WIP
#   |
#  \/
import runners
import runners.runner_IDPZ3
import runners.runner_IDP3
import runners.runner_ASP


# CONFIGURATION ========================================================================================================
# (!) Following is a list of configuration parameters. Don't worry too much about it , unless you have a strong
# opinion on something. The (more) relevant parameters can be specified via command line arguments

# Files and directories -------------------------------------------------------
dflt_input_dir = './test_bench'         # default input directory (if none provided)
#dflt_input_dir = './test_files'
from_subdir = True                      # use only input files from language subdirs, i.e. ./<INPUT_DIR>/<LANG>*/**/*
dflt_output_dir = './tmp'    # default output directory (if none provided)
to_uid_subdir = False                   # use unique output subdir (timestamp based), i.e., ./<OUTPUT_DIR>/<UID>/*

file_ext = {'IDPZ3': 'idp', 'IDP3': 'idp', 'ASP': 'lp',                 # respective language extensions
              'CSV': 'csv', 'CDMN': 'xlsx'}
ignore_files = [                                                        # uncomment the file tags of files to ignore
    '.timeout',  # (excessive time consumption)
    '.error',    # bad input files
    #'.unsupported',  # unsupported feature files
    # '_unsat',  # unsat specification files
    '.ignore',   # ignore files
    '_ignore',   # ignore dirs
]

# Languages and engines -------------------------------------------------------
input_lang = ['IDP3', 'IDPZ3', 'CSV']
output_lang = reason_lang = ['IDP3', 'IDPZ3', 'ASP']
suppor_languages = {'in': input_lang, 'out': output_lang, 'mx': reason_lang}  # supported languages
all_languages = sorted(set().union(*suppor_languages.values()))               # all languages (used by any functionality)
assert all(x in file_ext for x in all_languages), \
        f"no file extension found for: {', '.join([x for x in all_languages if x not in file_ext])}"
                                                                              # check every language has a file extension

settings = {                                                            # engine settings
    'max_models': 1,  # max nb of models to search for
    'timeout_seconds': 1,  # max nb of seconds to search
    'raise_unsat': False,  # raise an error if there is no solution found (within time limits)
    # TODO something smarter than raising an error, like check if both have same sat value,
    #  e.g. if both are unsat, the system works just fine...
    #'clingo_stats': 1,
    # TODO put this somewhere, base on verbosity level, ignore in other engines, ... ?
}

# Logging ---------------------------------------------------------------------
# ! warning: These can only be altered here:
map_key = {'in': 'Parse', 'out': 'Translate', 'mx': 'Model expand'}     # mapping keys to printable name
out_ext = {x: (x.lower()+'.' if list(file_ext.values()).count(file_ext[x]) > 1 else '') + file_ext[x] for x in file_ext}
do_mention_ignored_files = True         # include a mention of the ignored files in the logs

# ! warning: These can only be altered through command line args (False by default)
do_perform_sanity_check = None          # run check to see if files are consistent (i.e. parsable, not necessarily sat)
do_save_test_files = None               # store input, translated, and/or output files (still depending on verbosity)
do_redirect_std = None                  # default don't redirect std writers to files


# EXPERIMENTS ==========================================================================================================
# Here we run the experiments in accordance with the general configuration
# and user command line settings (see main or command line Help for this)

def get_input_files(dir, lang):
    """ return all suitable input files for a given input directory and language """
    files = list()
    try:
        if from_subdir:
            dir = f'{dir}/{lang.lower()}'

        # TODO fix glob
        ign = set().union(
            *map(lambda x: set(glob.glob(f'{dir}**/**/*{x}.{file_ext[lang]}', recursive=True)), ignore_files),
            *map(lambda x: set(glob.glob(f'{dir}**/**{x}/*.{file_ext[lang]}', recursive=True)), ignore_files),
            *map(lambda x: set(glob.glob(f'{dir}**{x}/**/*.{file_ext[lang]}', recursive=True)), ignore_files))
        files = set(glob.glob(f'{dir}**/**/*.{file_ext[lang]}', recursive=True)) - ign
    except KeyError:
        print(f'file extension unknown for language: {lang}')
    if files == []:
        wprint(f'No {"directory with" if from_subdir else ""} test files for {lang}', [lang])

    # TODO if get_config: return a dict {filename: config_filename} as well
    return sorted(files), sorted(ign)


def run_sanity_check(files, lang):
    """ run input files (for parser) by respective engine without any manipulations to see if the file is consistent
        (if engine provided for input language).
    """
    res = dict()  # store result msg's
    err = dict()  # store error msg's

    # TODO fix this! something wrong w. IDP3 engine
    #  error while loading shared libraries: libidp.so: cannot open shared object file: No such file or directory
    #if lang not in suppor_languages['mx']:
    if lang not in suppor_languages['mx'] or lang == "IDP3":
        wprint(f"could not do sanity check since "
              f"there is no reasoning engine supported for the {lang} input language.", [lang])
        res = {x: ('NA', '-') for x in files}
        return res, err

    Runner = getattr(getattr(runners, f'runner_{lang}'), f'{lang}Runner')
    for file in files:
        print(file)
        t0 = chrono.time()
        try:  # run
            if lang == "ASP":  # TODO fix this!
                ss, sat, msg = Runner.run_from_file(file, **settings, clingo_stats=1)
            else:
                ss, sat, msg = Runner.run_from_file(file, **settings)
        except Exception as e:  # error logging
            tt = chrono.time() - t0
            sat = 'ERR'
            err[file] = e
            eprint(e, tags=['check', lang])
        else:  # result logging
            tt = chrono.time() - t0
            dprint(msg, tags=['check', lang])
        res[file] = (sat, tt)
    return res, err


def run_parse(file, lang):
    """ Run parse test. """
    # TODO might be better located within 'get_input_files()' method? see previous command
    def get_config_file(file, ext='json'):
        """ return config file with the same basename as input file and a given extension (default: .json) if found(!)
            used for data file importers, such as CSV, which might require some configuration.
        """
        file_path = f'{os.path.splitext(file)[0]}.{ext}'
        if not os.path.isfile(file_path):
            return None
        return file_path

    mdlr = None
    ts = chrono.time()
    try:
        x = {'config_file': get_config_file(file)} if lang == 'CSV' else {}
        mdlr = Modeller.from_file(lang, file, **x, has_freevars=True)
    except Exception as e:
        tt = chrono.time() - ts
        sat = 'ERR'
        err = e
    else:
        tt = chrono.time() - ts
        sat = 'OK'
        err = None
    return sat, tt, err, mdlr


def run_translate(mdlr, lang):
    """ Run translation test. """
    outp = None
    ts = chrono.time()
    try:
        outp = mdlr.to_string(lang)
    except Exception as e:
        tt = chrono.time() - ts
        sat = 'ERR'
        err = e
    else:
        tt = chrono.time() - ts
        sat = 'OK'
        err = None
    return sat, tt, err, outp


def run_reason(spec, lang):
    """ Run reasoning test. """
    outp = msg = None
    ts = chrono.time()
    try:
        runner = getattr(getattr(runners, f'runner_{lang}'), f'{lang}Runner')(spec)
        if lang == "ASP":  # TODO QUICKFIX fix this!
            outp, _, msg = runner.model_expand(**settings, clingo_stats=1)
        else:
            outp, _, msg = runner.model_expand(**settings)
    except Exception as e:
        tt = chrono.time() - ts
        sat = 'ERR'
        err = e
    else:
        tt = chrono.time() - ts
        sat = 'OK'
        err = None
    return sat, tt, err, outp, msg


def run(log_file, err_file, res_file, input_dir, output_dir, input_lang, tests_parse, tests_transl, tests_reason):
    res = list()                                                                # results

    error_files = {
        'check': {input_lang: []},                                              # sanity check errors
        'parsing': {input_lang: []},                                            # parsing errors
        'translating': {x: [] for x in suppor_languages['out']} if tests_transl else None,  # translate errors
        'model expand': {x: [] for x in suppor_languages['mx']} if tests_reason else None   # reason errors
        }

    # INPUT FILES
    input_files, ignored_files = get_input_files(input_dir, input_lang)         # get input files

    # SANITY CHECK
    test = "check"
    if do_perform_sanity_check:
        result_check, error_check = run_sanity_check(input_files, input_lang)   # run sanity check
        for file in error_check:
            # error_files[test][input_lang][file] = (input_lang, input_lang, test, error_check[file])
            ewrite(err_file, logger.err_lines('', file, *(input_lang, input_lang, test, error_check[file])))
        input_files = input_files - error_check.keys()                          # ignore bad files.
        error_files[test][input_lang] = list(error_check.keys())

    # RUN
    nb = len(input_files)                                                       # log nb of actual input files
    for i, file in enumerate(input_files):
        base = file.split('/')[-1].split('.')[0]
        # base = file.split('/')[-1]
        id = f'{input_lang}_{i + 1}'
        nprint(f'{id} (/{nb}): {file}')

        # new experiment (store individual dict with all experiment results of one file)
        res.append({'file': file, 'lang': input_lang, **{x: [] for x in error_files.keys()}})
        if do_perform_sanity_check and result_check[file]:
            test = "check"
            res[-1][test] = result_check[file]

        # TEST PARSE
        test = "parsing"
        dcopy(file, f'{output_dir}/{input_lang.lower()}/{base}.{file_ext[input_lang]}.in')
        sat, tt, err, mdlr = run_parse(file, input_lang)                        # run parsing test
        res[-1][test] = (sat, tt)

        # error
        if err:
            error_files[test][input_lang].append(file)  # [file] = (input_lang, input_lang, test, err)
            ewrite(err_file, logger.err_lines(id, file, *(input_lang, input_lang, test, err)))
            eprint(err, tags=[test, input_lang])
            dwrite(res_file, logger.res_line(id, res[-1], do_perform_sanity_check))    # write results
            # continue  # unparsed file, continue with next

        prev = []  # used for storing executed tests (parsing and sanity check excluded)

        # TEST TRANSLATE
        test = "translating"
        for lang in tests_transl:
            if file in error_files["parsing"][input_lang]:                      # unparsed
                res[-1][test].append(("NT", "-"))
                continue
            sat, tt, err, outp = run_translate(mdlr, lang)                      # run translation test
            res[-1][test].append((sat, tt))
            if err:
                error_files[test][lang].append(file)  # [file] = (input_lang, f'{lang} {test}', err)
                ewrite(err_file, logger.err_lines(id, file, *(input_lang, lang, test, err)))
                eprint(err, tags=[test, lang])
            elif do_save_test_files:                                            # write translation to output file
                iwrite(f'{output_dir}/{input_lang.lower()}/{base}.{out_ext[lang]}', outp, new=True)

        prev.append(test)

        # TEST REASON
        # TODO revision of complete './runners/' directory is, uhm, wenselijk.
        test = "model expand"
        for lang in tests_reason:
            if file in error_files["parsing"][input_lang]:           # unparsed
                res[-1][test].append(("NT", "-"))
                continue
            if any(lang in error_files[p] and file in error_files[p][lang] for p in prev):  # failed any previous test
                res[-1][test].append(("NT", "-"))
                continue
            sat, tt, err, outp, msg = run_reason(mdlr.model, lang)              # run reasoning test
            res[-1][test].append((sat, tt))
            if err:
                error_files[test][lang].append(file)  # [file] = (input_lang, f'{lang} {test}', err)
                ewrite(err_file, logger.err_lines(id, file, *(input_lang, lang, test, err)))
                eprint(err, tags=[test, lang])
                continue
            if not outp:
                # TODO do something with timeouts?
                wprint(f'running {lang} aborted due to timeout for file {file}', tags=[test, lang, 'timeout'])
                continue
            dprint(msg, tags=[test, lang])
            if do_save_test_files:
                # TODO convert output back to original language(now output is written in reasoning language)? or both?
                if len(outp) == 1:                                      # write solution(s) to output file(s)
                    iwrite(f'{output_dir}/{input_lang.lower()}/{base}.{out_ext[lang]}.out', *outp, new=True)
                else:
                    for j, outpi in enumerate(outp):
                        iwrite(f'{output_dir}/{input_lang.lower()}/{base}.{out_ext[lang]}.out.{j}',
                               outpi, new=True)
                        # TODO QUICKFIX avoid ASP result from writing millions of files (since max models not passed to asp) ... :\
                        if j == settings['max_models']:
                            break
        prev.append(test)
        iwrite(res_file, logger.res_line(id, res[-1], do_perform_sanity_check))        # write results

    iupdate(log_file, logger.log_numbers(input_lang, len(input_files), do_mention_ignored_files, len(ignored_files),
                                         do_perform_sanity_check, error_files['check'], tests_parse, error_files['parsing'],
                                         tests_transl, error_files['translating'], tests_reason, error_files['model expand']))
    return input_files, ignored_files, error_files


# MAIN =================================================================================================================

def main():
    # Timestamp
    time = datetime.now()
    experiment_start = time.strftime("%Y-%m-%d %H:%M:%S")
    experiment_key = re.sub('[ :\-]', '', experiment_start)
    nprint(f'Started experiment at {experiment_start}')

    # Command line arguments
    argp = argparse.ArgumentParser(description='Knowledge-base API')
    argp.add_argument('-p', '--parse', type=str, nargs='+', help='input language for parsing', required=True)
    argp.add_argument('-t', '--translate', type=str, nargs='+', default=[], help='languages to translate specification')
    argp.add_argument('-x', '--expand', type=str, nargs='+', default=[], help='systems to perform model expansion')
    #argp.add_argument('-y', '--satisfy', type=str, nargs='+', default=[], help='systems to perform sat check')
    #argp.add_argument('-z', '--optimize', type=str, nargs='+', default=[], help='systems to perform optimization') # TODO find out how to provide the term to optimise in IDPZ3...
    argp.add_argument('-i', '--input', type=str, help='custom directory for input files')
    argp.add_argument('-o', '--output', type=str, help='custom directory for output files')
    argp.add_argument('-c', '--sanity-check', action='store_true',  help='perform sanity check to see if files are runnable in the first place')
    argp.add_argument('-r', '--redirect-output', action='store_true',  help='redirect stdout to additional logfile')
    argp.add_argument('-s', '--save-test-files', action='store_true',  help='store input (-v 3), translated and output (-v 2) files in output dir')
    #argp.add_argument('-l', '--log', action='store_true', help='custom directory for standard output and logging files')
    argp.add_argument('-v', '--verbose', type=int,  help='provide the level of verbosity [0-3]')
    args = argp.parse_args()

    # Languages and engines [-p[l] -t[l] -x[l]]
    tests_parse = list(map(lambda x: x.upper(), args.parse))             # input languages / parser tests
    assert all(x in suppor_languages['in'] for x in tests_parse), 'unsupported language for parsing'
    tests_transl = list(map(lambda x: x.upper(), args.translate))        # output languages / translation tests
    assert all(x in suppor_languages['out'] for x in tests_transl), 'unsupported language for translation'
    tests_reason = list(map(lambda x: x.upper(), args.expand))           # engines / runner tests
    assert all(x in suppor_languages['mx'] for x in tests_reason), 'unsupported language for model expansion'

    # Files and directories [-i[s] -o[s]]
    input_dir = args.input if args.input else dflt_input_dir            # input directory -i (or dflt)
    output_dir = args.output if args.output else dflt_output_dir        # output directory -o (or dflt)
    if to_uid_subdir:                                                   # unique subdir for this experiment run
        output_dir += f'/{experiment_key}'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    for x in tests_parse:                                                # add subdir for each input language
        if not os.path.exists(f'{output_dir}/{x.lower()}'):
            os.makedirs(f'{output_dir}/{x.lower()}')

    # Logging [-v[i] -c -s -r]
    log_dir = output_dir                                                # logging directory
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    logger.LOG = args.verbose if args.verbose else logger.LOG           # log level

    run_file = f'{log_dir}/run.txt'
    err_file = res_file = log_file = None                    # log files: run, error, results, results log
    if logger.LOG:
        err_file = f'{log_dir}/err.csv'
        log_file = f'{log_dir}/log.csv'
        res_file = f'{log_dir}/res.csv'

    global do_perform_sanity_check
    do_perform_sanity_check = args.sanity_check
    global do_save_test_files
    do_save_test_files = args.save_test_files
    global do_redirect_std
    do_redirect_std = args.redirect_output                              # redirect stdout, stderr Y/N
    if do_redirect_std:
        old_stdout = sys.stdout
        old_stderr = sys.stderr
        sys.stdout = open(f'{log_dir}/out.txt', 'w')
        sys.stderr = open(f'{log_dir}/err.txt', 'w')  # or sys.stderr = sys.stdout

    # write file headers and settings in logging files
    nwrite(run_file, [
           # logger.run_header(run_file, experiment_start, 'Experiment run overview file '),
           logger.run_settings(experiment_key, experiment_start, input_dir, output_dir, log_dir,
                               log_file, res_file, err_file, do_redirect_std),
           logger.title("Experiments"),
           logger.run_setup(tests_parse, tests_transl, tests_reason, map_key, all_languages, suppor_languages),
           logger.title("Results")
           ], new=True)
    ewrite(err_file, logger.err_header(), new=True)
    iwrite(log_file, logger.log_header(do_perform_sanity_check, do_mention_ignored_files,
                                       tests_parse, tests_transl, tests_reason), new=True)
    iwrite(res_file, logger.res_header(do_perform_sanity_check, tests_transl, tests_reason), new=True)

    input_files = dict()
    ignored_files = dict()
    errors = dict() #dict.fromkeys(['check', 'parsing', 'translating', 'model expand'], dict())
    for lang in tests_parse:
        # experiment call
        f_inp, f_ign, f_err = run(log_file, err_file, res_file, input_dir, output_dir,
                                  lang, tests_parse, tests_transl, tests_reason)
        input_files[lang] = f_inp
        ignored_files[lang] = f_ign
        for kind in f_err:
            if kind not in errors:
                errors[kind] = dict()
            if f_err[kind]:
                for l in f_err[kind]:
                    if l not in errors[kind]:
                        errors[kind][l] = f_err[kind][l]
                    else:
                        errors[kind][l].extend(f_err[kind][l])
                        # if f_err[kind][lang]:
                        #     for file in f_err[kind][lang]:
                        #         errors[kind][lang][file] = f_err[kind][lang][file]
        # write result summary to run file
        nwrite(run_file, logger.run_results(lang, f_err, do_perform_sanity_check, suppor_languages['mx']))

    # write result files to run file
    nwrite(run_file, [logger.title("FILES"),
                      logger.run_files(ignored_files, "IGNORED FILES"),
                      logger.run_files(errors['check'], "BAD INPUT FILES"),
                      logger.run_files(errors['parsing'], "ERRORS PARSING FILES"),
                      logger.run_files(errors['translating'], "ERRORS TRANSLATING FILES"),
                      logger.run_files(errors['model expand'], "ERRORS REASONING ON FILES")
                      ])

    # write numbers to log file
    iupdate(log_file, logger.log_numbers(None, {x: len(input_files[x]) for x in input_files},
                                         do_mention_ignored_files, {x: len(ignored_files[x]) for x in ignored_files},
                                         do_perform_sanity_check, errors['check'],
                                         tests_parse, errors['parsing'],
                                         tests_transl, errors['translating'],
                                         tests_reason, errors['model expand']))

    # close redirected files (if necessary)
    if args.redirect_output:
        sys.stdout.close()
        sys.stderr.close()
        sys.stdout = old_stdout
        sys.stderr = old_stderr
    nprint('Done.')

    # Timestamp
    time_end = datetime.now()
    experiment_end = time.strftime("%Y-%m-%d %H:%M:%S")
    nprint(f'Ended experiment at {experiment_end}')
    nprint(f'Elapsed time: {time_end-time}')

    exit(0)


if __name__ == '__main__':
    main()
