import os
import sys
import argparse
import warnings
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import parsers
from specification import Specification
from _dummyfiles import DUMMYFILE, DUMMYLANG_P


ln = 80


def _get(lang):
    try:
        return getattr(getattr(parsers, f'parser_{lang}'), f'{lang}Parser')
    except AttributeError:
        raise NotImplementedError(f'Could not retrieve an appropriate parser for input language \'{lang}\'')


def _run(langs, files_and_config):
    success = True
    for l in langs:
        print(f"(running {l} parser)")
        for fc in files_and_config[l]:
            f = {'file':fc} if not isinstance(fc, dict) else fc
            prnt = f"  {f['file'].split('/')[-1]}"
            try:
                blocks = _get(l)().parse_file(**f)
                spec = Specification(blocks)
                if len(langs) == len(files_and_config[l]) == 1:
                    return spec
                print(prnt.ljust(ln - 3, '.') + ".OK")
            except Exception as e:
                if len(langs) == len(files_and_config[l]) == 1:
                    raise e
                success = False
                print(prnt.ljust(ln - 7, '.') + ".FAILED")
                print(e)
    return "(done)\nParser test " + ("successful" if success else "failed") + "!"


def run_all(lang):
    print('TODO, not implemented yet')
    exit(-1)

def run_dummy(lang):
    if not lang:
        return _run(DUMMYLANG_P, DUMMYFILE)
    if not isinstance(lang, list):
        lang = [lang]
    lang = list(map(str.upper, lang))
    return _run(lang, DUMMYFILE)


def run_test(file, lang):
    if not lang:
        warnings.warn('No input language provided, trying to derive language from file extension of provided file.')
        lang = file.split('.')[-1]
    lang = lang.upper()
    print(f'Running {lang} parser on file \'{file}\'')
    return _run([lang], {lang:[file]})


if __name__ == "__main__":
    argp = argparse.ArgumentParser(description='Run Parser')
    argp.add_argument('-l', '--lang', type=str, help='test language (of file) for parsing')
    argp.add_argument('-a', '--all', action='store_true', help='run complete test')
    argp.add_argument('-d', '--dummy', action='store_true', help='run dummy files (for lang)')
    argp.add_argument('-f', '--file', type=str, help='run single file')
    args = argp.parse_args()
    if args.all:
        print('Running parser on complete test set. If no language (-l) is provided, all available languages are tested.')
        print(run_all(args.lang))
    elif args.dummy or not args.file:
        print('Running parser on dummy file(s). If no language (-l) is provided, a dummy selection of languages is tested.')
        print(run_dummy(args.lang))
    else:
        print(f'Running parser on file \'{args.file}\'.')
        print(run_test(args.file, args.lang))
    exit(0)
