# directory
DIR = "./test_scripts/_dummyfiles/"

# parser languages for dummy tests
DUMMYLANG_P = ["IDPZ3",
               "IDP3",
               "CSV",
               # "CDMN",
]

# translator languages for dummy tests
DUMMYLANG_T = ["IDPZ3",
               "IDP3",
               # "ASP",
]

# files for dummy tests
DUMMYFILE = {
    'IDPZ3': [{'file': f'{DIR}idpz3_dummy_lim.idpz3'}, {'file': f'{DIR}idpz3_dummy.idpz3'}, {'file': f'{DIR}idpz3_dummy_ext.idpz3'}],
    'IDP3': [{'file': f'{DIR}idp3_dummy_lim.idp3'}, {'file': f'{DIR}idp3_dummy.idp3'}, {'file': f'{DIR}idp3_dummy_ext.idp3'}],
    'CSV': [{'file': f'{DIR}csv_dummy.csv', 'config_file': f'{DIR}csv_dummy.json'}],
    #'CDMN': [{'file': f'{test_dir}cdmn_dummy.xlsx', 'sheets': ["set_better",  "glossary"]}],
}