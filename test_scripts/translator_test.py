import os
import sys
import argparse
import warnings
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import parsers
import translators
from specification import Specification
from _dummyfiles import DUMMYFILE, DUMMYLANG_P, DUMMYLANG_T


ln = 80


def _getp(lang):
    try:
        return getattr(getattr(parsers, f'parser_{lang}'), f'{lang}Parser')
    except AttributeError:
        raise NotImplementedError(f'Could not retrieve an appropriate parser for input language \'{lang}\'')


def _gett(lang):
    try:
        return getattr(getattr(translators, f'translator_{lang}'), f'{lang}Translator')
    except AttributeError:
        raise NotImplementedError(f'Could not retrieve an appropriate translator for test language \'{lang}\'')


def _run(langsp, langst, files_and_config):
    successp, successt = True, True
    if not isinstance(langsp, list):
        langsp = [langsp]
    langsp = list(map(str.upper, langsp))
    if not isinstance(langst, list):
        langst = [langst]
    langst = list(map(str.upper, langst))

    print(f"(parsing input)")
    for l in langsp:
        if not isinstance(files_and_config[l][0], dict):
            files_and_config[l] = [{'file': x} for x in files_and_config[l]]
        for fc in files_and_config[l]:
            prnt = f"  [{l}] {fc['file'].split('/')[-1]}"
            try:
                blocks = _getp(l)().parse_file(**fc)
                spec = Specification(blocks)
                fc['spec'] = spec
                if len(langsp) == len(files_and_config) == len(langst) == 1:
                    continue
                print(prnt.ljust(ln - 3, '.') + ".OK")
            except Exception as e:
                if len(langsp) == len(files_and_config) == len(langst) == 1:
                    raise e
                successp = False
                print(prnt.ljust(ln - 7, '.') + ".FAILED")
                print(e)

    for lt in langst:
        print(f"(running {lt} translator)")
        for lp in langsp:
            for fc in files_and_config[lp]:
                prnt = f"  [{lp}] {fc['file'].split('/')[-1]}"
                if 'spec' not in fc:
                    print(prnt.ljust(ln - 3, '.') + ".NA")
                    continue
                try:
                    repr = _gett(lt)().translate(fc['spec'])
                    if len(langsp) == len(files_and_config) == len(langst) == 1:
                            return fc['spec'], repr
                    #fc['repr'] = repr
                    print(prnt.ljust(ln - 3, '.') + ".OK")
                except Exception as e:
                    if len(langsp) == len(files_and_config) == len(langst) == 1:
                        raise
                    successt = False
                    print(prnt.ljust(ln - 7, '.') + ".FAILED")
                    print(e)
    return ("(done" + (" -- but not all files were parsed correctly, these are not tested." if not successp else "")
            + ")\nTranslator test " + ("successful" if successt else "failed") + "!")


def run_all(langp, langf):
    print('TODO, not implemented yet')
    exit(-1)


def run_dummy(langp, langt):
    if not langp:
        langp = DUMMYLANG_P
    if not langt:
        langt = DUMMYLANG_T
    return _run(langp, langt, DUMMYFILE)


def run_test(langp, langt, file):
    if not langp:
        warnings.warn('No input language provided, trying to derive language from file extension of provided file.')
        langp = file.split('.')[-1]
    if not langt:
        langt = DUMMYLANG_T
    return _run(langp, langt, {langp.upper():[file]})


if __name__ == "__main__":
    argp = argparse.ArgumentParser(description='Run Translator')
    argp.add_argument('-i', '--input-lang', type=str, help='input language of files')
    argp.add_argument('-l', '--lang', type=str, help='test language for translating')
    argp.add_argument('-a', '--all', action='store_true', help='run complete test')
    argp.add_argument('-d', '--dummy', action='store_true', help='run dummy files (for input/lang)')
    argp.add_argument('-f', '--file', type=str, help='run single file')
    args = argp.parse_args()
    if args.all:
        print('Running translator test on complete test set. If no input (-i) and/or test language (-l) is provided,'
              ' all available languages are parsed/translated during test, respectively.')
        print(run_all(args.input_lang, args.lang))
    elif args.dummy or not args.file:
        print('Running translator test on dummy files. If no input (-i) and/or test language (-l) is provided,'
              ' a dummy selection of languages is parsed/translated during test, respectively.')
        print(run_dummy(args.input_lang, args.lang))
    else:
        print(f'Running translator test on file \'{args.file}\'. If no test language (-l) is provided,'
              ' the file is translated to a dummy selection of languages.')
        print(run_test(args.input_lang, args.lang, args.file)[1])
    exit(0)
