# Test reasoning
# Authors: K. Van Dessel

import os
import sys
import shutil
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from test_scripts.test_translators import test_translator
from modeller import Modeller
import runners
import runners.runner_IDPZ3
import runners.runner_IDP3
import runners.runner_ASP

# files and configuration
test_dir = './test_scripts/_dummyfiles/'
test_inputs = list()
test_inputs.append({'lang': 'IDPZ3', 'file': f'{test_dir}idpz3_dummy.idpz3'})
test_inputs.append({'lang': 'IDP3', 'file': f'{test_dir}idp3_dummy.idp3'})
test_inputs.append({'lang': 'CSV', 'file': f'{test_dir}csv_dummy.csv', 'config_file': f'{test_dir}csv_dummy.json'})
#tests.append({'lang': 'CDMN', 'file': f'{test_dir}cdmn_dummy.xlsx', 'sheets': ["set_better",  "glossary"]})

test_engines = list()
test_engines.append('IDPZ3')
test_engines.append('IDP3')
#test_engines.append('ASP')  # TODO UNCOMMENT ONCE ASP IS WORKING FINE...

out = './tmp'
if not os.path.exists(out):
    os.makedirs(out)


def run_engine(spec: Specification, lang: str):
    print('WIP')
    return


def test_engine(test_bench, languages, do_write=False, do_store=False, do_print=False):
    for lang in languages:
        for test in test_bench:
            try:
                test_file = test[f'file_{lang}']
            except KeyError:
                print('WIP print something saying this one is ignored')
                continue
            prnt = f"run {lang} engine on translated {test['lang']} file {test_file})"

            try:
                #TODO
                model = None # WIP run_engine(test_file, lang)
            except Exception as e:
                print(prnt.ljust(93, '.') + ".FAILED")
                print(e)
            else:
                print(prnt.ljust(97, '.') + ".OK")
                if do_print:
                    print(model)
                if do_write and model:
                    output_file = f"{out}/{test_file.split('/')[-1]}.{lang.lower()}.out"
                    with open(output_file, 'w') as fo:
                        fo.write(model)
                if do_store and model:
                    if not do_write:
                        print('warning: can only store output file path, '
                              'since `do_write=False` there is nothing to store')
                        continue
                    test[f'rslt_{lang}'] = output_file


if __name__ == '__main__':
    print("Reasoning engine test")
    print("*** WIP: DOES NOT TEST ENGINES YET + IS ACTUALLY A COMPLETE TEST OF 'EVERYTHING', "
          "CREATING A SEPARATE ONE FOR ENGINE RUNNERS IS ALSO WIP ***")
    print("DO YOU WANT TO PERFORM THIS TEST ANYWAY? Y/N")
    if input().lower() not in ["y", "yes"]:
        print("GOOD CALL, BYE!")
        exit()
    print("SUIT YOURSELF!\n")
    # parse input languages/files to specifications
    test_parser(test_inputs, do_store=True)
    # store input files (for debugging or comparison)
    list(map(lambda x: shutil.copy(x['file'], f"{out}/{x['file'].split('/')[-1]}.in"), test_inputs))

    print('---')

    # translate specifications to output languages/files
    test_translator(test_inputs, test_engines, do_write=True, do_store=True)

    print('---')

    # reason on specifications of output languages/files
    test_engine(test_inputs, test_engines, do_write=True)
