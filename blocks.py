# Blocks for internal representation (vocabulary, theory, structure, (term))
# Kylian Van Dessel, Robin De Vogelaere

from __future__ import annotations

import re
import warnings
from abc import ABC

import nodes
from manipulators.finder_visitors import *
from manipulators.adder_visitors import *
from manipulators.updater_visitors import *
from manipulators.deleter_visitors import *
from nodes import BlockName


class Block(ABC):
    def __init__(self, node: Node):
        self.tree = node  # type: Node

    @property
    def kind(self):
        """ Block kind, e.g. STRUCTURE """
        return self.tree.symbol

    @property
    def name(self):
        """ Block name, e.g. 'S' """
        return extract_name(self.tree.children[0])

    @name.setter
    def name(self, s: str):
        self.tree.children[0] = Node.Name(s)

    @property
    def vocname(self):
        """ Block's interpreted vocabulary, e.g. 'V' """
        if self.kind == 'VOCABULARY':
            #warnings.warn(f"'{self.name}' is already a vocabulary, it doesn't interpret one.")
            raise AttributeError(f"'{self.name}' is already a vocabulary, it doesn't interpret one.")
        return extract_name(self.tree.children[1])

    @vocname.setter
    def vocname(self, s: str):
        self.tree.children[1] = Node.Name(s)

    @property
    def body(self):
        """ Block body. i.e. *DECLARATIONS, *INTERPRETATIONS, or *CONSTRAINTS"""
        return self.tree.children[2].children

    @body.setter
    def body(self, nodes: list):
        self.body = nodes

    @body.deleter
    def body(self):
        self.body = []

    def __str__(self):
        s = f'({self.kind.lower()} block)\n{self.kind} {self.name}'
        try:
            s += f': {self.vocname}'
        except AttributeError:
            pass
        return s + '\n\t' + '\n\t'.join(map(str, self.body))

    # getters
    def get_signature(self):
        """ Get the signature of the block              ,e.g., `structure S: V{<assignments>}` -> `structure S: V` """
        try:
            return f'{self.kind} {self.name}: {self.vocname}'
        except AttributeError:
            pass
        return f'{self.kind} {self.name}'

    def is_empty(self):
        """ Assert if block is empty. """
        return len(self.body) == 0

    # finders
    def get_predicates(self):
        """ Get a list of all predicate symbols in the structure. """
        return PredicatesFinder().visit(self.tree)

    def get_fully_interpreted_predicates(self):
        """ Get a list of all predicate symbols that are provided with a complete (two-valued) interpretation.
            (no three-valued interpretation). """
        return FullyInterpretedPredicatesFinder().visit(self.tree)

    def get_functions(self):
        """ Get a list of all function symbols in tree. """
        return FunctionsFinder().visit(self.tree)

    def get_symbols(self):
        """ Get a list of all symbols in tree. """
        return self.get_predicates() + self.get_functions()

    def get_objects(self):
        """ Get a list of all the definied objects (for a vocab = constructed from, for struct all interpretations) ."""
        return DomainElementsFinder().visit(self.tree)

    # adders
    def add_to_body(self, nodes: Node | list):
        """ Add node(s) to the body of the block. """
        if not isinstance(nodes, list):
            nodes = [nodes]
        self.body.extend(nodes)

    # setter / permutations
    def rename(self, old_name, new_name):
        NameReplacer(old_name, new_name).visit(self.tree)

    def match_vocabulary(self, vocab_or_matcher, create_new_matcher=True):
        """ Complete symbol data with typing info from vocabulary. """
        matcher = (SignatureAdder(vocab_or_matcher) if create_new_matcher else vocab_or_matcher)
        matcher.add_freevars(self.tree)
        self.tree = matcher.visit(self.tree)
        return matcher

    def match_asp_naming_convention(self, vocab_or_map, create_new_map=True):
        """ Change symbol and variable names to comply with ASP naming conventions. """
        mapping = (vocab_or_map.get_asp_naming_convention_mapping() if create_new_map else vocab_or_map)
        self.tree = CaseMatcher(mapping).visit(self.tree)
        return mapping

    def convert_functions(self, *args):
        raise NotImplementedError()

    # removers
    def clear(self):
        del self.body

    # representations
    def print_symbols(self):
        """ Print all symbols in tree. """
        print(f'Symbols in {self.kind.lower()} {self.name}: {", ".join(map(str, self.get_symbols()))}')

    def print(self, print_depth: int = 2):
        """ Print parse tree with a given depth. """
        self.tree.print(print_depth)


class Vocabulary(Block):
    def __init__(self, node: Node):
        """ Create Vocabulary object from tree. Additionally store list of function symbols"""
        super(Vocabulary, self).__init__(node)

    @classmethod
    def new(cls, name):
        return cls(Node.Block(kind=BlockName.VOCABULARY, name=name))

    # getters
    def get_declarations(self):
        """ Return all declarations in vocabulary. """
        return self.body

    def get_declared_symbols(self):
        """ Return all symbols in vocabulary. """
        return self.get_symbols()

    def get_declared_types(self):
        """ Return all type symbols in vocabulary. """
        return list(filter(Symbol.is_type, self.get_declared_symbols()))

    # TODO probably doesnt belong here...
    def get_asp_naming_convention_mapping(self):
        """ Return a mapping for all symbols in the vocabulary to match the naming conventions of ASP. """
        mapping = {}
        for symb in self.get_declared_symbols():  # change all symbol names to asp standards
            new_name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', re.sub('(.)([A-Z][a-z]+)', r'\1_\2', symb.name)).lower()
            mapping[symb] = new_name
        if len(mapping.values()) == len(set(mapping.values())):  # OK if each symbol name unique
            return mapping

        # make types unique
        for typesymb in self.get_declared_types():
            # if not mapping[typesymb].startswith('type_'):
            #     mapping[typesymb] = 'type_' + mapping[typesymb]
            i = 2
            typename = mapping[typesymb]
            while list(mapping.values()).count(mapping[typesymb]) > 1:
                mapping[typesymb] = typename + str(i)
                i += 1

        # make symbols unique
        for (symb, name) in [(s, n) for (s, n) in list(mapping.items()) if (list(mapping.values()).count(n) > 1)]:  #
            try:
                typing = [y for z in [extract_name(x) for x in symb.typing] for x, y in list(mapping.items()) if
                          x.name == z]
            except AttributeError:
                continue
            mapping[symb] = f"{name}'{'_'.join(typing)}"

            i = 2
            while list(mapping.values()).count(mapping[symb]) > 1:
                mapping[symb] = f"{name}{i}'{'_'.join(typing)}"
                i += 1
        return mapping

    # finders
    def get_predicate(self, node: Node) -> Node:
        """ Find a predicate Node in the list of predicate( symbol)s, return matching predicate(s) as Node objects. """
        psymb = Symbol(node).find_in(self.get_predicates())
        return psymb.to_node()

    def get_function(self, node: Node) -> Node:
        """ Find a function Node in the list of functions, return matching function(s) as Node objects. """
        fsymb = Symbol(node).find_in(self.get_functions())
        return fsymb.to_node()

    def get_return_type_of_function(self, func: Node) -> Node:
        """ Find a function Node's return type. """
        return Symbol(self.get_function(func)).return_type

    # adders
    def add_declarations(self, nodes: list):
        """ Add a list of symbol declarations to vocabulary. """
        return [self.add_declaration(node) for node in nodes]

    def add_declaration(self, node: Node):
        """ Add a symbol declaration to vocabulary. """
        # TODO maybe this raises new errors, this is something we didn't check before (only w. csv)
        assert len(node.children) < 2 or \
               Symbol(node.children[0]).is_type(), "An enumeration is only allowed with a type declaration."
        self.add_to_body(node)

    def add_type(self, name: str, enumeration: list = None):
        """ Add a type symbol to vocabulary, create a declaration first. """
        pred_node = Node.Type(name)
        if enumeration is not None:
            enum_node = Node("PRED_ENUM", [Node.Name(str(symbol)) for symbol in enumeration])
            self.add_declaration(Node("DECLARATION", [pred_node, enum_node]))
        else:
            self.add_declaration(Node("DECLARATION", [pred_node]))
        return pred_node

    def _add_pred_func(self, symbol, name: str, typing: list = None):
        """ Add a symbol to vocabulary, create a declaration first. """
        # TODO replace by Symbol.node()
        name_node = Node.Name(name)
        if typing is None:
            typing = []
        if not isinstance(typing, list):
            typing = [typing]
        typing_list = [Node.Name(type_name) for type_name in typing]
        typing_node = Node("TYPING", typing_list)
        symb_node = Node(symbol, [name_node, typing_node])
        self.add_declaration(Node("DECLARATION", [symb_node]))
        return symb_node

    def add_predicate(self, name: str, typing: list = None):
        """ Add a predicate symbol to vocabulary, create a declaration first. """
        if typing is None:
            typing = []
        return self._add_pred_func("PRED", name, typing)

    def add_function(self, name: str, typing: list):
        """ Add a function symbol to vocabulary, create a declaration first. """
        return self._add_pred_func("FUNC", name, typing)

    def add_constant(self, name: str, typing: str):
        """ Add a constant symbol to vocabulary, create a declaration first. """
        return self.add_function(name, [typing])

    def merge(self, vocab):
        """ Merge vocabulary with another vocabulary. """
        self.add_declarations(vocab.get_declarations())

    # setters / permutations
    def convert_functions(self, theor):
        """ Convert function symbols to predicate symbols. Add implied constraints of converted functions to theory. """

        def func_cons(symb):
            symb = Symbol(symb)
            args_ = list(map(lambda i: Node.Name(f'x{str(i)}'), range(len(symb.typing))))   # xi
            args = list(map(lambda x: Node('TERM', [x]), args_))
            vars_ = [Node('VAR', [x, t]) for x, t in zip(args_, symb.typing)]     # xi[Ti]  # TODO replace by Var Node
            quan_vars, aggr_var = vars_[:-1], vars_[-1]
            #cons = ('! %(vars)s:' % data if args_[:-1] else '') + '#{ %(var)s: %(symb)s(%(args)s) } =%(pf)s 1.' % data  # ! IDP dependent # TODO RM
            #TODO check aggr syntax tree!
            cons = Node('CMP', [
                ('=<' if symb.is_partial() else '='),
                Node('AGGR', ['CNT', [aggr_var, Node('ATOM', [Node('PRED', [symb.name]), *args])]]),
                1])

            if args_[:-1]:
                cons = Node('FORALL', [quan_vars, cons])
            return SignatureAdder(self).visit(Node('CONSTRAINT',cons))

        convertor = FunctionDeclarationEliminator()
        self.tree = convertor.visit(self.tree)
        for x in list(convertor.converted.values()):
            theor.add_constraint(func_cons(x))
        return convertor.converted

    # TODO CHECKOUT why this method is here (and seems unused), it was an important one...
    def make_opens(self, closed: list):
        """ Returns constraint '{P(X0,X1)} :- T1(X0), T1(X2).' for every predicate P(T1,T2) not closed in structure."""

        def make_open(pred):
            types = pred.typing
            n = len(types)
            varbase = 'X'
            vars_ = []
            atoms = []
            for i in range(n):
                x = varbase + str(i)
                vars_.append(x)
                atoms.append(f'{extract_name(types[i])}({x})')
            return f'{{ {pred.name}({",".join(vars_)})}} :- {", ".join(atoms)}.'

        def todo_pred(p, closed):
            return not any(p.find_in(closed))

        todo = [x for x in self.get_predicates() if todo_pred(x, closed)]
        return '\n'.join(map(make_open, todo))

    # removers
    def remove_declaration(self, name: str):
        # TODO replace implementation by visitor (see next methods..)
        declarations = self.tree.children[2].children
        for node in declarations:
            if extract_name(node) == name:
                declarations.remove(node)
                break

    def remove_objects(self, symb: Node | Symbol, obj: Node | str):
        self.tree = DeclarationRemover(symb, obj).visit(self.tree)

    def remove_predicate(self, symb: Node):
        self.tree = DeclarationRemover(symb).visit(self.tree)


class Structure(Block):
    def __init__(self, node: Node):
        """ Create Structure object from tree containing a list of symbol interpretations. """
        super().__init__(node)
        self.add_arity()

    @classmethod
    def new(cls, name, vocname=''):
        return cls(Node.Block(kind=BlockName.STRUCTURE, name=name, vocname=vocname))

    # getters
    def get_interpretations(self):
        """ Return all interpretations in structure. """
        return self.body

    # finders
    def get_interpretation_of(self, symb: str | Node):
        """ Return the interpretation of a predicate symbol by its node or name. """
        if isinstance(symb, str):
            symb = Node('PRED', [Node('NAME', [symb])])
        return EnumerationFinder(symb).visit(self.tree)

    def get_nb_of_elements(self, symb: str | Node):
        """ Return the number of elements in the interpretation of a symbol. """
        return len(self.get_interpretation_of(symb))

    def get_extremal_of_elements(self, symb: str | Node, minmax):
        """ Return the min or max value in the interpretation of a given predicate symbol. """
        try:  # first try to convert all to a float and use value corresponding to extremal float
            out = minmax([(float(x), x) for x in map(extract_name, self.get_interpretation_of(symb))],
                         key=lambda tup: tup[0])[1]
        except Exception:  # use alphabetical order
            out = minmax(map(extract_name, self.get_interpretation_of(symb)))
        return out

    def get_max_of_elements(self, symb: str | Node):
        return self.get_extremal_of_elements(symb, max)

    def get_min_of_elements(self, symb: str | Node):
        return self.get_extremal_of_elements(symb, min)

    # adders
    def add_interpretations(self, nodes: list):
        """ Add a list of symbol interpretations to structure. """
        return [self.add_interpretation(node) for node in nodes]

    def add_interpretation(self, node: Node):
        """ Add a symbol interpretation to structure. """
        self.add_to_body(node)

    def add_interpretation_q(self, symb: Node, enumeration: list):
        return InterpretationAdder(symb, enumeration).visit(self.tree)

    def _add_pred_func(self, symb: str, name: str, enum: list, arity: int = None, typing: list = None,
                       else_value: str|int|float = None, nvalued: str = None):
        """ Add a symbol interpretation to structure, create an ASSIGNMENT node first. """
        def create_enum(enum: list):
            if enum == []:
                return enum, None
            _enum = [([x] if not isinstance(x, list) else x) for x in enum]
            _arity = len(_enum[0])
            if _arity == 1:
                _enum = [Node.Name(x[0]) for x in _enum]
            else:
                _enum = [Node("TUPLE", [Node.Name(y) for y in x]) for x in _enum]
            return _enum, _arity
        if enum is None:
            warnings.warn('Tried to add *no* interpretation to structure. Structure is left unchanged.')
            return

        if isinstance(enum, tuple):
            raise TypeError('Too much hassle to support tuples as they are actually also not used, '
                            'so no clue how you got here...')
        symb_node = Node.Symbol(symb, name, arity, typing)

        _symb = Symbol(symb_node)
        _enum, _arity = create_enum(enum)
        ENUM = ('PRED_ENUM' if _symb.is_pred() else 'FUNC_ENUM')
        if _arity is not None:
            _arity = (_arity - 1 if _symb.is_func() else _arity)
            try:
                if _symb.arity != _arity:
                    raise ValueError(f'Trying to add an interpretation to structure {self.name}, '
                                     f'but there is a mismatch between the symbols arity of {_symb} '
                                     f'(arity={_symb.arity}) and the enumeration\'s arity (arity={_arity}).')
            except AttributeError:
                _symb.arity = _arity

        enum_node = Node(ENUM, _enum)
        else_node = Node('ELSE', [Node.Name(else_value)]) if else_value is not None else None
        ctcf_node = Node('NVALUED', [nvalued]) if nvalued else None
        assignment = Node('ASSIGNMENT', list(filter(None, [symb_node, enum_node, else_node, ctcf_node])))
        self.add_interpretation(assignment)

    def add_type(self, name: str, enum: list):
        """ Add a type symbol interpretation to structure, create an assignment first. """
        self.add_predicate(name, enum, arity=1)

    def add_predicate(self, name: str, enum: list, typing: list = None, arity: int = None,
                      nvalued: str = None):
        """ Add a predicate symbol interpretation to structure, create an assignment first. """
        self._add_pred_func("PRED", name, enum, nvalued=nvalued, typing=typing, arity=arity)

    def add_function(self, name: str, enum: list|dict, typing: list = None, arity: int = None,
                     else_value: str|int|float = None, nvalued: str = None):
        """ Add a function symbol interpretation to structure, create an assignment first. """
        if isinstance(enum, dict):
            enum_list = []
            for key, value in enum.items():
                enum_list.append((*key, value))
            enum = enum_list
        self._add_pred_func("FUNC", name, enum, else_value=else_value, nvalued=nvalued, typing=typing, arity=arity)

    def add_object_to_interpretation(self, symb: Node | Symbol, obj: Node | str):
        self.tree = InterpretationExtender(symb, obj).visit(self.tree)

    def merge(self, struc):
        """ Merge self with another structure. """
        self.add_interpretations(struc.get_interpretations())

    # setters / permutations
    def add_arity(self):
        """ Add the symbols' arity to the symbols in the structure. """
        self.tree = ArityAdder().visit(self.tree)

    def add_typing(self, vocab: Vocabulary):
        """ Add the symbols' typing info to the symbols in the structure. """
        self.match_vocabulary(vocab)

    def convert_functions(self):
        """ Convert function assignments to predicate assignments. """
        self.tree = FunctionAssignmentEliminator().visit(self.tree)

    # removers
    def remove_interpretation(self, name: str):
        # TODO replace by usage of (finder) visitor
        interpretations = self.tree.children[2].children
        for node in interpretations:
            if extract_name(node) == name:
                interpretations.remove(node)
                break

    def remove_objects(self, symb: Node | Symbol, obj: Node | str, all_occur=False):
        self.tree = InterpretationRemover(symb, obj, all_occur=all_occur).visit(self.tree)

    def remove_predicate(self, symb: Node):
        self.tree = InterpretationRemover(symb).visit(self.tree)


class Theory(Block):
    def __init__(self, node: Node):
        super(Theory, self).__init__(node)

    @classmethod
    def new(cls, name, vocname=''):
        return cls(Node.Block(kind=BlockName.THEORY, name=name, vocname=vocname))

    # getters
    def get_constraints(self):
        """ Return all constraints in the theory. """
        return self.body

    # adders
    def add_constraint(self, node: Node):
        """ Add a constraint to the theory. """
        self.add_to_body(node)

    def add_constraints(self, nodes: list):
        """ Add a list of constraints to theory. """
        return [self.add_constraint(node) for node in nodes]

    def merge(self, theor):
        # TODO remove duplicates + merge into original or new?
        self.add_constraints(theor.get_constraints())

    # removers
    def remove_constraints_containing_symbol(self, symb: Node):
        """ Remove all constraints containing the given predicate. """
        # TODO think about how we would want to do this, i.e., isn't there anything better than completely removing
        #  the constraints? e.g. for an AND and OR statement it can just be omitted, you could argue...
        #  for an implication, the full statement should be dropped, and so on...
        self.tree = ConstraintRemover(symb).visit(self.tree)
    # def remove_constraints_containing(self, item):
    #     """Removes all constraints containing the specified item"""
    #     # TODO Replace recursive method with visitor...
    #     # TODO @RDV guess I, Me! Mario!, did you a solid - check Theory.remove_predicate()
    #     constraints = self.tree.children[2].children
    #     self.tree.children[2].children = [node for node in constraints if not self._recursive_find(node, item)]
    # def _recursive_find(self, node, item):
    #     if isinstance(node, list):
    #         for element in node:
    #             if self._recursive_find(element, item):
    #                 return True
    #     elif not isinstance(node, Node):
    #         return False
    #     elif self._find_item(node, item):
    #         return True
    #     else:
    #         for child in node.children:
    #             if self._recursive_find(child, item):
    #                 return True
    #     return False
    # def _find_item(self, node, item):
    #     if node.symbol != "NAME":
    #         return False
    #     elif node.children[0] == item:
    #         return True
    #     else:
    #         return False

    def remove_constraint(self, node: Node):
        """ Remove a constraint from the theory. """
        constraints = self.tree.children[2].children
        for constraint in constraints:
            if node == constraint:
                constraints.remove(node)

    def remove_constraints(self, nodes: list):
        """ Remove a list of constraints from the theory. """
        return [self.remove_constraint(node) for node in nodes]


    # permutations
    def expand_implications(self):
        """ Convert implications to their basic logical form. """
        self.tree = ImplicationExpander().visit(self.tree)

    def convert_exists(self):
        """ Convert existential quantors to their comparison equivalent. """
        self.tree = ExistsEliminator().visit(self.tree)

    def convert_functions(self, vocab: Vocabulary):
        """ Convert functions to auxiliary variables and map these to the corresponding (function) predicates. """
        self.tree = FunctionEliminator(vocab).visit(self.tree)

    def negate_constraints(self):
        """ Convert constraints to their inverted form. """
        self.tree = Negator().visit(self.tree)

    def nnf(self):
        """ Convert constraints to normal negation form. """
        self.tree = NNF().visit(self.tree)

    def split_forall(self):
        """ Convert all universal quantors over multiple variables to separate quantors per variable. """
        self.tree = QuantorSplitter().visit(self.tree)

    def normalize_aggregates(self):
        self.tree = AggregateNormalizer().visit(self.tree)

    def normalize(self):
        self.expand_implications()  # convert implications to basic logical form, e.g., A=>B to ~A|B
        self.split_forall()  # convert quantors to single variable quantors, e.g., !x y ... z: to !x: !y: ... !z:
        self.nnf()  # put in Normal Negation Form
        # self.convert_exists()     # convert existential quantors to aggregates


class Optimise(Block):
    def __init__(self, node: Node):
        """ Create Optimise (Term) object from tree. """
        super(Optimise, self).__init__(node)
        self.minimize = True

    # permutations
    def negate_term(self):
        """ Convert term to its inverted form. e.g., t <=> -t """
        self.tree = Negator().visit(self.tree)

    def convert_count_to_sum(self):
        """ Convert cardinality aggregate to sum. """
        self.tree = AggregateCountToSum().visit(self.tree)

    def normalize(self):
        self.convert_count_to_sum()

class Query(Block):
    def __init__(self, node: Node):
        """ Create Query object from tree. """
        super(Query, self).__init__(node)
