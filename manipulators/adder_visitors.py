# Setters / adder visitors for setting or adding any information to a specification
# Authors: Kylian Van Dessel, Robin De Vogelaere, and Joost Vennekens

from __future__ import annotations

from visitors import VocabularyVisitor, StructureVisitor, TheoryVisitor
from manipulators.finder_visitors import FreeVarFinder
from nodes import Node, Symbol


class Adder(VocabularyVisitor, StructureVisitor, TheoryVisitor):
    """ Add elements (nodes, as well as parts of it) to node tree. """

    def __init__(self, symb: Symbol | Node = None, names: list = None):
        if isinstance(symb, Node):
            symb = Symbol(symb)
        self.symb = symb
        if names:
            if not isinstance(names, list):
                names = [names]
            names = list(map(lambda x: (Node.Name(x) if isinstance(x, str) else x), names))
        self.names = names

    pass


# Symbol signature -----------------------------------------------------------------------------------------------------

class SignatureAdder(Adder):
    """ Add symbol signature (typing/arity info) to symnbols,
        i.e. Replace all symbols with their matching vocabulary symbol (contains signature).
    """

    def __init__(self, vocab, freevars=[]):
        self.vocab = vocab  # type: Vocabulary
        self.freevars = freevars

    def add_freevars(self, node, *args):
        self.freevars = FreeVarFinder(self.vocab).visit(node)

    def visit_PRED(self, node, *args):
        return self.vocab.get_predicate(node)

    def visit_FUNC(self, node, *args):
        return self.vocab.get_function(node)

    def visit_TERM(self, node, *args):
        if node.children[0] in self.freevars:
            return Node('TERM', [self.vocab.get_function(
                Node('FUNC', [node.children[0], Node('ARITY', [0])]))])
        return super(SignatureAdder, self).visit_TERM(node, *args)


class ArityAdder(StructureVisitor):
    """ Add symbols arity based on assigned enumeration."""

    #def __init__(self):
    #    pass

    def visit_ASSIGNMENT(self, node, *args):
        if any([x for x in node.children[0].children if x.symbol == 'ARITY']):  # node already has arity information
            return node
        try:
            n = self.visit(node.children[1])
        except IndexError:
            pass
        else:
            node.children[0].children.append(Node('ARITY', [n]))
        return node

    def visit_PRED_ENUM(self, node, *args):
        try:
            return len(node.children[0].children)
        except IndexError:
            raise IndexError(f"No interpretation found for '{str(node)}'.")

    def visit_FUNC_ENUM(self, node, *args):
        return self.visit_PRED_ENUM(node, *args) - 1


# Symbol (interpretation) ----------------------------------------------------------------------------------------------

# TODO check if this works/is used
class DeclarationAdder(Adder):
    """ Add a declaration to vocabulary. """

    def visit_VOCABBODY(self, node, *args):
        node.children.append(Node('DECLARATION', [self.symb.to_node()]))


class InterpretationAdder(Adder):
    """ add an interpretation, e.g., T := { a, b } """

    def visit_STRUCBODY(self, node, *args):
        node.children.append(Node('ASSIGNMENT', [self.symb.to_node(), Node('PRED_ENUM', self.names),
                                                 Node('NVALUED', [None])]))


# TODO check if this works instead:  class InterpretationExtender(Adder):
class InterpretationExtender(StructureVisitor, VocabularyVisitor):
    """ add (an) object(s) to an existing interpretation,
         e.g., T := { a, b } w. ObjectAdder(T, x) -> T := {a, b, x} """

    def __init__(self, symb, names):
        if isinstance(symb, Node):
            symb = Symbol(symb)
        self.symb = symb
        if not isinstance(names, list):
            names = [names]
        names = list(map(lambda x: (Node.Name(x) if isinstance(x, str) else x), names))
        self.names = names

    def visit_ASSIGNMENT(self, node, *args):
        if Symbol(node.children[0]).matches(self.symb):
            node.children[1].children.extend(self.names)
        return node

    def visit_DECLARATION(self, node, *args):
        self.visit_ASSIGNMENT(node)
