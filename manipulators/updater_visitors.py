# Updaters / permutation visitors for changing (structures of) information in a specification
# Authors: Kylian Van Dessel, Robin De Vogelaere, and Joost Vennekens

from __future__ import annotations

from symtable import Symbol

from visitors import VocabularyVisitor, StructureVisitor, TheoryVisitor
from manipulators.finder_visitors import DefinedSymbolFinder
from nodes import Node, Symbol, extract_name
#from translators.idpz3_visitors import IDPZ3Theory as IDPTheory # ! makes system IDP dependent
# TODO replace previous import by, check first:
#  from translators.translator_IDPZ3 import translate_theory

# function_list = []
# TODO remove previous, check first

# Visitors to:
#  - Replace naming of symbols or variables
#  - Replace operators or functionality by equivalent
#  - Rewrite formula's  (= Replace formulas by equivalent)
#  - Manipulate formula's (= Replace formulas by non-equivalent)


# Replace naming of symbols or variables -------------------------------------------------------------------------------


class NameReplacer(TheoryVisitor):
    """ Change all occurrences of a name (symbol name, var name, ... doesn't matter) to a new name. """

    def __init__(self, old_name, new_name):
        self.find = old_name
        self.replace = new_name

    def visit_NAME(self, node, *args):
        if node.get_name() == self.find:
            return Node.Name(self.replace)
        return node


class PredicateReplacer(TheoryVisitor):
    """ Rename all occurrences of a given predicate. """

    def __init__(self, symb, name):
        self.goal = symb
        self.name = name

    def visit_PRED(self, node, *args):
        if Symbol(node).matches(self.goal):
            return Node('PRED', [Node.Name(self.name)] + node.children[1:])
        return node


class CaseMatcher(TheoryVisitor, StructureVisitor, VocabularyVisitor):
    """ Change symbols to (lower) snake_case and variables to upper case. """

    def __init__(self, mapping):
        self.mapping = mapping

    def visit_PRED(self, node, *args):
        try:
            p_mapped = self.mapping[Symbol(node)]
        except KeyError:
            raise KeyError(f'Predicate {Symbol(node)} not in list of symbols.')
        return Node(node.symbol, [Node.Name(p_mapped)] + list(map(self.visit, node.children[1:])))

    def visit_FUNC(self, node, *args):
        try:
            f_mapped = self.mapping[Symbol(node)]
        except KeyError:
            raise KeyError(f'Function {Symbol(node)} not in list of symbols.')
        return Node(node.symbol, [Node.Name(f_mapped)] + list(map(self.visit, node.children[1:])))

    def visit_TYPING(self, node, *args):
        try:
            t_mapped = list(map(lambda t: self.mapping[Symbol(Node.Type(t))], node.children))
            # TODO here is a (bad) QUICKFIX to skip builtin Int for ASP, but it needs adjustments elsewhere as well...
            #  t_mapped = list(map(lambda t: self.mapping[Symbol.SimpleType(t)],
            #                    [x for x in node.children if extract_name(x) not in ['Int', 'Real']]))
        except KeyError:
            t_unknown = [str(Symbol(Node.Type(x))) for x in node.children
                         if Symbol(Node.Type(x)) not in self.mapping.keys()]
            if all(map(lambda x: x in ['Int/1', 'Real/1'], t_unknown)):
                raise Exception('Built-in types not supported for ASP translation (yet).')
            if any(map(lambda x: x in ['Int/1', 'Real/1'], t_unknown)):
                print(f'warning: Built-in types not supported for ASP translation (yet) '
                      f'but there are other issues as well...')
            raise KeyError(f'Type{"s" if len(t_unknown)>1 else ""} {", ".join(t_unknown)} not in list of symbols.')
            # TODO IDEM
            #  if any(map(lambda x: x in ['Int/1', 'Real/1'], t_unknown)):
            #  print(f'warning: Built-in types not supported for ASP translation (yet) ')
            #return node  # TODO

        return Node(node.symbol, list(map(Node.Name, t_mapped)))

    def visit_VAR(self, node, *args):
        return Node('VAR', [Node.Name(extract_name(node).upper()),
                            Node.Name(self.mapping[Symbol(Node.Type(node.children[1]))])])

    def visit_MIN(self, node, *args):
        return Node('MIN', [Node.Name(self.mapping[Symbol(Node.Type(node.children[0]))])])

    def visit_MAX(self, node, *args):
        return Node('MAX', [Node.Name(self.mapping[Symbol(Node.Type(node.children[0]))])])

    def visit_TERM(self, node, *args):
        if node.children[0].symbol == 'NAME':
            term = extract_name(node)
            if term[0] == '"':
                return Node('TERM', [Node.Name(term.lower())])
            return Node('TERM', [Node.Name(term.upper())])
        return super(CaseMatcher, self).visit_TERM(node, *args)


# Replace operators or functionality by equivalent ---------------------------------------------------------------------

class FunctionAssignmentEliminator(StructureVisitor):
    """ Change all function assignments to predicate assignments. """

    def visit_PRED(self, node, *args):
        return node

    def visit_FUNC(self, node, *args):
        return Node('PRED', [self.visit(x) for x in node.children])

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node)

    def visit_ARITY(self, node, *args):
        node.children[0] += 1
        return node

    def visit_FUNC_ENUM(self, node, *args):
        return Node('PRED_ENUM', node.children)


class FunctionDeclarationEliminator(VocabularyVisitor, TheoryVisitor):
    """ Change function declarations into predicate declarations. Keep dict of converted functions. """

    def __init__(self):
        self.converted = {}  # original function symbol nodes

    def visit_FUNC(self, node, *args):
        # self.function_list.append(node)  # TODO remove, check first
        self.converted[node.get_name()] = node
        return Node('PRED', [self.visit(x) for x in node.children])


class FunctionEliminator(TheoryVisitor):
    """ Remove functions by replacing them with auxiliary variables and adding the function's predicate representation
        to the constraint, mapping the auxiliary variable to the function arguments.
    """

    def __init__(self, vocab=None):
        self.positive = True
        self.counter = 0
        self.vocab = vocab  # needed if typing info of function symbol is missing

    def new_var(self):
        """ return new variable name """
        self.counter += 1
        return Node.Name(f'_x{str(self.counter - 1)}')

    # TODO CHECKOUT changes before removing old statements
    @staticmethod
    def make_pred(eq):
        """ create Node for predicate 'F(..., x)' from eq = ('x', 'T', 'F(...)')."""
        #return f'{extract_name(eq[2])}({",".join(map(IDPTheory().visit, eq[2].children[1:] + [eq[0]]))})' TODO RM
        print(eq)
        print(Node('ATOM', Node.Symbol(kind='PRED', name=eq[2].children[0], arity=len(eq[2].children[1:])+1),
                    *eq[2].children[1:], Node('TERM', [Node.Name(eq[0])])))
        raise Exception('Sorry, just a debugging stop...')
        exit()
        return Node('ATOM', Node.Symbol(kind='PRED', name=eq[2].children[0], arity=len(eq[2].children[1:])+1),
                   *eq[2].children[1:], Node('TERM', [Node.Name(eq[0])]))

    # TODO CHECKOUT changes before removing old statements
    @staticmethod
    def make_var(eq):
        """ create Node for variable 'x[T]' from eq = ('x', 'T', 'F(...)')."""
        #return f'{extract_name(eq[0])} in {extract_name(eq[1])}'  # ! IDP dependent TODO RM
        return Node.Var(eq[0], eq[1])

    # TODO CHECKOUT changes before removing old statements
    def expand_function(self, atom, eqs):
        if eqs == []:  # no function(s) to expand, return atom
            return atom
        #vars_ = ', '.join(map(self.make_var, eqs))  # ! IDP dependent TODO RM
        vars_ = list(map(self.make_var, eqs))
        #body = " & ".join(map(self.make_pred, eqs))  # ! IDP dependent TODO RM
        if len(eqs) > 1:
            body = Node('AND', list(map(self.make_pred, eqs))) # TODO check if this ever happens
        else:
            body = self.make_pred(eqs)
        #if len(eqs) > 1: # TODO RM
        #    body = f'({body})'
        #atom = IDPTheory().visit(atom)  # ! IDP dependent
        if self.positive:  # !x: P(F(x)) -> !x: ?y: F(x,y) & P(y).
            #node = IDPParser().parse_string(f'? {vars_} :  {body} & {atom}', 'formula')  # ! IDP dependent TODO RM
            node = Node('EXISTS', [vars_, Node('AND', body, atom)])
        else:  # !x: ~P(F(1)) -> !x: !y: F(x,y) => ~P(y).
            #node = IDPParser().parse_string(f'! {vars_} :  {body} => {atom}', 'formula')  # ! IDP dependent TODO RM
            node = Node('FORALL', [vars_, Node('LIMP', body, atom)])
        return node

    def visit_CONSTRAINT(self, node, *args):
        self.positive = True
        return super(FunctionEliminator, self).visit_CONSTRAINT(node, *args)

    def visit_NOT(self, node, *args):
        self.positive = not self.positive
        return super(FunctionEliminator, self).visit_NOT(node, *args)

    def visit_ATOM(self, node, *args):
        eqs = []
        atom = super(FunctionEliminator, self).visit_ATOM(node, eqs)
        return self.expand_function(atom, eqs)

    def visit_TERM(self, node, *args):
        if node.children[0].symbol in ['FUNC', 'PFUNC']:
            func_ = node.children[0]  # function symbol
            args_ = node.children[1:]  # arguments
            try:  # return type
                type_ = Symbol(func_).return_type
            except AttributeError:
                try:
                    type_ = self.vocab.get_return_type_of_function(func_)
                except AttributeError:
                    print('Function symbol typing is missing, make sure it is there '
                          'or provide the vocabulary where to find it.')
                    raise
            var = self.new_var()
            args[0].append((var, type_, Node('TERM', [func_] + [self.visit(x, *args) for x in args_])))
            return Node('TERM', [var])
        return super(FunctionEliminator, self).visit_TERM(node, *args)

    def visit_PRED(self, node, *args):
        return node

    def visit_FUNC(self, node, *args):
        return Node('PRED', [self.visit(x) for x in node.children])

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node)

    def visit_ARITY(self, node, *args):
        node.children[0] += 1
        return node

    def visit_SETOBJ(self, node, *args):  # {(var1 var2 ...): (formula): (term)}
        # convert formula
        node.children[1] = self.visit(node.children[1])

        # convert term
        term = node.children[2]
        if term.children[0].symbol == 'NAME':  # simple term, nothing to (additionally) convert
            return node
        eqs = []
        node.children[2] = self.visit(term, eqs)

        # update formula
        phi = list(map(lambda x: Node('ATOM', list(map(self.visit, x[2].children)) + [Node('TERM', [x[0]])]), eqs))
        node.children[1] = Node('AND', [self.visit(node.children[1])] + phi)

        # update variable list
        node.children[0] += list(map(lambda x: Node('VAR', [x[0], x[1]]), eqs))
        return node

    def visit_SET(self, node, *args):  # {(var1 var2 ...): (formula)}
        node.children[1] = self.visit(node.children[1], *args)  # convert formula
        return node

    # TODO CHECKOUT changes, make sure lang. dependency is removed.
    def visit_DEFIMP(self, node, *args):
        self.positive = True
        if node.children[1].symbol != "ATOM":
            raise ValueError("Definition head can only contain a single atom, "
                             f"but found the following instead: {node.children[1]}")

        eqs = []  # eq = (var, type, func), e.g., ('xn+1','Tn+1', 'f(x0,...,xn)')
        atom = super(FunctionEliminator, self).visit_ATOM(node.children[1], eqs)
        if eqs == []:
            node.children[2] = self.visit(node.children[2])
            return node

        symb = DefinedSymbolFinder().visit(node)[0].to_node()
        if atom.children[0] == symb:
            head = atom
            #body = " & ".join(map(self.make_pred, eqs))
            if len(eqs) > 1:  # TODO check need
                body = Node('AND', list(map(self.make_pred, eqs)))
            else:
                body = self.make_pred(eqs)
        else:
            i = [i for i, x in enumerate(eqs) if symb == x[2].children[0]][0]
            head = Node('ATOM', list(map(self.visit, eqs[i][2].children)) + [Node('TERM', [eqs[i][0]])])
            #body = " & ".join(list(map(self.make_pred, eqs[:i] + eqs[i + 1:])) + [IDPTheory().visit(atom)])   #TODO RM
            body = Node('AND', list(map(self.make_pred, eqs[:i] + eqs[i+1:])) + [atom])

        body = Node('AND', [body, self.visit(node.children[2])])
        vars_ = node.children[0] + list(map(lambda x: Node.Var(x[0], x[1]), eqs))
        return Node('DEFIMP', [vars_, head, body])

    def visit_OPTIMISEBODY(self, node, *args):
        raise NotImplementedError('Function elimination from OBJECTVALUE (term) block not implemented.')
        eqs = []
        term = self.visit(node.children[0], eqs)
        #term = constraint_to_idp(term).strip('(').strip(')') #TODO RM
        if eqs:
            # TODO check syntax tree for aggregates!
            #term = f'sum{{{self.make_var(eqs[0])}: {self.make_pred(eqs[0])} : {term}}}' #TODO RM
            term = Node('AGGR', ['SUM', self.make_var(eqs[0]), self.make_pred(eqs[0]), term])
            return Node('OBJECTBODY', [Node('TERM', term)])
        return node


class ExistsEliminator(TheoryVisitor):
    """ Replace any existential quantifier by its aggregate equivalent
        e.g., `?x[T]: P(x).`    => `#{x[T]: P(x)} ~= 0 `
        e.g., `?*i x[T]: P(x).` => `#{x[T]: P(x)} * i` , with `*` comparison operator and `i` an integer value
    """
    def visit_NOT(self, node, *args):
        is_neg = not args[0] if args else True
        child = self.visit(node.children[0], is_neg)
        try:  # Came across a universal quantification?
            (child, is_hidden_exists) = child
        except TypeError:
            pass
        else:
            if child.symbol != 'FORALL':
                raise ValueError(f'Got an unexpected {child.symbol} node where a "FORALL" child node was expected.')

            if is_hidden_exists:
                return self.visit_EXISTS(child)
        return Node('NOT', [child])

    def visit_FORALL(self, node, *args):
        if args:
            return super(ExistsEliminator, self).visit_FORALL(node), args[0]
        return super(ExistsEliminator, self).visit_FORALL(node)

    # TODO check if this works properly
    def visit_EXISTS(self, node, *args):
        print('warning: \'exsistential quantification to cardinality aggregate conversion\' functionality not tested properly')
        set_ = Node('SET', [node.children[0], self.visit(node.children[1])])
        aggr_term = Node('TERM', [Node('AGGRSET', ['CNT', set_])])
        #aggr_term = Node('TERM', [Node('AGGR', ['CNT', node.children[0], self.visit(node.children[1])])])
        if len(node.children) > 2:
            aggr_cmp, aggr_val = node.children[2][:2]
            return Node('ATOM', [Node('CMP', [aggr_cmp, aggr_term, aggr_val])])  # ?*i x:f. -> #{x:f}*i.
        return Node('ATOM', [Node('CMP', ['NEQ', aggr_term, Node('TERM', [Node.Name('0')])])])  # ?x:f.    -> #{x:f}~=0.


class ImplicationExpander(TheoryVisitor):
    """ Expand implications to their (simple) logical form.
        e.g., `A <=> B` -> `A => B & A <= B` -> `(~A | B) & (A | ~B)`
    """

    def visit_LIMP(self, node, *args):
        return Node('OR', [Node('NOT', [self.visit(node.children[0])]), self.visit(node.children[1])])

    def visit_RIMP(self, node, *args):
        return Node('OR', [Node('NOT', [self.visit(node.children[1])]), self.visit(node.children[0])])

    def visit_EQUIV(self, node, *args):
        return Node('AND', [self.visit_LIMP(node), self.visit_RIMP(node)])


# Rewrite formulas (equivalent) ----------------------------------------------------------------------------------------

class QuantorSplitter(TheoryVisitor):
    """ Split quantor over variables into a seperate quantor for each variable.
        e.g., `!x1[T1] x2[T2] ... :` => `!x1[T1]: !x2[T2]: ...`
    """

    def visit_FORALL(self, node, *args):
        vars_ = node.children[0]
        if len(vars_) == 1:
            return super(QuantorSplitter, self).visit_FORALL(node, *args)
        return Node(node.symbol, [[vars_[0]], self.visit(Node(node.symbol, [vars_[1:], node.children[1]]))])


class AggregateNormalizer(TheoryVisitor):
    """ Normalize aggregates, i.e., convert such that the aggregate term has an arity of 0
        e.g., `sum{x[T]: f: g(T)}.` => `sum{x[T] y[T]: f & g(T)=t: t}`
    """

    def __init__(self):
        self.counter = 0

    def new_var(self):
        self.counter += 1
        return Node.Name(f'_z{str(self.counter - 1)}')

    # TODO check if this works properly
    def visit_SETOBJ(self, node, *args):  # { v(arlist) : f(ormula) : t(erm) }
        print('warning: \'aggregate normalisation\' functionality not tested properly')
        g = node.children[2]
        if g.children[0].symbol == 'NAME':  # term already normalized
            return node
        if g.children[0].symbol == 'MATH':
            raise NotImplementedError('Cannot convert aggregates with a mathematical aggregate term because of domain.')
        try:
            type_ = Symbol(g.children[0]).return_type
        except ValueError:
            raise
        var = self.new_var()
        v = node.children[0] + [Node('VAR', [var, type_])]
        t = Node('TERM', [var])
        f = Node('AND', [self.visit(node.children[1]),
                         Node('ATOM', [Node('CMP', ['EQ', t, Node('TERM', [var])])])])
        return Node(node.symbol, [v, f, t])


class AggregateCountToSum(TheoryVisitor):
    """ convert cardinality aggregate to sum aggregate
        e.g., `#{x[T]:phi}` => `sum{x[T]: phi: 1}`
    """

    # TODO check if this works properly
    def visit_AGGRSET(self, node, *args):
        print('warning: \'cardinality to sum aggregate convertion\' functionality not tested properly')
        if node.children[0] == 'CNT':
            node.children[0] = 'SUM'
            #node.children.append(Node('TERM', [Node.Name(1)]))
            node.children[1] = Node('SETOBJ',
                                    node.children[1].children + [Node('TERM', [Node.Name(1)])])
        return node


class NNF(TheoryVisitor):
    """ Convert specification to Negation Normal Form, i.e., negation is only applied directly to atoms
        e.g., `~(A|(B&~C))` -> `~A & (~B|C)`
    """

    @staticmethod
    def flip(symbol):
        # flipped = {'=': '~=', '<': '>=', '>': '=<', 'OR': 'AND', 'FORALL': 'EXISTS'}
        flipped = {'EQ': 'NEQ', 'LT': 'GEQ', 'GT': 'LEQ', 'OR': 'AND', 'FORALL': 'EXISTS', 'TRUE': 'FALSE'}
        flipped.update({v: k for (k, v) in list(flipped.items())})
        return flipped[symbol]

    def visit_NOT(self, node, *args):
        child = node.children[0]
        if child.symbol == "ATOM":
            grchild = child.children[0]
            if grchild.symbol == 'CMP':  # ~(x>y)  -> x=<y
                return Node('ATOM', [Node('CMP', [NNF.flip(grchild.children[0])] + grchild.children[1:])])
            if grchild.symbol == 'BOOL':  # ~true -> false  /  ~false -> true
                return Node.Bool(NNF.flip(grchild.children[0]))
            return node
        if child.symbol == 'NOT':  # ~~A -> A
            return self.visit(child.children[0])
        if child.symbol in ['AND', 'OR']:  # ~(A&B)  -> ~A|~B
            return Node(NNF.flip(child.symbol), [self.visit(Node('NOT', [x])) for x in child.children])
        if child.symbol in ['FORALL', 'EXISTS']:  # ~!x: A. -> ?x: ~A.
            return Node(NNF.flip(child.symbol), [child.children[0], self.visit(Node('NOT', [child.children[1]]))])


# Manipulate formulas (non-equivalent) ---------------------------------------------------------------------------------

class Negator(TheoryVisitor):
    """ Negate/invert atoms or terms.
        e.g., `f.` <=> `not f.`
        e.g., `t` <=> `-t`
    """

    def visit_CONSTRAINT(self, node, *args):
        child = node.children[0]
        if child.symbol == "NOT":
            return Node('CONSTRAINT', [self.visit(child.children[0], *args)])
        return Node('CONSTRAINT', [Node('NOT', [self.visit(node.children[0], *args)])])

    def visit_TERM(self, node, *args):
        if node.children[0].symbol == "MINUS":
            return node.children[0].children[0]
        return Node('TERM', [Node('MINUS', [node])])
