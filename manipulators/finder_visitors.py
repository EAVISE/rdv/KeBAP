# Getters / finder visitors for retrieving any information from a specification
# Authors: Kylian Van Dessel, Robin De Vogelaere, and Joost Vennekens

from __future__ import annotations

from visitors import VocabularyVisitor, StructureVisitor, TheoryVisitor, Visitor
from nodes import Node, Symbol, extract_name


class Finder(VocabularyVisitor, StructureVisitor, TheoryVisitor):
    """ Find Nodes of a certain kind (or parts of them).
        (Optionally) that match a certain other object. (only supported for goal symbols for now)
         Basically, returns a list of all (information of) nodes that are explicitly returned by its visits.
         Create a subclass, add a visit for the kind of node you are looking for and return the information you need.
    """

    def __init__(self, symb=None):
        if symb:
            if isinstance(symb, Node):
                symb = Symbol(symb)
            self.goal = symb

    def visit_noop_rec(self, node, *args):  # TODO replace w. rec2 check if still in use elsewhere
        return self.visit_noop_rec2(node, *args)

    def visit_noop_rec2(self, node, *args):
        return list(set([z for y in map(self.visit, filter(Visitor.op, node.children)) for z in y]))

    def visit_noop_norec(self, node, *args):
        return []

    def visit_FORALL(self, node, *args):
        return list(set([y for x in list(map(self.visit, node.children[0])) for y in x]
                        + self.visit(node.children[1], *args)))

    def visit_EXISTS(self, node, *args):
        return self.visit_FORALL(node, *args)

    def visit_AGGRSET(self, node, *args):
        return self.visit(node.children[1])
        # return self.visit_noop_rec(Node('', node.children[1:]), *args)  # TODO check if this can be removed

    def visit_DEFIMP(self, node, *args):
        return list(set([z for y in list(map(self.visit, filter(Finder.op, node.children[0])))
                         + list(map(self.visit, node.children[1:])) for z in y]))

    def visit_MIN(self, node, *args):
        return set()

    def visit_MAX(self, node, *args):
        return self.visit_MIN(node, *args)  # TODO check if this can be removed


# Getters (return found objects) ---------------------------------------------------------------------------------------

class PredicatesFinder(Finder):
    """ Get all predicate symbols. """

    def visit_PRED(self, node, *args):
        return [Symbol(node)]



class FullyInterpretedPredicatesFinder(PredicatesFinder):
    """ Find all predicate symbols that do not have a three-valued interpretation. """

    def visit_ASSIGNMENT(self, node, *args):
        if node.children[2].children[0] is not None:  # three-valued
            return []
        return self.visit(node.children[0])


class FunctionsFinder(Finder):
    """ Get all function symbols. """

    def visit_FUNC(self, node, *args):
        return [Symbol(node)]


class DefinedSymbolFinder(Finder):
    """ Find the symbol being defined in the definition. """

    def visit_DEFINITION(self, node, *args):
        return list({y for x in list(map(self.visit, node.children)) for y in x})

    def visit_DEFIMP(self, node, *args):
        return self.visit(node.children[1])

    def visit_ATOM(self, node, *args):
        return self.visit(node.children[0])

    def visit_TERM(self, node, *args):
        return self.visit(node.children[0])

    def visit_CMP(self, node, *args):
        return self.visit(node.children[1])

    def visit_PRED(self, node, *args):
        return [Symbol(node)]

    def visit_FUNC(self, node, *args):
        return self.visit_PRED(node, *args)


class UnarySymbolFinder(Finder):
    """ Get all unary predicates (types) and functions (constants), i.e. those with arity 0. """
    def visit_PRED(self, node, *args):
        symb = Symbol(node)
        if symb.arity == 0:
            return [symb]
        return []

    def visit_FUNC(self, node, *args):
        return self.visit_PRED(node, *args)


class SameTypingFinder(Finder):
    '''Finds all predicates and functions with the same arguments'''

    def __init__(self, typing: list):
        self.goal = [t.name for t in typing]

    # TODO CHECKOUT fix arg passing in visitor pattern
    def visit_noop_rec2(self, node, *args):
        return list(set(z for y in [self.visit(ch, *args) for ch in filter(Finder.op, node.children)] for z in y))

    def visit_PRED(self, node, *args):
        pred = Symbol(node)
        try:
            if tuple(self.goal) == tuple(pred.typing):
                return [pred]
        except:
            pass
        return []

    def visit_FUNC(self, node, *args):
        # TODO CHECKOUT changes before removing old statements, watch arity closely
        print('SameTypingFinder for FUNC: ', self.visit_PRED(node, *args))
        #func = Symbol(node)
        #pattern = args
        #try:
        #    typing = func.typing
        #except:
        #    return []
        #typing_tuple = tuple((typ.get_name() for typ in typing[:-1]))
        #if typing_tuple == pattern:
        #    return [func]
        #else:
        #    return []
        return self.visit_PRED(node, *args)


class EnumerationFinder(Finder):
    """ Find the enumeration assigned to a given symbol. """

    def visit_noop_rec(self, node, *args):
        res = [y for y in [self.visit(x) for x in node.children] if y != []]
        if not res:
            return
        if len(res) > 1:
            print(f'warning: Multiple possible enumerations found for \'{self.goal}\'')
        return res[-1]

    def visit_DECLARATION(self, node, *args):
        if Symbol(node.children[0]).matches(self.goal):
            return self.visit(node.children[1])  # TODO CHECKOUT no index error if decl doesn't contain enum?
        return []

    def visit_ASSIGNMENT(self, node, *args):
        # TODO CHECKOUT  + include for theory assignment as well.. (automatically if ASSIGNMENT tag is used?)
        #  return self.visit_DECLARATION(node, *args)
        if Symbol(node.children[0]).matches(self.goal):
            return self.visit(node.children[1])
        return []

    def visit_PRED_ENUM(self, node, *args):
        return node.children

    def visit_FUNC_ENUM(self, node, *args):
        return self.visit_PRED_ENUM(node, *args)


class DomainElementsFinder(Finder):
    """ Get all objects, i.e. type objects of types interpreted in structure/types constructed in vocabulary. """

    def visit_PRED_ENUM(self, node, *args):
        obj_list = []
        for o in node.children:
            if o.symbol == "TUPLE":
                obj_list.extend(o.children)
            elif o.symbol == "NAME":
                obj_list.append(o)
            else:
                print("warning: It is possible that we missed some domain elements.")
        return filter(lambda x: type(x) not in [int, float], set(x.children[0] for x in obj_list))

    def visit_FUNC_ENUM(self, node, *args):
        return self.visit_PRED_ENUM(node, *args)


class FreeVarFinder(Finder):
    """ Find all free variables in a node tree. """
    def __init__(self, vocab=None):
        if not vocab:
            print('warning: Could not fetch any constructed type objects, '
                  'if any they are (wrongly) considered free variables which might lead to errors ')
        self.domain_elements = DomainElementsFinder().visit(vocab.tree) if vocab else []

    @staticmethod
    def varlist(list_):
        'return the set of the variable names in a varlist'
        return set([var_.children[0] for var_ in list_])

    def visit_FORALL(self, node, *args):
        return set(self.visit(node.children[1])) - self.varlist(node.children[0])
        # TODO check this out
        #  return self.visit(node.children[1]) - self.varlist(node.children[0])

    def visit_EXISTS(self, node, *args):  # TODO remove? isn't this inherited from Finder?
        return self.visit_FORALL(node)

    def visit_SETOBJ(self, node, *args):
        return set().union(*list(map(self.visit, node.children[1:]))) - self.varlist(node.children[0])

    def visit_AGGRSET(self, node, *args):  # TODO remove if visit_AGGRSET +-l.38 ok.
        return self.visit(node.children[1])

    def visit_ATOM(self, node, *args):
        return set(super(FreeVarFinder, self).visit_ATOM(node))

    def visit_TERM(self, node, *args):
        if node.children[0].symbol == 'NAME':
            name = extract_name(node)
            try:
                float(name)  # numbers are not free variables
                return set()
            except ValueError:
                if name[0] == '\"':  # "string objects" are not free variables
                    return list(set())
                if name in self.domain_elements:  # domain elements /constructed objects are not free variables
                    return list(set())
                return set([node.children[0]])
        return set([y for x in map(self.visit, node.children) for y in x])

    def visit_DEFIMP(self, node, *args):
        if node.children[0]:
            return self.visit_FORALL(Node('FORALL', [node.children[0], node.children[1]]), *args) \
                .union(self.visit_FORALL(Node('FORALL', [node.children[0], node.children[2]]), *args))
            # TODO check this out
            #  return self.visit(node.children[1]).union(self.visit(node.children[2])) - self.varlist(node.children[0])
        return self.visit(node.children[1]).union(self.visit(node.children[2]))


# Finders (return True if object found else False) ---------------------------------------------------------------------

class SymbolUsageFinder(Finder):
    """ Find if symbol is used anywhere. """

    # TODO simply return true or false?

    def visit_PRED(self, node, *args):
        if Symbol(node).matches(self.goal):
            return [node]
        return []

    def visit_FUNC(self, node, *args):
        return self.visit_PRED(node, args)
