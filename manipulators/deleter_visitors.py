# Deleters / remover visitors for deleting any information from a specification
# Authors: Kylian Van Dessel, Robin De Vogelaere, and Joost Vennekens

from __future__ import annotations

from visitors import VocabularyVisitor, StructureVisitor, TheoryVisitor, Visitor
from manipulators.finder_visitors import SymbolUsageFinder
from nodes import Node, Symbol


class Remover(VocabularyVisitor, StructureVisitor, TheoryVisitor):
    """ Remove (parts of) nodes.
         Basically, returns the complete tree except for (parts of) nodes you explicitly return None for (or remove).
         Create a subclass, add a visit for the node you wish to remove, return None (or the part you wish to keep).
    """

    def visit_noop_rec(self, node, *args):  # TODO check if still in use elsewhere
        return self.visit_noop_rec2(node, *args)

    def visit_noop_rec2(self, node, *args):
        return Node(node.symbol,
                    list(filter(None, [self.visit(x, *args) if Visitor.op(x) else x for x in node.children])))


class ObjectRemover(Remover):
    """ remove one or more objects (obj) from an existing interpretation of a symbol (symb),
         e.g., T := { a, b } w. ObjectRemover(T, b) -> T := {a}
    """
    def __init__(self, symb: Symbol, obj: list | str):
        if not isinstance(obj, list):
            obj = [obj]
        self.objects = list(map(lambda x: (Node.Name(x) if isinstance(x, str) else x), obj))
        if isinstance(symb, Node):
            symb = Symbol(symb)
        self.symbol = symb

    def visit_ASSIGNMENT(self, node, *args):
        if Symbol(node.children[0]).matches(self.symbol):
            return list(filter(lambda x: x not in self.objects, node.children[1].children[1]))
        return node


class InterpretationRemover(Remover):
    """ remove an interpretation from structure
         or remove (an) object(s) from an interpretation in structure if attr objects is provided.
         (all_occur = remove object from all interpretations i.o. only from the given symbol's interpretation
    """

    def __init__(self, symb: Symbol | Node, obj: list | str | None = None, all_occur=False):
        self.objects = None
        if obj:
            if not isinstance(obj, list):
                obj = [obj]
            self.objects = list(map(lambda x: (Node.Name(x) if isinstance(x, str) else x), obj))
        if isinstance(symb, Node):
            symb = Symbol(symb)
        self.symbol = symb
        self.all_occur = all_occur

    def visit_ASSIGNMENT(self, node, *args):
        if Symbol(node.children[0]).matches(self.symbol):
            if not self.objects:  # remove assignment completely
                return None
            node.children[1].children = list(filter(lambda x: x not in self.objects, node.children[1].children))
            return node

        if self.all_occur:
            if not self.symbol.is_type():
                raise ValueError("Cannot remove all occurrences of an object that is not a type object")
            try:
                for i, typ in enumerate(Symbol(node.children[0]).typing):
                    if Symbol(Node.Type(typ)).matches(self.symbol):
                        if not self.objects:  # remove assignment completely
                            return None
                        node.children[1].children = [x for x in node.children[1].children if
                                                     x.children[i] not in self.objects]
                        break
            except AttributeError:
                pass
        return node


class DeclarationRemover(InterpretationRemover):
    """ Remove a declaration from vocabulary. """

    def visit_DECLARATION(self, node, *args):
        return self.visit_ASSIGNMENT(node, args)


class ConstraintRemover(Remover):
    """ Remove all constraints containing a specific symbol from theory. """

    def __init__(self, symb: Symbol | Node):
        self.symbol = Symbol(symb) if isinstance(symb, Node) else symb

    def visit_CONSTRAINT(self, node, *args):
        if SymbolUsageFinder(self.symbol).visit(node):
            return None
        return node

