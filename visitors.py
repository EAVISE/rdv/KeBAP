# Visitor pattern for Specification
# Authors: Kylian Van Dessel, Joost Vennekens

from __future__ import annotations
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from nodes import Node


class Visitor:
    """ Visitor pattern main visitor for default handling. """

    def visit(self, node, *args):
        ''' Operation node. Return operation depending on node kind. '''
        return node.receive(self, *args)

    # TODO replace by rec2
    def visit_noop_rec(self, node, *args):
        return self.visit_noop_rec2(node, *args)
        # return Node(node.symbol, [self.visit(x, *args) for x in node.children])

    def visit_noop_rec2(self, node, *args):
        """ No operation node with recursion. Visit children before returning node (if children Nodes, not strings) """
        return Node(node.symbol, [self.visit(x, *args) if self.op(x) else x for x in node.children])

    def visit_noop_norec(self, node, *args):
        """ No operation node without recursion. Return node without visiting any children. """
        return Node(node.symbol, node.children)

    @staticmethod
    def op(x):
        """ Assert if a (syntax tree) operation is possible, i.e. x is not None and is not a plain string). """
        if x and not isinstance(x, str):
            return True
        return False


class VocabularyVisitor(Visitor):
    """ Visitor pattern for default handling of vocabulary elements. """

    def visit_VOCABULARY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_VOCABULARYBODY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_DECLARATION(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_FUNC(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node, *args)

    def visit_PRED(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PRED_ENUM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TUPLE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_BUILTIN(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_SUPERSET(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_SUBSET(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_TYPING(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_ARITY(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_NAME(self, node, *args):
        return self.visit_noop_norec(node, *args)


class StructureVisitor(Visitor):
    """ Visitor pattern for default handling of structure elements. """

    def visit_STRUCTURE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_STRUCTUREBODY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ASSIGNMENT(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_FUNC(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node, *args)

    def visit_PRED(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_FUNC_ENUM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PRED_ENUM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TUPLE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ELSE(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_NVALUED(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_BUILTINTYPE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TYPING(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ARITY(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_NAME(self, node, *args):
        return self.visit_noop_norec(node, *args)


class TheoryVisitor(Visitor):
    """ Visitor pattern for default handling of theory elements. """

    def visit_THEORY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_THEORYBODY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_OPTIMISE(self, node, *args):
        return self.visit_THEORY(node, *args)

    def visit_OPTIMISEBODY(self, node, *args):
        return self.visit_THEORYBODY(node, *args)

    def visit_QUERY(self, node, *args):
        return self.visit_THEORY(node, *args)

    def visit_QUERYBODY(self, node, *args):
        return self.visit_THEORYBODY(node, *args)

    def visit_CONSTRAINT(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_LIMP(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_RIMP(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_EQUIV(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_IFFORMULA(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_NOT(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_AND(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_OR(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ATOM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PRED(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TERM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_IFTERM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ABS(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_MIN(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_MAX(self, node, *args):
        return self.visit_MIN(node, *args)

    def visit_FUNC(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node, *args)

    def visit_DEFINITION(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_DEFIMP(self, node, *args):  # !(var1 var2 ...):(head)<-(body).
        return Node(node.symbol, [list(map(self.visit, node.children[0])),
                                  self.visit(node.children[1], *args), self.visit(node.children[2], *args)])

    def visit_CMP(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_MATH(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_MINUS(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_VAR(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_BUILTINTYPE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TYPING(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_ARITY(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_BOOL(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_NAME(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_FORALL(self, node, *args):  # !(var1 var2 ...):(formula)
        return Node(node.symbol, [list(map(self.visit, node.children[0])), self.visit(node.children[1], *args)])

    def visit_EXISTS(self, node, *args):  # ?(var1 var2 ...):(formula)
        new_node = self.visit_FORALL(node, *args)
        assert len(node.children) <= 2, f'\'EXIST\' node should not have more than two child nodes, information will get lost: {node.children[2:]}'
        return new_node

    def visit_SETOBJ(self, node, *args):  # {(var_list): (formula)[: (term)]}
        return Node(node.symbol, [list(map(self.visit, node.children[0])),
                                  *list(map(lambda x: self.visit(x, *args), node.children[1:]))])

    def visit_SET(self, node, *args):
        return self.visit_SETOBJ(node, *args)

    def visit_AGGRSET(self, node, *args):  # (aggr)(set)
        return Node(node.symbol, [node.children[0], self.visit(node.children[1], *args)])

    def visit_AGGRLST(self, node, *args):
        return self.visit_noop_rec2(node, *args)
