# Modeller for logical specifications
# Authors: Kylian Van Dessel, Robin De Vogeleare
from __future__ import annotations

import warnings
import parsers
import translators
from specification import Specification
from blocks import Vocabulary, Structure, Theory, Optimise, Query
from nodes import Node, extract_name, BlockName


class Modeller:
    @staticmethod
    def _Parser(lang: str):
        lang = lang.upper()
        try:
            return getattr(getattr(parsers, f'parser_{lang}'), f'{lang}Parser')
        except AttributeError:
            print('Could not retrieve an appropriate parser, probably the requested input language is unsupported.')
            raise

    @staticmethod
    def _Translator(lang: str):
        lang = lang.upper()
        try:
            return getattr(getattr(translators, f'translator_{lang}'), f'{lang}Translator')
        except AttributeError:
            print('Could not retrieve an appropriate parser, probably the requested input language is unsupported.')
            raise

    #  INIT ------------------------------------------------------------------------------------------------------------
    def __init__(self, spec: Specification):
        """ Create a new Modeller for a Specification. """
        self._specification = spec

    @classmethod
    def from_string(cls, lang: str, repr: str, has_freevars: bool = False, **kwargs):
        """ Create a new Modeller for a(n input language) specification from a string representation. """
        blocks = cls._Parser(lang)().parse_string(repr, **kwargs)
        if has_freevars:
            warnings.warn('Since free variables may occur, the symbols\' signatures '
                          'are not automatically completed. This may limit functionality.')
        spec = Specification(blocks, ignore_freevars=has_freevars)
        return cls(spec)

    @classmethod
    def from_file(cls, lang: str, file: str, has_freevars: bool = False, **kwargs):
        """ Create a new Modeller for a(n input language) specification from a file. """
        blocks = cls._Parser(lang)().parse_file(file, **kwargs)
        if has_freevars:
            warnings.warn('Since free variables may occur, the symbols\' signatures '
                          'are not automatically completed. This may limit functionality.')
        spec = Specification(blocks, ignore_freevars=has_freevars)
        return cls(spec)

    def to_string(self, lang: str):
        """ Translate a Specification or element to an output language representation. """
        repr = self._Translator(lang)().translate(self.specification)
        return repr

    def to_file(self, lang: str, output_file: str):
        """ Translate a Specification or element to an output language representation, write to file. """
        repr = self._Translator(lang)().translate(self.specification)
        with open(output_file, 'w') as f:
            f.write(repr)
        return repr

    #  PROPERTIES ------------------------------------------------------------------------------------------------------
    @property
    def specification(self):
        """The modelled specification."""
        return self._specification

    @specification.setter
    def specification(self, spec):
        self._specification = spec

    @specification.deleter
    def specification(self):
        del self._specification

    @property
    def vocabulary(self):
        """The modelled specification's vocabulary."""
        return self._specification.vocabulary

    @vocabulary.setter
    def vocabulary(self, vocab:Vocabulary):
        self._specification.vocabulary = vocab

    @vocabulary.deleter
    def vocabulary(self):
        # TODO instead of deleting, provide new empty block
        #  self._specification.vocabulary = Vocabulary.new(self.vocabulary.name)
        # TODO or clear existing
        #  self.vocabulary.clear()
        # TODO print warnings or don't allow if still 'in use' by structure or theory
        del self._specification.vocabulary

    @property
    def structure(self):
        """The modelled specification's structure."""
        return self._specification.structure

    @structure.setter
    def structure(self, struc: Structure):
        self._specification.structure = struc

    @structure.deleter
    def structure(self):
        del self._specification.structure

    @property
    def theory(self):
        """The modelled specification's theory."""
        return self._specification.theory

    @theory.setter
    def theory(self, theor: Theory):
        self._specification.theory = theor

    @theory.deleter
    def theory(self):
        del self._specification.theory

    @property
    def optimise(self):
        """Some optimisation term/object value you specified along the way."""
        return self._specification.optimise

    @optimise.setter
    def optimise(self, term: Optimise):
        self._specification.optimise = term

    @optimise.deleter
    def optimise(self):
        del self._specification.optimise

    @property
    def query(self):
        """Some query you specified along the way."""
        return self._specification.query

    @query.setter
    def query(self, query: Query):
        self._specification.query = query

    @query.deleter
    def query(self):
        del self._specification.query

    #  IO  -------------------------------------------------------------------------------------------------------------
    def __str__(self):
        """ Get string representation of Modeller, i.e. (an overview of) the specification and any other attributes """
        s = self.get_overview()
        return s

    def get_overview(self):
        """ Get an overview of the specification in a (printable) string format. """
        s = f'(modeller)\nSpecification({self.theory.name},{self.structure.name},{self.vocabulary.name}))'
        x = f'[{",".join(list(map(lambda x: x.name, filter(None, [self.optimise, self.query]))))}]'
        if x !='[]':
            s += x
        s += f'\n\t{self.vocabulary.get_signature()} {{({len(self.vocabulary.get_declarations())})}}'
        s += (f'\n\t{self.structure.get_signature()} {{({len(self.structure.get_interpretations())}/~'
              f'{len(self.vocabulary.get_declared_symbols()) - len(self.structure.get_interpretations())})}}')
        s += f'\n\t{self.theory.get_signature()} {{({len(self.theory.get_constraints())})}}'
        if self.optimise:
            s += f'\n\t{self.optimise.get_signature()} {{{extract_name(self.optimise.body)}}}'
        if self.query:
            s += f'\n\t{self.query.get_signature()} {{{extract_name(self.query.body)}}}'
        return s

    @staticmethod
    def parse_elem(lang: str, elem: str, kind: str):
        """ Parse a(n input language) specification or element ('kind=') to a syntax tree Node. """
        node = Modeller._Parser(lang)().parse_string(elem, start=kind)
        return node

    @staticmethod
    def translate_elem(lang: str, elem: Node):
        """ Translate a Specification or element to an output language representation,
            optionally write to file ('output_file='). """
        warnings.warn('Element-based translation is not supported (yet), translation might fail.')
        repr = Modeller._Translator(lang)().translate_node(elem)
        #repr = Modeller._Translator(lang)().translate(elem)
        return repr

    # GETTERS / FINDERS  -----------------------------------------------------------------------------------------------

    # SETTERS / ADDERS  ------------------------------------------------------------------------------------------------

    def add_type(self, name: str,
                 enum: list | None = None, enum_in_block: BlockName = BlockName.STRUCTURE):
        if enum is None:
            self.vocabulary.add_type(name)
            return
        if enum_in_block == BlockName.VOCABULARY:
            self.vocabulary.add_type(name, enum)
        elif enum_in_block == BlockName.STRUCTURE:
            self.vocabulary.add_type(name)
            self.structure.add_type(name, enum)
        elif enum_in_block == BlockName.THEORY:
            self.vocabulary.add_type(name)
            #self.theory.add_type(name, enumeration)  # TODO
            warnings.warn('Complete symbol interpretations in Theory block are not supported yet, '
                          'they will be added to structure instead.')
            self.structure.add_type(name, enum)

    def add_function(self, name: str, typing: list,
                     enum: list | dict | None = None, else_value : str|int|float = None, nvalued : str|None = None):
        self.vocabulary.add_function(name, typing)
        if enum is not None:
            self.structure.add_function(name, enum, else_value=else_value, nvalued=nvalued, typing=typing)

    def add_predicate(self, name: str, typing: list, enumeration: list|None = None):
        pred = self.vocabulary.add_predicate(name, typing)
        if enumeration is not None:
            arity = len(typing)
            self.structure.add_predicate(name, arity, enumeration)
        return pred

    def add_constraints(self, lang: str, constraints: str|list):
        """Add one or more constraints from string (separated by a . or as a list) to the theory. """
        if isinstance(constraints, list):
            constraints = "".join(constraints)
        nodes = self.parse_elem(lang, constraints, kind='theory_body')
        self.theory.add_constraints(nodes)

    # UPDATERS  --------------------------------------------------------------------------------------------------------

    # DELETERS  --------------------------------------------------------------------------------------------------------

    def remove_predicate(self, symb: Node | str, from_theory = True):
        ''' remove predicate from specification by Node representation or name (str). '''
        self.vocabulary.remove_predicate(symb)
        self.structure.remove_predicate(symb)
        if from_theory:
            self.theory.remove_constraints_containing_symbol(symb)

    def remove_constraint(self, lang, constraints: str | list):
        """Remove one or more constraints from string (separated by a . or as a list) to the theory. """
        if isinstance(constraints, list):
            constraints = "".join(constraints)
        nodes = self.parse_elem(lang, constraints, kind='theory_body')
        self.theory.remove_constraint(nodes)

        # TODO @RDV I am not sure if I got it right, you give a constraint as a string, convert it to a node and remove
        #  that node from your theory, right? this will of course only work if you parse it with the same parser as the
        #  original language, and only if there has nothing changed to it in the mean time. Maybe if you look at equal
        #  constraints? but then you should write a new theory with both constraints against each other, and have some
        #  smart way of inventing a representative structure to test it against?


if __name__ == '__main__':
    file = './test_scripts/_dummyfiles/idp3_dummy_ext.idp3'
    with open(file,'r') as f: repr = f.read()
    print(repr)

    mdlr = Modeller._Parser('idp3')().parse_string(repr)
    #print(mdlr)
    mdlr = Modeller.from_file('idp3', file)
    #print(mdlr)

    repr = mdlr.to_string('idp3')
    print(repr)

    x = '!x[T]:P(x).'
    sytr = Modeller.parse_elem('idp3', x, 'constraint')
    print(sytr)
    exit(0)
