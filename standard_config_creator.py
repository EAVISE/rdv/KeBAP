"""
Creates a default theory and constants
Typical use case: Often a user has a bunch of features (predicates or functions) that have the same
typing (e.g. all features that apply to person). In such case, the user might want to generate choice constants
for each and a theory linking the choice constants together.
"""

from nodes import Symbol
from specification import Specification
from manipulators.finder_visitors import SameTypingFinder
from runners.runner_IDPZ3 import IDPZ3Runner


def add_default_constants(spec: Specification, typing: tuple, suffix: str = "_"):
    if isinstance(typing, str):
        typing = (typing,)
    finder = SameTypingFinder()
    preds_funcs = finder.visit(spec.vocabulary.tree, *typing)
    goal_consts = []
    for type_ in typing:
        goal_const = f"{type_.lower()}_{suffix}"
        goal_consts.append(goal_const)
        spec.vocabulary.add_function(goal_const, type_)

    goals = ', '.join(f"{const}() " for const in goal_consts)

    for item in preds_funcs:
        if item.symbol == 'PRED':
            name = f"{item.name}_{suffix}"
            spec.vocabulary.add_predicate(name)
            spec.theory.add_constraint(f"\n\t{item.name}({goals}) <=> {name}().")
        elif item.symbol[-4:] == 'FUNC':
            name = f"{item.name}_{suffix}"
            output = item.return_type
            spec.vocabulary.add_function(name, [output])
            spec.theory.add_constraint(f"\n\t{item.name}({goals}) = {name}().")

        else:
            raise Exception("How did you even get here?")
    return spec


if __name__ == '__main__':
    file = "./tetra/laptop_split/processor_defaults.idp"
    spec = Specification.from_file("IDPZ3", file, ignore_freevars=True)
    spec = add_default_constants(spec, "Processor_model")
    runner = IDPZ3Runner(spec)
    # TODO replaced this since method doesn't exist anymore, but string method should do the same
    #  couldn't test it due to a lack of files './tetra/...'
    #  print(runner.translate())
    print(runner)
