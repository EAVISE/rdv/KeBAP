# Nodes and symbols for internal representation
# Kylian Van Dessel, Robin De Vogelaere, Lucas Van Laer, Joost Vennekens
from __future__ import annotations

import warnings
from abc import ABC
from copy import deepcopy
from enum import Enum, auto
from functools import reduce
from typing import Any


class Node:
    """ Node object for building a tree representation of (an extension of) a First Order Logic specification.

    Attributes:
        symbol      node symbol name (from list of SymbolNames)
        children    list of children Nodes
    Example:
        x | (y & z)
        Node(OR, [Node(NAME,['x']), Node[AND, [Node(NAME,['y']), Node(NAME,['z'])]]])
    """

    def __init__(self, symb : str, ch : list[Node|int|str] = None):
        if not symb in NodeName.list():
            raise ValueError(f'Could not initialise Node since \"{symb}\" is not a valid symbol. '
                             f'Accepted symbols are: \n{NodeName.list()}.')
        if ch is None:
            ch = []
        if not isinstance(ch, list):  # TODO change to assertion
            warnings.warn(f'\'ch\' should be a *list* of children nodes, changed {ch} to [{ch}]')
            ch = [ch]
        self.symbol = symb
        self.children = ch

    @classmethod
    def Name(cls, name : str|Node):
        """ Return new name Node, i.e. NAME[<name>] """
        try:
            if name.symbol != 'NAME':
                raise ValueError(f'Expected a NAME node (or a string) but got a \'{name.symbol}\' node instead.')
        except AttributeError:
            pass
        else:
            return name
        return cls('NAME', [name])

    @classmethod
    def Bool(cls, bool_ : bool = True):
        """ Return new boolean Node, i.e. BOOL[<TRUE/FALSE>] """
        x = bool_
        if isinstance(bool_, str):
            x = str(bool_).upper()
        if isinstance(bool_, int):
            x = 'TRUE' if bool_ == 1 else ('FALSE' if bool_ == 0 else 'NONE')
        if x not in ('TRUE', 'FALSE'):
            raise ValueError(f'"{bool_}" is not a boolean value.')
        return cls('BOOL', [x])

    @classmethod
    def Type(cls, name : str|Node, builtin : list[str|Node] = None, superset : list[str|Node] = None, subset : list[str|Node] = None):
        """ Return new type Node, i.e. PRED[NAME[<name>], [ARITY[1]]] """
        name = Node.Name(name)
        ch = [name, Node('ARITY', [1])]
        if builtin:
            #ch.append(Node('BUILTIN', [builtin]))
            ch.append(Node('BUILTIN', [Node.Name(x) for x in builtin]))
        if superset:
            ch.append(Node('SUPERSET', [Node.Name(x) for x in superset]))
        if subset:
            ch.append(Node('SUBSET', [Node.Name(x) for x in subset]))
        return cls('PRED', ch)

    @classmethod
    def Symbol(cls, kind : str,  name : str|Node, arity : int = None, typing : list = None):
        """ Return a new predicate Node. """
        try:
            if name.symbol != 'NAME':
                raise ValueError(f'Expected a \'NAME\' node but got a \'{name.symbol}\' node instead.')
        except AttributeError:
            name = Node.Name(name)
        ch = [name]
        if typing is not None:
            assert isinstance(typing, list)
            ch.append(Node('TYPING', [Node.Name(x) for x in typing]))
            l = len(typing) - (1 if kind in ['FUNC', 'PFUNC'] else 0)
            if arity is None:
                arity = l
            else:
                if arity != l:
                    raise ValueError(f'Arity ({arity}) of symbol \'{extract_name(name)}\' does not match length ({l}) of typing {typing}.')
        if arity is not None:
            ch.append(Node('ARITY', [arity]))
        return cls(kind, ch)

    @classmethod
    def Var(cls, name, type_name):
        """ Return new var Node, i.e. VAR[NAME[<var_name>], [NAME[<var_type>]]] """
        return cls('VAR', [Node.Name(name), Node.Name(type_name)])

    @classmethod
    def Block(cls, kind: BlockName | str, name:str = '', vocname:str = ''):
        """ Return new empty(!) block Node, i.e. <block_kind>[NAME[<block_name>], NAME[<voc_name>], <block_kind>BODY[]] """
        if not isinstance(kind, BlockName):
            try:
                kind = BlockName(kind.upper())
            except ValueError:
                raise ValueError(f'Could not initialise block Node since \"{kind}\" is not a valid block.')
        ch = [Node.Name(name),
              Node.Name(vocname),
              Node(f'{kind.name}BODY', [])]
        return cls(kind.name, ch)

    @property
    def name(self):
        """ Node name """
        return extract_name(self)

    @name.setter
    def name(self, s: str):
        if self.symbol != 'NAME':
            raise AttributeError('Can only set name of a NAME node')
        self.children[0] = str(s)
        return

    def __len__(self):
        return len(self.children)

    def __hash__(self):
        return hash(self.symbol) ^ reduce(lambda x, y: x ^ y, list(map(hash, self.children)))

    def __eq__(self, other):
        if self.symbol != other.symbol or len(self.children) != len(other.children):
            return False
        return all(self.children[i] == other.children[i] for i in range(len(self.children)))

    def __str__(self):
        """ SYMBOL[<children>], e.g. AND[OR[NAME[x], NAME[y]], NAME[z]]"""
        return self.symbol + "[" + ','.join(map(str, self.children)) + "]"

    def print(self, print_depth=2):
        """ Print string representation of Node, i.e. parse tree with a given depth. """
        print(self.to_tree(print_depth))

    def to_tree(self, print_depth=2):
        """ Return string representation of Node, i.e. parse tree with a given depth. """
        def recurse_print(node, depth=0, indent=0):
            t = ''
            try:
                if depth == indent:
                    return '\n' + '\t' * indent + str(node)
                if node.symbol in ['NAME', 'ARITY', 'TYPING']:
                    return '\n' + '\t' * indent + str(node)
                t += '\n' + '\t' * indent + str(node.symbol) + '['
                for x in node.children:
                    if not isinstance(x, list):
                        x = [x]
                    for y in x:
                        t += recurse_print(y, depth=depth, indent=indent + 1)
                t += '\n' + '\t' * indent + ']'
            except AttributeError:
                t += '\n' + '\t' * indent + str(node)
                pass
            return t
        return recurse_print(self, print_depth)

    def receive(self, visitor, *args):
        func = "visit_" + self.symbol
        return getattr(visitor, func)(self, *args)


class Symbol(ABC):
    def __init__(self, node: Node):
        """ Create a Symbol object for a symbol Node for easier access and comparison of the symbol's properties,
            e.g. for FUNC[NAME[F], TYPING[NAME[T],NAME[T2],NAME[T3]], ARITY[2]] == F[T,T2]:T3

        Attributes:
            kind: symbol kind, e.g. 'FUNC'
            name: symbol name, e.g. 'F'
            arity: symbol arity, e.g. '2'
            _typing: symbol typing list, e.g. '[T,T2]'
            return_type: symbol return type (for predicate by definition Bool), e.g. 'T3'
            superset
            subset
            builtin
        """
        if node.symbol not in SymbName.list():
            raise TypeError(f'{str(node)} is not a symbol, valid symbol kinds are: {SymbName.list()}')
        attr = {x.symbol.lower(): list(map(extract_name, x.children)) for x in node.children}
        for k in attr:
            if k.upper() not in SymbAttr.list():
                warnings.warn(f'Got an unexpected attribute \'{k}\' while creating a Symbol (which will probably be ignored).')
        self.symbol = node.symbol
        self.name = attr['name'][0]
        del attr['name']
        if 'arity' in attr:
            self._arity = int(attr['arity'][0])
            del attr['arity']
        if 'typing' in attr:
            self._typing = attr['typing']
            if self.symbol == 'PRED':
                self.return_type = 'Bool'
            else:
                self.return_type = self.typing[-1]
                self._typing = self.typing[:-1]
            if hasattr(self, 'arity'):
                assert self._arity == len(self._typing)
            else:
                self._arity = len(self._typing)
            del attr['typing']
        for k, v in attr.items():
            setattr(self, k, v)

    @classmethod
    def from_node(cls, node):
        """ Create PredicateSymbol from Node. """
        return cls(node)

    @property
    def typing(self):
        return self._typing

    @typing.setter
    def typing(self, typing: list):
        self._typing = typing
        self._arity = len(self.typing)

    @property
    def arity(self):
        return self._arity

    @arity.setter
    def arity(self, arity: int):
        if hasattr(self, 'typing'):
            raise AttributeError('Cannot change symbol arity since the symbol contains a typing, update typing instead.')
        self._arity = arity

    def __str__(self):
        s = ('*' if self.is_partial() else '') + self.name
        if hasattr(self, 'arity'):
            s += '/' + str(self.arity)
        if hasattr(self, 'typing'):
            s += '[' + ','.join([extract_name(x) for x in self.typing]) + ']'
        if hasattr(self, 'arity') and self.is_func():
            s += ':1'
        if hasattr(self, 'typing') and self.is_func():
            s += '[' + extract_name(self.return_type) + ']'
        return s

    def to_node(self) -> Node:
        """ Convert Symbol object to Node object. """
        if self.is_type():
            return Node.Type(self.name, builtin = self.builtin if hasattr(self, 'builtin') else None,
                                        superset = self.superset if hasattr(self, 'superset') else None,
                                        subset = self.subset if hasattr(self, 'subset') else None)
        return Node.Symbol(kind = self.symbol, name = self.name,
                           arity = self.arity if hasattr(self, 'arity') else None,
                           typing = self.typing + ([self.return_type] if self.is_func() else []) if hasattr(self, 'typing') else None)

    def convert_to_pred(self):
        """ Convert function symbol to predicate symbol. """
        if self.is_pred():
            warnings.warn('Symbol is already a predicate, no use in converting it')
            return
        self.symbol = 'PRED'
        if hasattr(self, 'typing'):
            self.typing = self.typing.extend(self.return_type)
            self.return_type = 'Bool'
        elif hasattr(self, 'arity'):
            self.arity += 1

    def convert_to_func(self):
        """ Convert predicate symbol to function symbol. """
        if self.is_func():
            warnings.warn('Symbol is already a function, no use in converting it')
            return
        self.symbol = 'FUNC'
        if hasattr(self, 'typing'):
            self.return_type = self.typing[-1]
            self.typing = self.typing[:-1]
        elif hasattr(self, 'arity'):
            self.arity -= 1

    def __hash__(self):
        x = (self.symbol, self.name)
        try:
            x += tuple(self.typing)
        except AttributeError:
            try:
                x += tuple([self.arity])
            except AttributeError:
                pass
        return hash(x)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.matches(other)

    def is_pred(self) -> bool:
        return self.symbol == 'PRED'

    def is_partial(self) -> bool:
        return self.symbol == 'PFUNC'

    def is_func(self) -> bool:
        return self.symbol == 'FUNC' or self.is_partial()

    def is_type(self) -> bool:
        if not self.is_pred():
            return False
        if not hasattr(self, 'arity'):
            raise AttributeError("Cannot determine if predicate symbol is a type.")
        if self.arity != 1:
            return False
        if hasattr(self, 'typing'):
            return False
        return True

    def is_bool(self) -> bool:
        if not self.is_pred():
            return False
        try:
            return self.arity == 0
        except AttributeError:
            raise Exception("Cannot determine if predicate symbol is a boolean (constant).")

    def copy(self, name: str = None) -> Symbol:
        """ Copy a symbol object to a new one, with optionally a new name."""
        from copy import deepcopy
        symb = deepcopy(self)
        if name:
            symb.name = name
        return symb

    def matches(self, other: Symbol) -> bool:
        """ Check if Symbol matches another one, i.e. Same symbol*, name, arity and typing as given other Symbol.
            *partial and complete functions are considered the same, unknown attributes are ignored.
        """
        if self.symbol != other.symbol:
            return False
        if self.name != other.name:
            return False
        try:
            if self.arity != other.arity:
                return False
            if self.typing != other.typing:
                return False
            if self.is_partial() != other.is_partial():
                warnings.warn(f'May have matched a partial function with a (complete) function for \'{self} == {other}?\'.')
            return True
        except AttributeError:
            return True

    def find_in(self, symb_list: list[Symbol]) -> Symbol:
        """ Find Symbol in a list of Symbols. Raise error if not found or ambiguous. """
        matches = [x for x in symb_list if self.matches(x)]
        if len(matches) < 1:
            raise ValueError(f'{str(self)} not found')
        if len(matches) > 1:
            t = '", "'.join(map(str, matches[:-1]))
            raise ValueError(f'Ambiguity in identity of "{str(self)}", could be "{t}" or "{matches[-1]}"')
        return matches[0]


def extract_name(s: Any) -> str:
    if s is None:
        return
    if isinstance(s, str):
        return s
    if isinstance(s, int):
        return str(s)
    if isinstance(s, float):
        return str(s)
    return extract_name(s.children[0])


class AutoMirrorValues(Enum):
    @staticmethod
    def _generate_next_value_(name, start, count, last_values):
        return name

    @classmethod
    def list(cls):
        return list(map(lambda x: x.value, cls))


class NodeName(AutoMirrorValues):
    ABS = auto()
    AGGRSET = auto()
    AGGRLST = auto()
    AND = auto()
    ARITY = auto()
    ASSIGNMENT = auto()
    ATOM = auto()
    BOOL = auto()
    BUILTIN = auto()
    CMP = auto()
    CONSTRAINT = auto()
    DECLARATION = auto()
    DEFIMP = auto()
    DEFINITION = auto()
    ELSE = auto()
    EQUIV = auto()
    EXISTS = auto()
    FORALL = auto()
    FUNC = auto()
    FUNC_ENUM = auto()
    IFFORMULA = auto()
    IFTERM = auto()
    LIMP = auto()
    MATH = auto()
    MAX = auto()
    MIN = auto()
    MINUS = auto()
    NAME = auto()
    NOT = auto()
    NVALUED = auto()
    OR = auto()
    OPTIMISE = auto()
    OPTIMISEBODY = auto()
    PFUNC = auto()
    PRED = auto()
    PRED_ENUM = auto()
    QUERY = auto()
    QUERYBODY = auto()
    RIMP = auto()
    SET = auto()
    SETOBJ = auto()
    SUBSET = auto()
    SUPERSET = auto()
    STRUCTURE = auto()
    STRUCTUREBODY = auto()
    TERM = auto()
    THEORY = auto()
    THEORYBODY = auto()
    TUPLE = auto()
    TYPING = auto()
    VAR = auto()
    VOCABULARY = auto()
    VOCABULARYBODY = auto()


class SymbName(AutoMirrorValues):
    PRED = auto()
    FUNC = auto()
    PFUNC = auto()


class SymbAttr(AutoMirrorValues):
    NAME = auto()
    TYPING = auto()
    ARITY = auto()
    BUILTIN = auto()
    SUPERSET = auto()
    SUBSET = auto()


class BlockName(AutoMirrorValues):
    VOCABULARY = auto()
    STRUCTURE = auto()
    THEORY = auto()
    QUERY = auto()
    OPTIMISE = auto()
