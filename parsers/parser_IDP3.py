# IDP3 parser
# Authors: Kylian Van Dessel, Joost Vennekens

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from parsers.parser import Parser
from parsers.idp3_lex import IDP3Lex
from parsers.idp3_yacc import IDP3Yacc, IDP3Yacc_start, YaccError


class IDP3Parser(Parser):
    def __init__(self):
        pass

    def parse_string(self, s: str, start=None, **kwargs):
        """ Parse IDP3 specification from string. """
        if start:
            start = 'constraint' if start == 'definition' else start
            return IDP3Yacc_start(start=start).parse(s, lexer=IDP3Lex)
        return IDP3Yacc.parse(s, lexer=IDP3Lex)

    def parse_file(self, file: str, **kwargs):
        """ Parse IDP3 specification from file. """
        with open(file, 'r') as f:
            s = f.read()
        return self.parse_string(s, **kwargs)

    def parse_empty(self):
        """ Parse empty IDP3 specification. """
        idp_empty = "Vocabulary V {} Theory T:V {} Structure S:V {}"
        return self.parse_string(idp_empty)


if __name__ == '__main__':
    try:
        idp_file = sys.argv[1]
    except IndexError:
        print('warning: no IDP3 file provided, using test file instead.')
        idp_file = './test_scripts/_dummyfiles/idp3_dummy_ext.idp3'
    print(f'running IDP3 parser on \'{idp_file}\'')
    blocks = IDP3Parser().parse_file(idp_file)
    print(blocks)
    print('\n'.join(map(str,blocks)))
    exit(0)
