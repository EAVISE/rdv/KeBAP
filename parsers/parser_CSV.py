# CSV parser
# authors: Robin De Vogelaere
from __future__ import annotations

import datetime
import json
import os
import pandas as pd
import warnings
from numpy import isnan
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from blocks import Vocabulary, Structure
from nodes import Node
from parsers.parser import Parser


def to_boolean(item):
    try:
        if isnan(item):
            return item
    except:
        pass
    if isinstance(item, str):
        item = item.lower()
    if (item == 'true' or item == "yes" or item == 'y' or str(item) == '1'
            or str(item) == '1.0' or item == 1 or item is True):
        return True
    else:
        return False


def to_date(x, delimiter='/'):
    """Returns dates in FO(.) standard representation. Note that support for dates is minimal."""
    if x is None:
        return None
    dd, mm, yyyy = x.split(delimiter)
    # date = datetime.date(int(yyyy),int(mm),int(dd))
    date =f"#{yyyy}-{mm}-{dd}"
    return date


def clean(item: str) -> str:
    """Replaces unsupported characters from strings with an 'x'.
    IDP strings can only contain numbers, letters and underscores"""
    if not isinstance(item, str):
        return item
    item = item.replace(' ', '_')
    for char in item:
        if not char.isalnum() and char != '_':
            item = item.replace(char, '_')
        if item[0].isdigit():
            item = f"_{item}"
    return item


class CSVParser(Parser):
    def __init__(self,file=None, config_dict=None, key=None):
        self.csv = None
        self.key = key
        self.config_dict = config_dict
        self.vocabulary = Vocabulary(Node.Block("vocabulary", 'V'))
        self.structure = Structure(Node.Block("structure", 'S', 'V'))
        if file:
            self.read_csv(file, self.config_dict, self.key)

    def read_csv(self, file, config_dict=None, key=None):
        """To exclude columns set typing to !Exclude in config"""
        self.csv = pd.read_csv(file)
        self.key = key if key else self.key
        self.config_dict = config_dict if config_dict else self.config_dict
        # header: {"typing": type, "default": default value, "values" [values]} or None
        self.config_dict = self._update_config_types()  # insert missing types in config dict
        self._update_dict_values()  # extract all values from columns and add to config dict
        self._add_types()  # add types to vocabulary & structure from config dict
        self._add_predicates_and_functions()  # add functions and predicates to vocabulary & structure

    @classmethod
    def from_config(cls, file, config, key=None):
        """creates a CsvImporter using both a csv file and a config file"""
        assert os.path.isfile(file), f'No csv file found at "{file}".'
        assert os.path.isfile(config), f'No config file found at "\'{config}\'".'
        config_dict = cls._load_config(config)
        return cls(file, config_dict, key)

    # make callable
    def __call__(self, *args, **kwargs):
        return self.vocabulary, self.structure

    # save configuration
    def save_config(self, file_path="./config.json"):
        """Creates a json file for the configuration info (typing, default, values)"""
        class SetEncoder(json.JSONEncoder):
            def default(self, obj):
                if isinstance(obj, set):
                    return list(obj)
                if isinstance(obj, datetime.date):
                    return str(obj)
                return json.JSONEncoder.default(self, obj)

        with open(file_path, 'w') as f:
            json.dump(self.config_dict, f, indent=4, cls=SetEncoder)

    @staticmethod
    def _load_config(file_path="./config.json"):
        """loads the config file to the config_dict"""
        with open(file_path) as f:
            return json.load(f)

    # Internal methods to:
    # - first add missing types to config_dict
    # - then add values occuring in the columns to config_dict
    # - creating types from config_dict to add to vocabulary & enumeration to structure
    # - create functions and predicates from columns
    def _update_config_types(self):
        """Method used to automatically create a config_dict for a csv or update an existing config_dict
        with automatic type info"""
        config_dict = self.config_dict if self.config_dict else {}

        for column in self.csv.columns:
            # set type: either user defined type, or extracted from pandas dataframe
            try:
                config_dict[column]["typing"]
            except KeyError:
                type_ = self.csv[column].dtype
                # Types in Python format must be converted to the appropriate string for IDP.
                if type_ == bool:
                    type_ = "Bool"
                elif type_ == float:  # NaN might cause conversion to float, so we check if this should be an int
                    if all(map(float.is_integer, self.csv[column].dropna())):
                        type_ = "Int"
                    else:
                        type_ = "Real"
                elif type_ == int:
                    type_ = "Int"
                elif all([isinstance(x, bool) for x in self.csv[column].dropna()]):
                    # If a column contains bool & nan, dtype is object, so we need to test if all occurring values
                    # are booleans
                    type_ = "Bool"
                else:
                    type_ = column.capitalize()
                try:
                    # we don't want to overwrite an existing dict, so we try adding to an existing dict first
                    config_dict[column]["typing"] = type_
                except KeyError:
                    # if the key (column) doesn't exist, we create a new nested dictionary
                    config_dict[column] = {}
                    config_dict[column]["typing"] = type_
        return config_dict

    def _update_dict_values(self):
        """Transform values to correct type and add values to existing config_dict. """
        for column in self.csv.columns:
            type_ = self.config_dict[column]["typing"]
            if type_ == "Int":
                values = set(int(x) for x in self.csv[column].dropna()) # ugly, but json can't handle numpy datatypes
            elif type_ == "Real":
                values = set(float(x) for x in self.csv[column].dropna())
            elif type_ == "Bool":
                self.csv[column] = self.csv[column].apply(to_boolean)  # supports multiple ways of expressing booleans
                values = [True, False]
            elif type_ == "Date":
                self.csv[column] = self.csv[column].apply(to_date)
                values = set(self.csv[column])
            elif type_ == "!Exclude":
                values = None
            else:  # custom type. Its values may consist of strings or be a subset of Int or Real
                try:
                    supertype = self.config_dict[column]["sub"]
                    if supertype == "Int":
                        values = set(int(x) for x in self.csv[column].dropna())
                except:
                    self.csv[column] = self.csv[column].apply(clean)
                    values = set(self.csv[column].dropna())
            # add values to config file, if no user-defined values have been added
            try:
                if not self.config_dict[column]["values"]:  # values exists, but is empty
                    self.config_dict[column]["values"] = values
            except KeyError:  # values does not exist
                self.config_dict[column]["values"] = values

    def _add_types(self):
        """adds types to both vocabulary and structure"""
        for v in self.config_dict.values():
            if v["typing"] not in ["Int", "Real", "Bool", "Date", "!Exclude"]:
                self.vocabulary.add_type(str(v["typing"]).capitalize())
                self.structure.add_type(str(v["typing"]).capitalize(), v["values"])

    def _add_predicates_and_functions(self):
        """adds predicates and functions to both vocabulary and structure"""
        # If no key rows are provided an index type is added as key column"""
        if self.key is None:
            self.vocabulary.add_type("Index__")
            self.structure.add_type("Index__", list(self.csv.index))
            self.key = ["Index__"]
            self.config_dict["Index__"]={"typing": "Index__", "values": list(self.csv.index)}
            self.csv["Index__"] = self.csv.index
        if isinstance(self.key, str):
                self.key = [self.key]
        # Add predicate (key -> Bool) or functions (key -> column type)
        for column in self.csv.columns:
            if column in self.key:
                continue
            name = column.lower()
            type_ = self.config_dict[column]["typing"]
            if type_ == "!Exclude":
                continue
            incomplete = self.csv[column].isnull().values.any()
            has_default = "default" in self.config_dict[column].keys()
            enumerations_df = self.csv[[*self.key, column]].dropna()
            enumerations = [list(row[1:]) for row in enumerations_df.itertuples()]  # [1:]: remove index column
            #enumerations = [tuple(row[1:]) for row in enumerations_df.itertuples()]  # [1:]: remove index column

            if type_ == "Bool":  # predicate
                self.vocabulary.add_predicate(name, typing=self.key)
                if incomplete and has_default:
                    # if default is true, add values to enumeration, if false, no action is needed
                    if self.config_dict[column]["default"]:
                        enumerations_df = self.csv[[*self.key, column]].fillna(True)
                        enumerations = [list(row[1:]) for row in enumerations_df.itertuples()]
                # in any case, add predicate to structure
                self.structure.add_predicate(name, [x[:-1] for x in enumerations if x[-1]], typing=[*self.key])
                # TODO add predicate where defined if incomplete
            else:  # function
                self.vocabulary.add_function(name, typing=[*self.key, type_])
                else_value = None
                if incomplete:  # partial enumeration
                    try: # default provided = complete function
                        else_value = self.config_dict[column]["default"]
                        # TODO add else_value to type enumeration if not already in there (solve in _update_dict_values)
                    except KeyError:  # no default = partial function (assumption)
                        warnings.warn("Incomplete enumerations or partial functions are not yet supported.")
                self.structure.add_function(name, enumerations, typing=[*self.key, type_], else_value=else_value)
                # TODO this version of the CSV parser requires the superset operator to deal with partial enumerations
                #  or (domain: ...) to implement as partial function.
                #  for older versions of IDP(Z3 or 3?) or for max compatibility, default values are required
            if incomplete and not has_default:
            # if incomplete without default, add predicate to indicate where the original predicate / function is defined
                self.vocabulary.add_predicate(f"{name}_def", typing=self.key)
                def_enum = [tuple(row[1:-1]) for row in enumerations_df.itertuples()]
                self.structure.add_predicate(f"{name}_def", def_enum, typing=self.key)

    # Methods inherited from Parser class
    def parse_file(self, file: str, config_file: str | None = None, save_config: str | None = None, **kwargs):
        """ Parse CSV representation from file and return vocabulary and structure directly.
        Use CsvParser.from_config() if you want to refer to the parser itself."""
        # create config_dict
        if config_file:
            self.config_dict = self._load_config(config_file)
       # read and parse file
        self.read_csv(file)
        # save config dict, if requested
        if save_config:
            if os.path.isfile(save_config):
                warnings.warn(f'hope you are sure, *really* sure, not that you can do anything about it now... '
                      f'Old config file will be overwritten ("{save_config}")')
            self.save_config(save_config)
        return self()

    def parse_string(self, repr, **kwargs):
        raise NotImplementedError("This parser expects at least a CSV FILE, use parse file instead.")


if __name__ == '__main__':
    file = "./test_bench/csv/test.csv"
    config_file = "./test_bench/csv/test.json"
    #file = "../tetra/laptop_split/laptop_60.csv"
    #config_file = "../tetra/laptop_split/new_config.json"

    #parser = CSVParser(file)
    parser = CSVParser.from_config(file, config_file)
    # parser.save_config("../tetra/laptop_split/new_config_output.json")

    # print(parser.config_dict)
    parser.vocabulary.print(3)
    parser.structure.print(3)