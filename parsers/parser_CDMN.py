# cDMN parser
# Author: Simon Vandevelde

from __future__ import annotations

import os
import sys
from cdmn.glossary import Glossary
from cdmn.interpret import VariableInterpreter
from cdmn.cdmn import generate_fodot
from cdmn.idply import Parser as IdplyParser
from cdmn.table_operations import create_struct, create_theory, create_voc, fill_in_merged, identify_tables, find_glossary
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from parsers.parser import Parser
from parsers.parser_IDPZ3 import IDPZ3Parser


class CDMNParser(Parser):
    def __init__(self):
        pass

    def parse_file(self, file_path: str, sheets=None):
        """ Parse one or more sheets from an xlsx containing a cDMN model.

        Args:
            file_path (str): The path to the .xlsx file.
            sheets (list[str]): A list of sheetnames. If left empty, all sheets
            will be converted.
        """
        # Check file correctness.
        assert file_path.endswith('.xlsx'), "Only .xlsx files are supported"
        assert os.path.isfile(file_path), "Incorrect file path."

        # Convert the sheet(s) of the xlsx into nice tables.
        if sheets is None:
            sheets = []
        sheets = fill_in_merged(file_path, sheets)
        tables = identify_tables(sheets)

        # Parse the tables, and return the equivalent blocks.
        g = Glossary(find_glossary(tables))
        i = VariableInterpreter(g)
        parser = IdplyParser(i)
        voc = create_voc(g)
        struct = create_struct(tables, parser, g)
        theory = create_theory(tables, parser)
        return IDPZ3Parser().parse_string(voc+struct+theory)

    def parse_string(self, **kwargs):
        raise NotImplementedError('This is a file importer, use \'parse_file()\' instead')


if __name__ == '__main__':
    try:
        xlsx_file = sys.argv[1]
        xlsx_sheets = sys.argv[2:]
    except (IndexError, ValueError):
        print('warning: no XLSX file and/or sheet names provided, using test file instead.')
        xlsx_file, xlsx_sheets = './test_scripts/_dummyfiles/cdmn_dummy.xlsx', ["set_better",  "glossary"]
    print(f'running cDMN parser on \'{xlsx_file}\' (sheets: {", ".join(xlsx_sheets)})')
    CDMNParser().parse_file(xlsx_file, xlsx_sheets)
    exit(0)
