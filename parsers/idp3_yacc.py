# IDP3 syntax grammar (YACC)
# Authors: Kylian Van Dessel, Joost Vennekens

import warnings
import ply.yacc as yacc
from parsers.idp3_lex import reserved, symbol_names, tokens
from blocks import Vocabulary, Structure, Theory, Optimise, Query
from nodes import Node, extract_name


class YaccError(Exception):
    pass


S_DFLT = 'SDFLT'


precedence = (
    ('left', 'COMMA'),
    ('nonassoc', 'FORALL', 'EXISTS'),
    ('nonassoc', 'DEFIMPL'),
    ('nonassoc', 'EQUIV'),
    ('nonassoc', 'RIMP', 'LIMP'),
    ('left', 'OR'),
    ('left', 'AND'),
    ('right', 'NOT'),
    ('nonassoc', 'LT', 'GT', 'LEQ', 'GEQ'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE', 'MODULO'),
    ('right', 'UMINUS'),
)


# idp block

def p_idp_block_list_many(p):
    'idp_block_list : idp_block_list idp_block'
    p[0] = p[1] + p[2]


def p_idp_block_list_one(p):
    'idp_block_list : idp_block'
    p[0] = p[1]


def p_idp_block(p):
    '''idp_block : vocab_block
                 | struc_block
                 | theory_block
                 | term_block
                 | query_block'''
    p[0] = [p[1]]


# VOCABULARY -----------------------------------------------------------------------------------------------------------

# block

def p_vocab_block(p):
    'vocab_block : VOCAB name vocab_body'
    p[0] = Node('VOCABULARY', [p[2], Node.Name(''), p[3]])
    p[0] = Vocabulary(p[0])


def p_vocab_body(p):
    'vocab_body : LBRACE decl_list RBRACE'
    p[0] = Node('VOCABULARYBODY', p[2])


def p_vocab_body_empty(p):
    'vocab_body : LBRACE RBRACE'
    p[0] = Node('VOCABULARYBODY', [])


# declarations

def p_decl_list_many(p):
    'decl_list : decl_list declaration'
    p[0] = p[1] + [p[2]]


def p_decl_list_one(p):
    'decl_list : declaration'
    p[0] = [p[1]]


def p_declaration(p):
    '''declaration : type_decl
                   | pred_decl
                   | func_decl '''
    if not isinstance(p[1], list):
        p[1] = [p[1]]
    p[0] = Node('DECLARATION', p[1])


# Type declaration

def p_type_decl(p):  # 'type T'
    'type_decl : TYPE name'
    p[0] = [Node.Type(p[2])]


def p_type_decl_numerical(p):  # 'type T = { 1; 2; ... }'
    'type_decl : TYPE name EQ pred_enum'
    p[0] = [Node.Type(p[2]), p[4]]


def p_type_decl_construct(p):  # 'type T constructed from { 'a', F(T,T), C }'
    'type_decl : TYPE name CONSTR FROM LBRACE type_enum RBRACE'
    p[0] = [Node.Type(p[2]), p[6]]


def p_type_enum(p):  # IDP3 constr from uses ',' instead of ';' as with regular pred enum
    'type_enum : element_list'
    p[0] = Node('PRED_ENUM', p[1])


def p_type_decl_sub(p):  # 'type T isa T1,T2,...'
    'type_decl : type_decl SUBSET element_list'
    p[1][0].children.append(Node('SUBSET', p[3]))
    p[0] = p[1]


def p_type_decl_super(p):  # 'type T contains T1,...'
    'type_decl : type_decl SUPERSET element_list'
    p[1][0].children.append(Node('SUPERSET', p[3]))
    p[0] = p[1]


def p_type_decl_builtin(p):  # 'type T isa int / 'type T isa nat'
    'type_decl : type_decl SUBSET built_in'
    p[1][0].children.append(Node("BUILTIN", [p[3]]))
    p[0] = p[1]


def p_builtin(p):
    '''built_in : INT
                | NAT
                | STR'''
    p[0] = Node.Name(p[1])  # TODO map p[3] to some keywords (independent from idp3 naming of built-ins)


# Predicate declaration

def p_pred_decl(p):  # 'P(T,T,...)'
    'pred_decl : name LPAR element_list RPAR'
    p[0] = Node.Symbol('PRED', name=p[1], typing=p[3])


def p_pred_decl_bool(p):  # 'B' / 'B()'
    '''pred_decl : name
                 | name LPAR RPAR'''
    p[0] = Node.Symbol('PRED', name=p[1], typing=[])


# Function declaration

def p_func_decl(p):  # 'F(T,T,...):T'
    'func_decl : name LPAR element_list RPAR COLON name'
    p[0] = Node.Symbol('FUNC', name=p[1], typing=p[3]+[p[6]])


def p_func_decl_part(p):  # 'partial F(T,T,...):T'
    'func_decl : PARTIAL func_decl'
    p[2].symbol = 'PFUNC'
    p[0] = p[2]


def p_func_decl_const(p):  # 'C:T'
    'func_decl : name COLON name'
    p[0] = Node.Symbol('FUNC', name=p[1], typing=[p[3]])


def p_func_decl_const_par(p):  # 'C():T'
    'func_decl : name LPAR RPAR COLON name'
    p[0] = Node.Symbol('FUNC', name=p[1], typing=[p[5]])


# STRUCTURE ------------------------------------------------------------------------------------------------------------

# block

def p_struc_block(p):  # structure S:V { ... }
    'struc_block : STRUC name COLON name struc_body'
    p[0] = Node('STRUCTURE', [p[2], p[4], p[5]])
    p[0] = Structure(p[0])


def p_struc_block_noname(p):  # structure :V { ... }
    'struc_block : STRUC COLON name struc_body'
    p[0] = Node('STRUCTURE', [Node.Name(S_DFLT), p[3], p[4]])
    p[0] = Structure(p[0])


def p_struc_body(p):
    'struc_body : LBRACE interpr_list RBRACE'
    p[0] = Node('STRUCTUREBODY', p[2])


def p_struc_body_empty(p):
    'struc_body : LBRACE RBRACE'
    p[0] = Node('STRUCTUREBODY', [])


# interpretations

def p_struc_body_many(p):
    'interpr_list : interpr_list interpretation'
    p[0] = p[1] + [p[2]]


def p_struc_body_one(p):
    'interpr_list : interpretation'
    p[0] = [p[1]]


def p_interpretation(p):
    '''interpretation : pred_assign
                      | func_assign'''
    p[0] = Node('ASSIGNMENT', p[1])


# Predicate assignment

def p_pred_assign(p):  # 'P = ...'
    'pred_assign : name EQ pred_eq'
    p[0] = [Node.Symbol('PRED', p[1]), p[3]]#, Node('NVALUED', [None])]


def p_pred_assign_args(p):  # 'P[T,T,...] = ...'
    'pred_assign : name pred_typing EQ pred_eq'
    p[0] = [Node.Symbol('PRED', p[1], typing=p[2]), p[4]]#, Node('NVALUED', [None])]


def p_pred_assign_three_valued(p):  # 'P<cf> = ...'
    'pred_assign : name three_valued EQ pred_eq'
    p[0] = [Node.Symbol('PRED', p[1]), p[4], Node('NVALUED', [p[2]])]


def p_pred_assign_three_valued_args(p):  # 'P[T,T,...]<cf> = ...'
    'pred_assign : name pred_typing three_valued EQ pred_eq'
    p[0] = [Node.Symbol('PRED', p[1], typing=p[2]), p[5], Node('NVALUED', [p[3]])]


def p_pred_typing(p):  # '[T,T,T]'
    'pred_typing : LSQBRACK element_list RSQBRACK'
    p[0] = p[2]


def p_pred_typing_bool(p):  # '[]'
    'pred_typing : LSQBRACK RSQBRACK'
    p[0] = []


def p_three_valued_true(p):  # '<ct>'
    'three_valued : LT CERTTRUE GT'
    p[0] = True


def p_three_valued_false(p):  # '<cf>'
    'three_valued : LT CERTFALSE GT'
    p[0] = False


# Predicate interpretation

def p_pred_eq(p):
    '''pred_eq : pred_enum
               | pred_bool'''
    p[0] = p[1]


def p_pred_enum(p):  # '{ 1,a; (2,b) }'
    '''pred_enum : LBRACE tuple_list RBRACE
                 | LBRACE tuple_list SEMICOLON RBRACE '''
    p[0] = Node('PRED_ENUM', p[2])


def p_pred_bool_false(p):  # 'false' / '{}'
    '''pred_bool : FALSE
                 | LBRACE RBRACE'''
    p[0] = Node('PRED_ENUM', [])


def p_pred_bool_true(p):  # 'true' / '{()}'
    '''pred_bool : TRUE
                 | LBRACE LPAR RPAR RBRACE'''
    p[0] = Node('PRED_ENUM', [Node('TUPLE', [])])


# Function assignment

def p_func_assign(p):  # 'F = ...'
    'func_assign : name EQ func_eq'
    p[0] = [Node.Symbol('FUNC', name=p[1]), p[3]]


def p_func_assign_args(p):  # 'F[T,T,...:T] = ...'
    'func_assign : name func_typing EQ func_eq'
    p[0] = [Node.Symbol('FUNC', name=p[1], typing=p[2]), p[4]]


def p_func_assign_three_valued(p):  # 'F<cf> = ...'
    'func_assign : name three_valued EQ func_eq'
    p[0] = [Node.Symbol('FUNC', name=p[1]), p[4], Node('NVALUED', [p[2]])]


def p_func_assign_three_valued_args(p):  # 'F[T,T,...:T]<cf> = ...'
    'func_assign : name func_typing three_valued EQ func_eq'
    p[0] = [Node.Symbol('FUNC', name=p[1], typing=p[2]), p[5], Node('NVALUED', [p[3]])]


def p_func_typing_func(p):  # '[T,T:T]'
    'func_typing : LSQBRACK element_list COLON name RSQBRACK'
    p[0] = p[2] + [p[4]]


def p_func_typing_const(p):  # '[:T]'
    'func_typing : LSQBRACK COLON name RSQBRACK'
    p[0] = [p[3]]


# Function interpretation

def p_func_eq(p):
    '''func_eq : func_enum
               | func_const'''
    p[0] = p[1]


def p_func_enum(p):  # '{ 1,a->b; (2,b)->c }'
    '''func_enum : LBRACE mapping_list RBRACE
                 | LBRACE mapping_list SEMICOLON RBRACE'''
    p[0] = Node('FUNC_ENUM', p[2])


def p_mapping_list_many(p):
    'mapping_list : mapping_list SEMICOLON mapping'
    p[0] = p[1] + [p[3]]


def p_mapping_list_one(p):
    'mapping_list : mapping'
    p[0] = [p[1]]


def p_mapping(p):
    'mapping : tuple MAP name'
    p[1].children.append(p[3])
    p[0] = p[1]


def p_func_const_long(p):  # '{ -> a }'
    'func_const : LBRACE MAP name RBRACE'
    p[0] = Node('FUNC_ENUM', [Node('TUPLE', [p[3]])])


def p_func_const(p):  # 'a' /  ''a'' / '"a"'
    'func_const : name'
    p[0] = Node('FUNC_ENUM', [Node('TUPLE', [p[1]])])


# THEORY ---------------------------------------------------------------------------------------------------------------

# block

def p_theory_block(p):
    'theory_block : THEOR name COLON name theory_body'
    p[0] = Node('THEORY', [p[2], p[4], p[5]])
    p[0] = Theory(p[0])


def p_theory_body(p):
    'theory_body : LBRACE constr_list RBRACE'
    p[0] = Node('THEORYBODY', p[2])


def p_theory_body_empty(p):
    'theory_body : LBRACE RBRACE'
    p[0] = Node('THEORYBODY', [])


# constraints

def p_constr_list_many(p):
    'constr_list : constr_list constraint'
    p[0] = p[1] + [p[2]]


def p_constr_list_one(p):
    'constr_list : constraint'
    p[0] = [p[1]]


def p_constraint(p):  # 'f.'
    'constraint : formula DOT'
    p[0] = Node('CONSTRAINT', [p[1]])


def p_constraint_definition(p):  # '{ <definition> }'
    'constraint : definition'
    p[0] = p[1]


# Formula
#  (formula between parentheses, negation of formula, binary op. of formulas, quantification of formula, atom)

def p_formula_par(p):  # '(f)'
    'formula : LPAR formula RPAR'
    p[0] = p[2]


def p_formula_neg(p):  # '~f'
    'formula : NOT formula'
    p[0] = Node('NOT', [p[2]])


def p_formula_binop(p):  # 'f & g' / 'f | g' / 'f=>g' / ...
    '''formula : formula AND formula
               | formula OR formula
               | formula LIMP formula
               | formula RIMP formula
               | formula EQUIV formula'''
    p[2] = symbol_names[p[2]]
    if p[1].symbol == p[2] and p[2] in ('AND', 'OR'):
        p[1].children.append(p[3])
        p[0] = p[1]
    else:
        p[0] = Node(p[2], [p[1], p[3]])


def p_formula_quantified(p):  # '!x[T] y[T] ... : f' / '?x[T] y[T] ... : f'
    '''formula : FORALL var_list COLON formula %prec FORALL
               | EXISTS var_list COLON formula %prec EXISTS'''
    p[0] = Node(symbol_names[p[1]], [p[2], p[4]])


def p_var_list_many(p):
    'var_list : var_list var_decl'
    p[0] = p[1] + [p[2]]


def p_var_list_one(p):
    'var_list : var_decl'
    p[0] = [p[1]]


def p_var_decl(p):
    'var_decl : name LSQBRACK name RSQBRACK'
    p[0] = Node('VAR', [p[1], p[3]])


def p_formula_atom(p):
    'formula : atom'
    p[0] = p[1]


# Atom
#  (true/false, boolean predicate symbol, predicate symbol with terms, comparison of terms)

def p_atom_bool(p):  # 'true' / 'false'
    '''atom : bool'''
    p[0] = Node('ATOM', [p[1]])


def p_atom_pred_bool(p):  # 'B'
    'atom : name'
    p[0] = Node('ATOM', [Node.Symbol('PRED', p[1], arity=0)])


def p_atom_pred(p):  # 'P(T, ...)'
    'atom : name LPAR term_list RPAR'
    p[0] = Node('ATOM', [Node.Symbol('PRED', p[1], arity=len(p[3])), *p[3]])


def p_atom_cmp(p):  # 't1 = t2' / 't1 =< t2' / ...
    'atom : term comparison term'
    p[0] = Node('ATOM', [Node('CMP', [p[2], p[1], p[3]])])


def p_atom_cmp_quan(p):  # '?=1 x[T] y[T] ...: f' -->  '#{x[T] y[T] ... : f} = 1'
    'atom : EXISTS comparison term var_list COLON formula'
    p[0] = Node(symbol_names[p[1]], [p[4], p[6], (p[2], p[3])])
    # p[0] = Node('ATOM', [Node('CMP', [p[2], Node('TERM', [Node('AGGR', ['CNT', p[4], p[6]])]), p[3]])])


def p_atom_cmp_quan_equal(p):  # '?1 x[T]: f'
    'atom : EXISTS term var_list COLON formula'
    p[0] = Node(symbol_names[p[1]], [p[3], p[5], (symbol_names['='], p[2])])
    # p[0] = Node('ATOM', [Node('CMP', [symbol_names['='], p[2], Node('TERM', [Node('AGGR', ['CNT', p[3], p[5]])])])])


def p_comparison(p):
    '''comparison : EQ
                  | LEQ
                  | NEQ
                  | GEQ
                  | LT
                  | GT '''
    p[0] = symbol_names[p[1]]


# Term
#  (variable/constant, function symbol with terms, mathematical operation, aggregate function)

def p_term_var(p):  # 'C' / '"a"' / '5'
    'term : name'
    p[0] = Node('TERM', [p[1]])

def p_term_const(p):  # 'C()'
    'term : name LPAR RPAR'
    p[0] = Node('TERM', [Node.Symbol('FUNC', p[1], arity=0)])


def p_term_func(p):  # 'F(T, ...)'
    'term : name LPAR term_list RPAR'
    p[0] = Node('TERM', [Node.Symbol('FUNC', p[1], arity=len(p[3])), *p[3]])


def p_term_par(p):  # '(t)'
    'term : LPAR term RPAR'
    # p[0] = Node('TERM', [Node('PAR', [p[2]])])
    p[0] = p[2]


def p_term_math(p):
    '''term : math
            | abs
            | aggr
            | extremum'''

    p[0] = Node('TERM', [p[1]])


# - Mathematical operations

def p_math_operation(p):  # 't1 + t2' / 't1 - t2' / ...
    '''math : term PLUS term
            | term MINUS term
            | term TIMES term
            | term DIVIDE term
            | term MODULO term'''
    p[2] = symbol_names[p[2]]
    p[0] = Node('MATH', [p[1], p[2], p[3]])


def p_math_operation_prefix(p):  # '+(t1,t2)' / '-(t1,t2)' / ...
    '''math : PLUS LPAR term COMMA term RPAR
            | MINUS LPAR term COMMA term RPAR
            | TIMES LPAR term COMMA term RPAR
            | DIVIDE LPAR term COMMA term RPAR
            | MODULO LPAR term COMMA term RPAR'''
    p[1] = symbol_names[p[1]]
    p[0] = Node('MATH', [p[3], p[1], p[5]])


def p_math_uminus(p):  # '-t'
    'math : MINUS term %prec UMINUS'
    p[0] = Node('MINUS', [p[2]])


def p_abs(p):  # 'abs(t)'
    'abs : ABS LPAR term RPAR'
    p[0] = Node('ABS', [p[3]])


# - Aggregates

def p_set_obj(p):  # '{x[T] ... : f : g}'  # set maps to an object function 'g'
    'set_obj : LBRACE var_list COLON formula COLON term RBRACE'
    p[0] = Node('SETOBJ', [p[2], p[4], p[6]])

def p_aggr_set(p):  # 'sum<set>'                    - sum, max, min, prod
    'aggr : aggr_func set_obj'
    p[0] = Node('AGGRSET', [p[1], p[2]])


def p_aggr_list(p):  # 'Sum(t1, t2, ...)'           - Sum, Max, Min, Prod
    'aggr : aggr_func LPAR term_list RPAR'
    p[0] = Node('AGGRLST', [p[1]] + p[3])


def p_aggr_func(p):  # TODO case sensitivity has been removed here, consider reinstating, i.e. separate Sum/sum
    '''aggr_func : SUM
                 | PROD
                 | MIN
                 | MAX'''
    p[0] = reserved[p[1]]


def p_set(p):  # '{x[T] ... : f}'  # regular set
    '''set : LBRACE var_list COLON formula RBRACE'''
    p[0] = Node('SET', [p[2], p[4]])


def p_aggr_card(p):  # '#<set>'  # cardinality aggregate
    '''aggr : COUNT set
            | CARD set'''
    p[0] = Node('AGGRSET', ['CNT', p[2]])


def p_extremum_min(p):  # 'MIN[:T]'
    'extremum : TYPEMIN LSQBRACK COLON name RSQBRACK'
    #p[0] = parse_element('term', f'min{{x[{extract_name(p[4])}] : true : x}}').children[0]
    p[0] = Node('MIN', [p[4]])

def p_extremum_max(p):  # 'MAX[:T]'
    'extremum : TYPEMAX LSQBRACK COLON name RSQBRACK'
    p[0] = Node('MAX', [p[4]])


def p_term_list_many(p):
    'term_list : term_list COMMA term'
    p[0] = p[1] + [p[3]]


def p_term_list_one(p):
    'term_list : term'
    p[0] = [p[1]]


# Definitions

def p_definition(p):
    'definition : LBRACE definition_body RBRACE'
    p[0] = Node('DEFINITION', p[2])


def p_def_body_many(p):
    'definition_body : definition_body def_rule'
    p[0] = p[1] + [p[2]]


def p_def_body_one(p):
    'definition_body : def_rule'
    p[0] = [p[1]]


def p_def_rule(p):
    'def_rule : def_constr'
    p[0] = Node('DEFIMP', p[1])


def p_def_quan(p):  # '!x[T]: !y[T]: ...'
    'def_constr : FORALL var_list COLON def_constr %prec FORALL'
    p[4][0].extend(p[2])
    p[0] = p[4]


def p_def_constr(p):  # 'A <- f.'
    'def_constr : atom DEFIMPL formula DOT'
    p[0] = [[], p[1], p[3]]


def p_def_constr_omit_body(p):  # 'A <- .'
    'def_constr : atom DEFIMPL DOT'
    p[0] = [[], p[1], Node('ATOM', [Node.Bool(True)])]


def p_def_constr_omit_arrow(p):  # 'A.'
    'def_constr : atom DOT'
    p[0] = [[], p[1], Node('ATOM', [Node.Bool(True)])]


# TERM -----------------------------------------------------------------------------------------------------------------

def p_term_block(p):
    'term_block : OPTIM name COLON name LBRACE term_body RBRACE'
    p[0] = Node('OPTIMISE', [p[2], p[4], Node('OPTIMISEBODY', p[6])])
    p[0] = Optimise(p[0])


def p_term_body(p):
    'term_body : term'
    p[0] = [p[1]]


# QUERY -----------------------------------------------------------------------------------------------------------------

def p_query_block(p):
    'query_block : QUERY name COLON name LBRACE query_body RBRACE'
    p[0] = Node('QUERY', [p[2], p[4], Node('QUERYBODY', p[6])])
    p[0] = Query(p[0])


def p_query_body(p):
    'query_body : set'
    p[0] = [p[1]]


# GENERIC ELEMENTS -----------------------------------------------------------------------------------------------------

# Tuple list

def p_tuple_list_many_range(p):  # 'a; b, c; (d, e); f..i; ...'
    'tuple_list : tuple_list SEMICOLON range'
    p[0] = p[1] + p[3]


def p_tuple_list_one_range(p):
    'tuple_list : range'
    p[0] = p[1]


def p_range(p):  # '1..N' -> '1,2,...,N' / 'a..x' -> 'a,b,...,x'
    'range : name DOT DOT name'
    try:
        l = list(range(int(extract_name(p[1])), int(extract_name(p[4])) + 1))
    except ValueError:
        x = [p[1], p[4]]
        try:
            for i in range(len(x)):
                x[i] = extract_name(x[i])
                if x[i].startswith('\'') and x[i].endswith('\''):
                    x[i] = x[i].replace('\'', '"')
                if x[i].startswith('"') and x[i].endswith('"'):
                    x[i] = x[i].strip('"')
            l = [chr(x) for x in range(ord(x[0]), ord(x[1]) + 1)]
        except ValueError:
            raise
        except TypeError:
            raise TypeError(f'Cannot generate range for given interval {x}.')
    p[0] = [Node('TUPLE', [Node('NAME', [i])]) for i in l]


def p_tuple_list_many(p):
    'tuple_list : tuple_list SEMICOLON tuple'
    p[0] = p[1] + [p[3]]


def p_tuple_list_one(p):
    'tuple_list : tuple'
    p[0] = [p[1]]


def p_tuple_par(p):
    'tuple : LPAR element_list RPAR'
    p[0] = Node('TUPLE', p[2])


def p_tuple_nopar(p):
    'tuple : element_list'
    p[0] = Node('TUPLE', p[1])


# Element list
def p_element_list_many(p):  # 'a, b, ...'
    'element_list : element_list COMMA name'
    p[0] = p[1] + [p[3]]


def p_element_list_one(p):
    'element_list : name'
    p[0] = [p[1]]


# Bool
def p_bool(p):  # 'true' / 'false'
    '''bool : FALSE
            | TRUE '''
    p[0] = Node.Bool(p[1])  # = Node('BOOL', [p[1]])


# Name

# TODO create separate nodes for "object" and "string" to disambiguate between them?
def p_name(p):  # 'a' / 'B' / ''c'' / '"d"' / '1'
    '''name : QUOTE name QUOTE'''
            #| APOST name APOST'''
    p[2].name = f'"{p[2].name}"'
    p[0] = p[2]


def p_id(p):
    'name : ID'
    if p[1].startswith('\'') and p[1].endswith('\''):
        p[1] = p[1].strip('\'')
        #p[1].set_name(f'"{p[1].get_name()}"')
    p[0] = Node.Name(p[1])


def p_error(s):
    raise YaccError(s)


IDP3Yacc = yacc.yacc(debug=False, write_tables=False)
IDP3Yacc_start = lambda start: (yacc.yacc(start=start, debug=False, write_tables=False))
