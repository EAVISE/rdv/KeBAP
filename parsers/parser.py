# Parser abstract interface class
# authors: Kylian Van Dessel

from __future__ import annotations

from abc import ABC, abstractmethod
from blocks import Block
from nodes import Node

class Parser(ABC):
    """ Abstract interface class to implement parsers for different (input) languages
    Methods:
        parse_file(<file_path>) -> list[Block] | Node
        parse_string(<string_representation>) -> list[Block] | Node
    """

    @abstractmethod
    def parse_file(self, file, **kwargs) -> list[Block] | Node:
        """ Parse (input language) specification from a file to a list of Block objects (by default).
            Parsing elements on an individual base may return single Node instead (depending on language). """
        pass

    @abstractmethod
    def parse_string(self, repr, **kwargs) -> list[Block] | Node:
        """ Parse (input language) specification from a string to a list of Block objects (by default).
            Parsing elements on an individual base may return single Node instead (depending on language). """
        pass


if __name__ == '__main__':
    pass
