# IDP3 lexical tokenizer (LEX)
# authors: Kylian Van Dessel, Joost Vennekens

import ply.lex as lex


symbol_names = {'&': 'AND', '|': 'OR', '~': 'NOT', '=>': 'RIMP', '<=': 'LIMP', '<=>': 'EQUIV',
                '=': 'EQ', '~=': 'NEQ', '<': 'LT', '=<': 'LEQ', '>': 'GT', '>=': 'GEQ',
                '+': 'PLUS', '-': 'MINUS', '*': 'TIMES', '/': 'DIVIDE', '%': 'MODULO',
                '!': 'FORALL', '?': 'EXISTS' }


class LexError(Exception):
    pass


reserved = {
    'type': 'TYPE',
    'partial': 'PARTIAL',
    'isa': 'SUBSET',
    'contains': 'SUPERSET',
    'constructed': 'CONSTR',
    'from': 'FROM',
    'nat': 'NAT',
    'int': 'INT',
    'string': 'STR',

    'ct': 'CERTTRUE',
    'cf': 'CERTFALSE',
    'true': 'TRUE',
    'false': 'FALSE',

    'card': 'CARD',
    'sum': 'SUM',
    'prod': 'PROD',
    'min': 'MIN',
    'max': 'MAX',
    'abs': 'ABS',
    'MAX': 'TYPEMAX',
    'MIN': 'TYPEMIN',
}


tokens = ['AND', 'OR', 'NOT', 'RIMP', 'LIMP', 'EQUIV',
          'EQ', 'NEQ', 'LT', 'LEQ', 'GT', 'GEQ',
          'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MODULO', 'COUNT',
          'FORALL', 'EXISTS', 'DEFIMPL', 'MAP',
          'LPAR', 'RPAR', 'LBRACE', 'RBRACE', 'LSQBRACK', 'RSQBRACK',
          'DOT', 'COMMA', 'COLON', 'SEMICOLON', 'QUOTE', #'APOST'
          'VOCAB', 'STRUC', 'THEOR', 'OPTIM', 'QUERY', 'ID'] + list(reserved.values())


t_AND = r'&'
t_OR = r'\|'
t_NOT = r'~'
t_RIMP = r'=>'
t_LIMP = r'<='
t_EQUIV = r'<=>'

t_FORALL = r'!'
t_EXISTS = r'\?'
t_DEFIMPL = r'<-'
t_MAP = r'->'

t_EQ = r'='
t_NEQ = r'~='
t_LT = r'<'
t_LEQ = r'=<'
t_GT = r'>'
t_GEQ = r'>='

t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MODULO = r'\%'
t_COUNT = r'\#'

t_LPAR = r'\('
t_RPAR = r'\)'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_LSQBRACK = r'\['
t_RSQBRACK = r'\]'

t_DOT = r'\.'
t_COMMA = r','
t_COLON = r':'
t_SEMICOLON = r';'
t_QUOTE = r'\"'
#t_APOST = r'\''

t_ignore = " \t\n\r"
t_ignore_commentblock = r'(\/\*(\*(?!\/)|[^*])*\*\/)'
t_ignore_commentline = r'(\/\/.*)'


def t_VOCAB(t):
    r'[V|v]ocabulary'
    t.value = t.value.upper()
    return t


def t_STRUC(t):
    r'[S|s]tructure'
    t.value = t.value.upper()
    return t


def t_THEOR(t):
    r'[T|t]heory'
    t.value = t.value.upper()
    return t


def t_OPTIM(t):
    r'[T|t]erm'
    t.value = t.value.upper()
    return t


def t_QUERY(t):
    r'[Q|q]uery'
    t.value = t.value.upper()
    return t


def t_PROCEDURE(t):
    r'procedure .*\{([^\{])*\}'
    pass  # discard token


def t_ID(t):
    r"[a-zA-Z0-9_']+"
    t.type = reserved.get(t.value, 'ID')  # check for reserved words
    return t


def t_error(t):
    raise LexError(t)


IDP3Lex = lex.lex(debug=False)
