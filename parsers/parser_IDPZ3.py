# IDP-Z3 parser
# Authors: Kylian Van Dessel, Robin De Vogelaere, Joost Vennekens

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from parsers.parser import Parser
from parsers.idpz3_lex import IDPZ3Lex
from parsers.idpz3_yacc import IDPZ3Yacc, IDPZ3Yacc_start, YaccError


class IDPZ3Parser(Parser):
    def __init__(self):
        pass

    def parse_string(self, s: str, start=None, **kwargs):
        """ Parse IDP-Z3 specification from string. """
        if start:
             start = 'constraint' if start == 'definition' else start
             return IDPZ3Yacc_start(start=start).parse_elem(s, lexer=IDPZ3Lex)
        return IDPZ3Yacc.parse(s, lexer=IDPZ3Lex)

    def parse_file(self, file: str, **kwargs):
        """ Parse IDP-Z3 specification from file. """
        with open(file, 'r') as f:
            repr = f.read()
        return self.parse_string(repr, kwargs)

    def parse_empty(self):
        """ Parse empty IDP-Z3 specification. """
        idp_empty = "Vocabulary {} Theory {} Structure {}"
        return self.parse_string(idp_empty)


if __name__ == '__main__':
    try:
        idp_file = sys.argv[1]
    except IndexError:
        print('warning: no IDP-Z3 file provided, using test file instead.')
        idp_file = './test_scripts/_dummyfiles/idpz3_dummy.idpz3'
    print(f'running IDP-Z3 parser on \'{idp_file}\'')
    blocks = IDPZ3Parser().parse_file(idp_file)
    print(blocks)
    print('\n'.join(map(str,blocks)))
    exit(0)
