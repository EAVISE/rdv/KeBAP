# IDP-Z3 syntax grammar (YACC)
# Authors: Robin De Vogelaere, Kylian Van Dessel, Joost Vennekens

import warnings
import ply.yacc as yacc
from parsers.idpz3_lex import reserved, symbol_names, tokens
from blocks import Vocabulary, Structure, Theory
from nodes import Node, extract_name


class YaccError(Exception):
    pass


V_DFLT, S_DFLT, T_DFLT = 'V', 'S', 'T'


precedence = (
    ('left', 'COMMA'),
    ('nonassoc', 'FORALL', 'EXISTS'),
    ('nonassoc', 'DEFIMPL'),
    ('nonassoc', 'EQUIV'),
    ('nonassoc', 'RIMP', 'LIMP'),
    ('left', 'OR'),
    ('left', 'AND'),
    ('right', 'NOT'),
    ('nonassoc', 'LT', 'GT', 'LEQ', 'GEQ'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE', 'MODULO'),
    ('right', 'UMINUS'),
)


# idp block

def p_idp_block_list_many(p):
    'idp_block_list : idp_block_list idp_block'
    p[0] = p[1] + p[2]


def p_idp_block_list_one(p):
    'idp_block_list : idp_block'
    p[0] = p[1]


def p_idp_block(p):
    '''idp_block : vocab_block
                 | struc_block
                 | theory_block'''
    p[0] = [p[1]]


# VOCABULARY -----------------------------------------------------------------------------------------------------------

# block

def p_vocab_block(p):
    'vocab_block : VOCAB name vocab_body'
    p[0] = Node('VOCABULARY', [p[2], Node.Name(''), p[3]])
    p[0] = Vocabulary(p[0])


def p_vocab_block_noname(p):
    'vocab_block : VOCAB vocab_body'
    p[0] = Node('VOCABULARY', [Node.Name(V_DFLT), Node.Name(''), p[2]])
    p[0] = Vocabulary(p[0])


def p_vocab_body(p):
    'vocab_body : LBRACE decl_list RBRACE'
    p[0] = Node('VOCABULARYBODY', p[2])


def p_vocab_body_empty(p):
    'vocab_body : LBRACE RBRACE'
    p[0] = Node('VOCABULARYBODY', [])


# import

def p_import(p):
    'declaration : IMPORT name'
    warnings.warn('Importing other vocabularies is not supported.')
    p[0] = None

# declarations

def p_decl_list_combine(p):
    'decl_list : decl_list decl_list'
    p[0] = p[1] + p[2]


def p_decl_list_many(p):
    'decl_list : decl_list declaration'
    p[0] = p[1] + [p[2]]


def p_decl_list_one(p):
    'decl_list : declaration'
    p[0] = [p[1]]


def p_declarations(p):
    '''decl_list : pred_decls
                | func_decls '''
    p[0] = [Node('DECLARATION', [decl]) for decl in p[1]]

def p_declaration(p):
    '''declaration : type_decl
                   | pred_decl
                   | func_decl '''
    if not isinstance(p[1], list):
        p[1] = [p[1]]
    p[0] = Node('DECLARATION', p[1])


# Type declaration

def p_type_decl(p):  # 'type T'
    'type_decl : TYPE name'
    p[0] = [Node.Type(p[2])]


def p_type_decl_numerical(p):  # 'type T := { 1, 2, ... }'
    'type_decl : TYPE name ASSIGN pred_enum'
    p[0] = [Node.Type(p[2]), p[4]]


def p_type_decl_construct(p):  # 'type T := constructed from { 'a', F(T,T), C }'
    'type_decl : TYPE name ASSIGN CONSTR FROM LBRACE pred_enum RBRACE'
    p[0] = [Node.Type(p[2]), p[7]]


def p_type_decl_sub(p):  # 'type T <: T1'
    'type_decl : type_decl SUBSET name'
    p[1][0].children.append(Node("SUBSET", [p[3]]))
    p[0] = p[1]


#def p_builtin(p):  # see 'name:'
    #'''built_in : '''


def p_type_decl_super(p):  # 'type T :> T1,T2,...'
    'type_decl : type_decl SUPERSET element_list'
    p[1][0].children.append(Node("SUPERSET", p[3]))
    p[0] = p[1]


# Predicate declaration

def p_pred_decl(p):  # P: ( T * T * ...) -> Bool
    'pred_decl : name COLON argument_list MAP BOOL'
    p[0] = Node.Symbol('PRED', name=p[1], typing=p[3])


def p_pred_decl_bool(p):  # P: () -> Bool / P: -> Bool
    '''pred_decl : name COLON LPAR RPAR MAP BOOL
                 | name COLON MAP BOOL '''
    p[0] = Node.Symbol('PRED', name=p[1], typing=[])

# Combined predicate declarations
def p_pred_decl_combined(p):
    'pred_decls : element_list COLON argument_list MAP BOOL'
    p[0] = [Node.Symbol('PRED', name=x, typing=p[3]) for x in p[1]]


def p_pred_decl_bool_combined(p):
    '''pred_decls : element_list COLON LPAR RPAR MAP BOOL
                 | element_list COLON MAP BOOL '''
    p[0] = [Node.Symbol('PRED', name=x, typing=[]) for x in p[1]]


# Function declaration

def p_func_decl(p):  # F: (T * T * ...) -> T
    'func_decl : name COLON argument_list MAP name'
    p[0] = Node.Symbol('FUNC', name=p[1], typing=p[3]+[p[5]])


def p_func_decl_const_par(p):  # C: () -> T
    'func_decl : name COLON LPAR RPAR MAP name'
    p[0] = Node.Symbol('FUNC', name=p[1], typing=[p[6]])


def p_func_decl_const(p):  # C: -> T
    'func_decl : name COLON MAP name'
    p[0] = Node.Symbol('FUNC', name=p[1], typing=[p[4]])


# Combined function declarations
def p_func_decl_combined(p):
    'func_decls : element_list COLON argument_list MAP name'
    p[0] = [Node.Symbol('FUNC', name=x, typing=p[3]+[p[5]]) for x in p[1]]


def p_func_decl_const_par_combined(p):
    'func_decls : element_list COLON LPAR RPAR MAP name'
    p[0] = [Node.Symbol('FUNC', name=x, typing=[p[6]]) for x in p[1]]


def p_func_decl_const_combined(p):
    'func_decls : element_list COLON MAP name'
    p[0] = [Node.Symbol('FUNC', name=x, typing=[p[4]]) for x in p[1]]


# argument list
def p_argument_list_many(p):
    'argument_list : arguments'
    p[0] = p[1]


def p_argument_list_many_par(p):
    'argument_list : LPAR arguments RPAR'
    p[0] = p[2]


def p_argument_list_one_no_brackets(p):
    'argument_list : name'
    p[0] = [p[1]]


def p_arguments_many(p):  # a * b * ...
    'arguments : arguments TIMES name'
    p[0] = p[1] + [p[3]]


def p_arguments_one(p):
    'arguments : name'
    p[0] = [p[1]]


# STRUCTURE ------------------------------------------------------------------------------------------------------------

# block

def p_struc_block(p):  # structure S:V { ... }
    'struc_block : STRUC name COLON name struc_body'
    p[0] = Node('STRUCTURE', [p[2], p[4], p[5]])
    p[0] = Structure(p[0])


def p_struc_block_noname(p):  # structure :V { ... }
    'struc_block : STRUC COLON name struc_body'
    p[0] = Node('STRUCTURE', [Node.Name(S_DFLT), p[3], p[4]])
    p[0] = Structure(p[0])


def p_struc_block_novoc(p):  # structure S { ... }
    'struc_block : STRUC name struc_body'
    p[0] = Node('STRUCTURE', [p[2], Node.Name(V_DFLT), p[3]])
    p[0] = Structure(p[0])


def p_struc_block_noname_novoc(p):  # structure { ... }
    'struc_block : STRUC struc_body'
    p[0] = Node('STRUCTURE', [Node.Name(S_DFLT), Node.Name(V_DFLT), p[2]])
    p[0] = Structure(p[0])


def p_struc_body(p):
    'struc_body : LBRACE interpr_list RBRACE'
    p[0] = Node('STRUCTUREBODY', p[2])


def p_struc_body_empty(p):
    'struc_body : LBRACE RBRACE'
    p[0] = Node('STRUCTUREBODY', [])


# interpretations

def p_struc_body_many(p):
    'interpr_list : interpr_list interpretation'
    p[0] = p[1] + [p[2]]


def p_struc_body_one(p):
    'interpr_list : interpretation'
    p[0] = [p[1]]


def p_interpretation(p):
    '''interpretation : pred_assign
                      | func_assign'''
    p[0] = Node('ASSIGNMENT', p[1])


# Predicate assignment

def p_pred_assign(p):
    'pred_assign : name ASSIGN pred_eq DOT'
    p[0] = [Node.Symbol('PRED', p[1]), p[3]]


# Predicate interpretation

def p_pred_eq(p):
    '''pred_eq : pred_enum
               | pred_bool'''
    p[0] = p[1]


def p_pred_enum(p):
    'pred_enum : LBRACE tuple_list RBRACE'
    p[0] = Node('PRED_ENUM', p[2])


def p_pred_enum_singles(p):
    'pred_enum : LBRACE element_list RBRACE'
    p[0] = Node('PRED_ENUM', p[2])


def p_pred_bool_false(p):  # 'false' / '{}'
    '''pred_bool : FALSE
                 | LBRACE RBRACE'''
    p[0] = Node('PRED_ENUM', [])


def p_pred_bool_true(p):  # 'true' / '{()}'
    '''pred_bool : TRUE
                 | LBRACE LPAR RPAR RBRACE'''
    p[0] = Node('PRED_ENUM', [Node('TUPLE', [])])


# Function assignment

def p_func_assign(p):
    'func_assign : name ASSIGN func_eq DOT'
    p[0] = [Node.Symbol('FUNC', p[1]), p[3]]


def p_func_assign_else(p):
    'func_assign : name ASSIGN func_eq ELSE name DOT'
    p[0] = [Node.Symbol('FUNC', name=p[1]), p[3], Node('ELSE', [p[5]])]


# Function interpretation

def p_func_eq(p):
    '''func_eq : func_enum
               | func_const'''
    p[0] = p[1]


def p_func_enum(p):  # '{ (1,a)->b, (2,b)->c }'
    'func_enum : LBRACE mapping_list RBRACE'
    p[0] = Node('FUNC_ENUM', p[2])


def p_mapping_list_many(p):
    'mapping_list : mapping_list COMMA mapping'
    p[0] = p[1] + [p[3]]


def p_mapping_list_one(p):
    'mapping_list : mapping'
    p[0] = [p[1]]


def p_mapping(p):
    'mapping : tuple MAP name'
    p[1].children.append(p[3])
    p[0] = p[1]


def p_mapping_single(p):
    'mapping : name MAP name'
    p[0] = Node('TUPLE', [p[1], p[3]])


def p_func_const_long(p):  # '{ -> a }'
    'func_const : LBRACE MAP name RBRACE'
    p[0] = Node('FUNC_ENUM', [Node('TUPLE', [p[3]])])


def p_func_const(p):  # 'a' /  ''a'' / '"a"'
    'func_const : name'
    p[0] = Node('FUNC_ENUM', [Node('TUPLE', [p[1]])])


# THEORY ---------------------------------------------------------------------------------------------------------------

# block

def p_theory_block(p):
    'theory_block : THEOR name COLON name theory_body'
    p[0] = Node('THEORY', [p[2], p[4], p[5]])
    p[0] = Theory(p[0])


def p_theory_block_noname(p):
    'theory_block : THEOR COLON name theory_body'
    p[0] = Node('THEORY', [Node.Name(T_DFLT), p[3], p[4]])
    p[0] = Theory(p[0])


def p_theory_block_novoc(p):
    'theory_block : THEOR name theory_body'
    p[0] = Node('THEORY', [p[2], Node.Name(V_DFLT), p[3]])
    p[0] = Theory(p[0])


def p_theory_block_noname_novoc(p):
    'theory_block : THEOR theory_body'
    p[0] = Node('THEORY', [Node.Name(T_DFLT), Node.Name(V_DFLT), p[2]])
    p[0] = Theory(p[0])


def p_theory_body(p):
    'theory_body : LBRACE constr_list RBRACE'
    p[0] = Node('THEORYBODY', p[2])


def p_theory_body_empty(p):
    'theory_body : LBRACE RBRACE'
    p[0] = Node('THEORYBODY', [])


# constraints

def p_constr_list_many(p):
    'constr_list : constr_list constraint'
    p[0] = p[1] + [p[2]]


def p_constr_list_one(p):
    'constr_list : constraint'
    p[0] = [p[1]]


def p_constraint(p):  # 'f.'
    'constraint : formula DOT'
    p[0] = Node('CONSTRAINT', [p[1]])


def p_constraint_definition(p):  # '{ <definition> }'
    'constraint : definition'
    p[0] = p[1]


# Formula
#  (formula between parentheses, negation of formula, binary op. of formulas, quantification of formula, atom)

def p_formula_par(p):  # '(f)'
    'formula : LPAR formula RPAR'
    p[0] = p[2]


def p_formula_neg(p):  # '~f'
    'formula : NOT formula'
    p[0] = Node('NOT', [p[2]])


def p_formula_binop(p):  # 'f & g' / 'f | g' / 'f=>g' / ...
    '''formula : formula AND formula
               | formula OR formula
               | formula LIMP formula
               | formula RIMP formula
               | formula EQUIV formula'''
    p[2] = symbol_names[p[2]]
    if p[1].symbol == p[2] and p[2] in ('AND', 'OR'):  # concatenate AND/OR
        p[1].children.append(p[3])
        p[0] = p[1]
    else:
        p[0] = Node(p[2], [p[1], p[3]])


def p_formula_if(p):  # 'if f then f else f'
    '''formula : IF formula THEN formula ELSE formula'''
    p[0] = Node("IFFORMULA", [p[2], p[4], p[6]])


def p_formula_quantified(p):  # '!x in T, y in T:f' / '?x in T, y in T:f'
    '''formula : FORALL var_list COLON formula %prec FORALL
               | EXISTS var_list COLON formula %prec EXISTS'''
    p[0] = Node(symbol_names[p[1]], [p[2], p[4]])


def p_var_list_many(p):
    'var_list : var_list COMMA var_decl'
    if not isinstance(p[3], list):
        p[3] = [p[3]]
    p[0] = p[1] + p[3]


def p_var_list_one(p):
    'var_list : var_decl'
    if isinstance(p[1], list):
        p[0] = p[1]
    else:
        p[0] = [p[1]]


def p_var_decl_many(p):  # shorthand '!x, y in T' for '!x in T, !y in T:'
    'var_decl : element_list IN name'
    variables = []
    for item in p[1]:
        variables.append(Node('VAR', [item, p[3]]))
    p[0] = variables


def p_var_decl_one(p):
    'var_decl : name IN name'
    p[0] = Node('VAR', [p[1], p[3]])


def p_formula_atom(p):
    'formula : atom'
    p[0] = p[1]

# Atom
#  (true/false, boolean predicate symbol, predicate symbol with terms, comparison of terms)

def p_atom_bool(p):  # 'true' / 'false'
    '''atom : bool'''
    p[0] = Node('ATOM', [p[1]])


def p_atom_pred_bool(p):  # 'B()'
    'atom : name LPAR RPAR'
    p[0] = Node('ATOM', [Node.Symbol('PRED', p[1], arity=0)])


def p_atom_pred(p):  # 'P(T, ...)'
    'atom : name LPAR term_list RPAR'
    p[0] = Node('ATOM', [Node.Symbol('PRED', p[1], arity=len(p[3])), *p[3]])


def p_atom_cmp(p):  # 't1 = t2' / 't1 =< t2' / ...
    'atom : term comparison term'
    p[0] = Node('ATOM', [Node('CMP', [p[2], p[1], p[3]])])


def p_comparison(p):
    '''comparison : EQ
                  | LEQ
                  | NEQ
                  | GEQ
                  | LT
                  | GT '''
    p[0] = symbol_names[p[1]]


# Term
#  (variable/constant, function symbol with terms, mathematical operation, aggregate function)
def p_term_var(p):  # '"a"' / '5'
    'term : name'
    p[0] = Node('TERM', [p[1]])


def p_term_func_const(p):  # 'F()'
    'term : name LPAR RPAR'
    p[0] = Node('TERM', [Node.Symbol('FUNC', p[1], arity=0)])


def p_term_func(p):  # 'F(T, ...)'
    'term : name LPAR term_list RPAR'
    p[0] = Node('TERM', [Node.Symbol('FUNC', p[1], arity=len(p[3])), *p[3]])


def p_term_par(p):  # '(t)'
    'term : LPAR term RPAR'
    # p[0] = Node('TERM', [Node('PAR', [p[2]])])
    p[0] = p[2]


def p_term_math(p):
    '''term : math
            | abs
            | aggr'''

    p[0] = Node('TERM', [p[1]])


# - Mathematical operations

def p_math_operation(p):  # 't1 + t2' / 't1 - t2' / ...
    '''math : term PLUS term
            | term MINUS term
            | term TIMES term
            | term DIVIDE term
            | term MODULO term'''
    p[2] = symbol_names[p[2]]
    p[0] = Node('MATH', [p[1], p[2], p[3]])


def p_math_uminus(p):  # '-t'
    'math : MINUS term %prec UMINUS'
    p[0] = Node('MINUS', [p[2]])
    #p[0] = Node('MATH', ['', symbol_names[p[1]], p[2]])


def p_abs(p):  # 'abs(t)'
    'abs : ABS LPAR term RPAR'
    p[0] = Node('ABS', [p[3]])


# - Aggregates

# TODO TBD change order of arguments to IDP-Z3 aggr for both IDP3 and IDP-Z3 (non-important)
def p_set_obj(p):  # '{g | x in T ... : f }'  # set maps to an object function 'g'
    'set_obj : LBRACE term OR var_list COLON formula RBRACE'
    p[0] = Node('SETOBJ', [p[4], p[6], p[2]])


def p_set_obj_empty(p):  # '{g | x in T ... }'
    'set_obj : LBRACE term OR var_list RBRACE'
    f = Node('ATOM', [Node.Bool(bool_=True)])
    p[0] = Node('SETOBJ', [p[4], f, p[2]])


def p_aggr_sum(p):  # 'sum{(set_obj)}'
    'aggr : SUM LBRACE set_obj RBRACE'
    p[0] = Node('AGGRSET', [reserved[p[1]], p[3]])


def p_aggr_minmax(p):  # 'min(set_obj)'
    'aggr : aggr_func set_obj'
    p[0] = Node('AGGRSET', [p[1], p[2]])


def p_aggr_func(p):
    '''aggr_func : MIN
                 | MAX'''
    p[0] = reserved[p[1]]


def p_set(p):       # '{x in T ... : f}'
    '''set : LBRACE var_list COLON formula RBRACE'''
    p[0] = Node('SET', [p[2], p[4]])


def p_aggr_card(p):  # '#(set)'
    '''aggr : COUNT set
            | CARD set'''
    p[0] = Node('AGGRSET', ['CNT', p[2]])


def p_term_list_many(p):
    'term_list : term_list COMMA term'
    p[0] = p[1] + [p[3]]


def p_term_list_one(p):
    'term_list : term'
    p[0] = [p[1]]


# TODO: check that terms have same output type
def p_term_if(p):  # 'if f then t1 else t2'
    '''term : IF formula THEN term ELSE term'''
    p[0] = Node("TERM",[Node("IFTERM", [p[2], p[4], p[6]])])


# Definitions
def p_definition(p):
    'definition : LBRACE definition_body RBRACE'
    p[0] = Node('DEFINITION', p[2])


def p_def_body_many(p):
    'definition_body : definition_body def_rule'
    p[0] = p[1] + [p[2]]


def p_def_body_one(p):
    'definition_body : def_rule'
    p[0] = [p[1]]


def p_def_rule(p):
    'def_rule : def_constr'
    p[0] = Node('DEFIMP', p[1])


def p_def_quan(p):  # '!x in T: ! y in T: ...'
    'def_constr : FORALL var_list COLON def_constr %prec FORALL'
    p[4][0].extend(p[2])
    p[0] = p[4]


def p_def_constr(p):  # 'A <- f.'
    'def_constr : atom DEFIMPL formula DOT'
    p[0] = [[], p[1], p[3]]


def p_def_constr_omit_arrow(p):  # 'A.'
    'def_constr : atom DOT'
    p[0] = [[], p[1], Node('ATOM', [Node.Bool(True)])]


# GENERIC ELEMENTS

# Tuple list
def p_tuple_list_many_range(p):  # 'a, (b, c), (d, e), f..i, ...'
    'tuple_list : tuple_list COMMA range'
    p[0] = p[1] + p[3]


def p_tuple_list_one_range(p):
    'tuple_list : range'
    p[0] = p[1]


def p_range(p):  # '1..N' -> '1,2,...,N' / 'a..x' -> 'a,b,...,x'
    'range : name DOT DOT name'
    try:
        l = list(range(int(extract_name(p[1])), int(extract_name(p[4])) + 1))
    except ValueError:
        x = [p[1], p[4]]
        try:
            for i in range(len(x)):
                x[i] = extract_name(x[i])
                if x[i].startswith('\'') and x[i].endswith('\''):
                    x[i] = x[i].replace('\'', '"')
                if x[i].startswith('"') and x[i].endswith('"'):
                    x[i] = x[i].strip('"')
            l = [chr(x) for x in range(ord(x[0]), ord(x[1]) + 1)]
        except ValueError:
            raise
        except TypeError:
            raise TypeError(f'Cannot generate range for given interval {x}.')
    p[0] = [Node('TUPLE', [Node('NAME', [i])]) for i in l]


def p_tuple_list_many(p):
    'tuple_list : tuple_list COMMA tuple'
    p[0] = p[1] + [p[3]]


def p_tuple_list_one(p):
    'tuple_list : tuple'
    p[0] = [p[1]]


def p_tuple_par(p):
    'tuple : LPAR element_list RPAR'
    p[0] = Node('TUPLE', p[2])


# Element list
def p_element_list_many(p):  #'a, b, ...'
    'element_list : element_list COMMA name'
    p[0] = p[1] + [p[3]]


def p_element_list_one(p):
    'element_list : name'
    p[0] = [p[1]]


# Bool

def p_bool(p):  # 'true' / 'false'
    '''bool : FALSE
            | TRUE '''
    p[0] = Node.Bool(p[1])  # = Node('BOOL', [p[1]])


# Name

# TODO create separate nodes for "object" and "string" to disambiguate between them?
def p_name(p):  # 'a' / 'B' / ''c'' / '"d"' / '1'
    '''name : QUOTE name QUOTE'''
            #| APOST name APOST'''
    p[2].name = f'"{p[2].name}"'
    p[0] = p[2]


def p_builtin(p):  # (same as user-declared types, but reserved as built-in types)
    '''name : INT
            | REAL
            | FLOAT
            | DATE'''
    p[0] = Node.Name(p[1])


#def p_float(p):
#    'name : FLOAT'
#    p[0] = Node.Name(p[1])


def p_id(p):
    'name : ID'
    if p[1].startswith('\'') and p[1].endswith('\''):
        p[1] = p[1].strip('\'')
    p[0] = Node.Name(p[1])


def p_error(s):
    raise YaccError(s)


IDPZ3Yacc = yacc.yacc(debug=False, write_tables=False)
IDPZ3Yacc_start = lambda start: (yacc.yacc(start=start, debug=False, write_tables=False))
