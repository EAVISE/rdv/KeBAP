# IDP-Z3 lexical tokenizer (LEX)
# authors: Robin De Vogelaere, Kylian Van Dessel, Joost Vennekens

import ply.lex as lex


symbol_names = {'&': 'AND', '|': 'OR', '~': 'NOT', '=>': 'RIMP', '<=': 'LIMP', '<=>': 'EQUIV',
                '=': 'EQ', '~=': 'NEQ', '<': 'LT', '=<': 'LEQ', '>': 'GT', '>=': 'GEQ',
                '+': 'PLUS', '-': 'MINUS', '*': 'TIMES', '/': 'DIVIDE', '%': 'MODULO',
                '!': 'FORALL', '?': 'EXISTS', ':=': ' ASSIGN', '<:': 'SUBSET', ':>': 'SUPERSET' }


class LexError(Exception):
    pass


reserved = {
    'type': 'TYPE',
    'constructed': 'CONSTR',
    'from': 'FROM',
    'Real': 'REAL',
    'Int': 'INT',
    'Bool' : 'BOOL',
    'Date': 'DATE',

    'true': 'TRUE',
    'false': 'FALSE',

    'card': 'CARD',
    'sum': 'SUM',
    'min': 'MIN',
    'max': 'MAX',
    'abs': 'ABS',

    'in': 'IN',
    'else': 'ELSE',
    'if': 'IF',
    'then': 'THEN',

    'import': 'IMPORT',
}


tokens = ['AND', 'OR', 'NOT', 'RIMP', 'LIMP', 'EQUIV',
          'EQ', 'NEQ', 'LT', 'LEQ', 'GT', 'GEQ',
          'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MODULO', 'COUNT', 'FLOAT',
          'FORALL', 'EXISTS', 'DEFIMPL', 'MAP',
          'LPAR', 'RPAR', 'LBRACE', 'RBRACE',
          'DOT', 'COMMA', 'COLON', 'QUOTE', 'ASSIGN', 'SUBSET', 'SUPERSET',
          'VOCAB', 'STRUC', 'THEOR', 'ID'] + list(reserved.values())


t_AND = r'&'
t_OR = r'\|'
t_NOT = r'~'
t_RIMP = r'=>'
t_LIMP = r'<='
t_EQUIV = r'<=>'

t_FORALL = r'!'
t_EXISTS = r'\?'
t_DEFIMPL = r'<-'
t_MAP = r'->'

t_LPAR = r'\('
t_RPAR = r'\)'
t_LBRACE = r'\{'
t_RBRACE = r'\}'

t_DOT = r'\.'
t_COMMA = r','
t_COLON = r':'
t_QUOTE = r'\"'
t_ASSIGN = ':='
t_SUBSET = r'<:'
t_SUPERSET = r':>'

t_EQ = r'='
t_NEQ = r'~='
t_LT = r'<'
t_LEQ = r'=<'
t_GT = r'>'
t_GEQ = r'>='

t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MODULO = r'\%'
t_COUNT = r'\#'

t_ignore = " \t\n\r"
t_ignore_commentblock = r'(\/\*(\*(?!\/)|[^*])*\*\/)'
t_ignore_commentline = r'(\/\/.*)'
t_ignore_annotations = r'\[([^\[])*\]'


def t_VOCAB(t):
    r'\b[V|v]ocabulary\b'
    t.value = t.value.upper()
    return t


def t_STRUC(t):
    r'\b[S|s]tructure\b'
    t.value = t.value.upper()
    return t


def t_THEOR(t):
    r'\b[T|t]heory\b'
    t.value = t.value.upper()
    return t


def t_PROCEDURE(t):
    r'procedure .*\{([^\{])*\}'
    pass  # discard token


def t_DISPLAY(t):
    r'\bdisplay\b\s?\{(.|\s)*?(?=[Tt]heory|[sS]tructure|[Vv]ocabulary|procedure|\Z)'
    pass  # discard token


def t_FLOAT(t):
    r"\d+.\d+"
    return t


def t_ID(t):
    r"[a-zA-Z0-9_']+"
    t.type = reserved.get(t.value, 'ID')  # Check for reserved words
    return t


def t_error(t):
    raise LexError(t)


IDPZ3Lex = lex.lex(debug=False)
